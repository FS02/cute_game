# Start by reading files in Documentation/Technical!

## Documentation
* Documentation/Technical - random cooings.
* Documentation/Doxygen/... - documentation generated with Doxygen. Generated files will be ignored by Git. Do not push them to bitbucket. Thank you a lot.

To build Doxygen docs, install Doxygen and execute the following commands:

cd C:/path/to/root/of/cute_game

doxygen Documentation/Doxygen/Engine/Doxyfile

doxygen Documentation/Doxygen/Game/Doxyfile

doxygen Documentation/Doxygen/Editor/Doxyfile

I made a bash script for this. I'd consider adding a bat script for Windows.

Run doxygen from the root directory of the project.
