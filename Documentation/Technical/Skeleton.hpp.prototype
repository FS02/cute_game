class Skeleton
{
public:
	Skeleton(b2World* world);
	Skeleton(b2World* world, const std::string& cesfile);
	~Skeleton();
	
	void create(const std::string& cesfile);
	void addBody(const std::string& id);
	void addFixture(const std::string& bodyId, const b2FixtureDef&);
	void addJoint(const std::string& id, const b2JointDef&);
	void destroyBody(const std::string& id);
	void destroyJoint(const std::string& id);
	
	b2Body* body(const std::string& id);
	b2Joint* joint(const std::string& id);
	
	void trigger(const std::string& id);
	
private:
	static bindSquirrel(HSQUIRRELVM vm);

	static bool createShapes(Skeleton*, b2Shape*, JSON::ValueHandle&);
	static bool createFixtures(Skeleton* b2Fixture*, std::vector<b2Shape>&, JSON::ValueHandle&);
	static bool createBodies(Skeleton*, std::vector<b2Fixture>&, JSON::ValueHandle&);
	static bool createJoints(Skeleton*, JSON::ValueHandle*);
	static bool createTriggers(Skeleton*, JSON::ValueHandle*);
	
	static void triggerForce(b2Body*, const b2Vec2&);
	static void triggerImpulse(b2Body*, const b2Vec2&);
	//...//

	std::map<std::string, b2Body> m_bodies;
	std::map<std::string, b2Joint> m_Joints;
	std::multimap<std::string, std::pair<std::function, std::stack>> m_triggers; // Function and it's argument
}