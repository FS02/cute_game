TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11

unix{
    # settings for linux

    INCLUDEPATH += /usr/include/SDL2 /usr/local/include/SDL2 ../ ../CuteEngine/Squirrel/include
    LIBS += -L../build-CuteEngine-Desktop-Debug -lCuteEngine -lSDL2 -lSDL2_mixer -lBox2D -lGL -lSDL2_image -ldl
}

win32{
    # settings for windows

    INCLUDEPATH += E:\SDL-Libraries\SDL2-2.0.0\i686-w64-mingw32\include\SDL2 E:\SDL-Libraries\SDL2_mixer-2.0.0\i686-w64-mingw32\include\SDL2 ../
    LIBS += -LC:/Users/Hyper\ Studios/Desktop/CuteG/CE_Build/debug -LE:\SDL-Libraries\SDL2-2.0.0\i686-w64-mingw32\lib -LE:\SDL-Libraries\SDL2_mixer-2.0.0\i686-w64-mingw32\lib -lCuteEngine
    LIBS += -lSDL2 -lOpenGL32 -lSDL2_mixer -lSDL2_image
}

SOURCES += \
    main.cpp
