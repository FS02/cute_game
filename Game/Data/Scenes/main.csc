{
	"Metadata" :
	{
		"Name" : "Test scene",
		"Gravity" : [0, -9.8]
	},
	"Entities" : [
		{
			"Name" : "Platform #1",
			"Description" : "Obstacle.cee",
			"Location.Position" : [ 2, -2 ],
			"Location.Rotation" : -0.349055556,
			"Graphics.Size" : [ 3, 0.8 ],
			"Physics.Size" : [ 3, 0.8 ]
		},
		{
			"Name" : "Platform #2",
			"Description" : "Obstacle.cee",
			"Location.Position" : [ -3, 0 ],
			"Graphics.Size" : [ 3, 0.8 ],
			"Physics.Size" : [ 3, 0.8 ]
		},
		{
			"Name" : "Player",
			"Description" : "Player.cee",
			"Location.Position" : [ -4, 8 ]
		},
		{
			"Name" : "Edge",
			"Description" : "Edge.cee"
		}
	]
}
