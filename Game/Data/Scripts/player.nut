::import("Data/Scripts/CuteEngine")

class player extends CuteComponent
{
	jumpLimit = 2
	function start()
	{
		local a = Vector2(10,10);
		if (camera)
		{
			camera.activate();
		}
//		physics.skeleton().mainBody().setFixedRotation(false);
		print("Player Component started");
	}

	function update(delta)
	{		
		if (input.control("Left"))
			physics.skeleton().revoluteJoint("RevoluteJoint").setMotorSpeed(10);
		else if (input.control("Right"))
			physics.skeleton().revoluteJoint("RevoluteJoint").setMotorSpeed(-10);
		else 
			physics.skeleton().revoluteJoint("RevoluteJoint").setMotorSpeed(0);
		
		local other = physics.rayCast(Vector2(0,-1),1.5,2);
		if (other) jumpLimit = 2;
		if (input.controlDown("Jump") && (other || jumpLimit > 0))
		{
			physics.skeleton().mainBody().applyLinearImpulse(Vector2(0,1.5), physics.skeleton().mainBody().getWorldCenter());
			--jumpLimit;
		}

		if (camera) camera.setCenter(location.getPosition());
		print(input.mouseWheel().x);

		local curRect = graphics.getTextureRect();
		++curRect.x;
		graphics.setTextureRect(curRect);
		local c = Color(0, 0, 0, 1);
		graphics.setColor(graphics.getColor() - c);

	}

	function end()
	{
	}
}
