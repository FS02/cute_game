class CuteComponent
{
	entity = null
	graphics = null
	location = null
	physics = null
	scripts = null
	sound = null
	camera = null

	constructor()
	{
		entity = _entity
		graphics = _graphics
		location = _location
		physics = _physics
		scripts = _scripts
		sound = _sound		
		camera = _camera
	}
	
	function start()
	{
	}
	
	function update(delta)
	{
	}
	
	function end()
	{
	}
}
