#include <CuteEngine/Core.hpp>
#include <CuteEngine/World.hpp>
#include <CuteEngine/Utils.hpp>
#include <SDL.h>
#include <iostream>
#include <CuteEngine/World/SquirrelBindFactory.hpp>

#undef main
int main()
{
    SDL_Init(SDL_INIT_EVERYTHING);

    // Bind native class to Squirrel
    cute::SquirrelBindFactory::bind(&cute::_StaticBindVector2::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::_StaticBindVector3::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Rect::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Matrix3x3::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Color::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Camera2::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::GameWorld::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::InputMgr::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Entity::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Graphics2D::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Transform2D::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Physics2D::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Scripts::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Sound::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Camera2D::bindSquirrel);
    cute::SquirrelBindFactory::bind(&cute::Skeleton::bindSquirrel);

    cute::IRenderWindow* window = cute::IRenderWindow::CreateSDL2Window();
    window->create(cute::WindowDef("CuteEngine"));

    cute::Physics2DCompMgr* physMgr;
    cute::GameWorld* world = new cute::GameWorld(); // We should destroy world before SDL_Quit, I think.
    world->addComponentMgr(new cute::TransformCompMgr());
    world->addComponentMgr(new cute::Graphics2DComponentMgr());
    world->addComponentMgr(new cute::SoundComponentMgr());
    world->addComponentMgr(physMgr = new cute::Physics2DCompMgr());
    world->addComponentMgr(new cute::ScriptsComponentMgr());
    // add other component managers here

    world->initialize();
    world->loadScene("main.csc");

    cute::InputMgr input;
    input.loadFromJSON("Data/input.cib");

    // main loop
    bool doQuit = false;
    double delta = 0, frameBegin;
    Sqrat::RootTable().SetInstance("world", world);
    Sqrat::RootTable().SetInstance("input", &input);

    while(!doQuit)
    {
        frameBegin = GetTime();

        input.update();
        if(input.SDLEvent().type == SDL_QUIT)
            doQuit = true;

        window->clear(cute::Color::Magenta);
        world->update(delta);
        window->display();

        delta = GetTime() - frameBegin;
    }

    delete world;
    delete window;
    SDL_Quit();
}
