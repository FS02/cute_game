#ifndef ENTITYTYPEEDITOR_HPP
#define ENTITYTYPEEDITOR_HPP

#include <QWidget>
#include <CuteEngine/World/GameWorld.hpp>

namespace Ui {
    class EntityTypeEditor;
}

class EntityTypeEditor : public QWidget, public cute::ResourceMgrMonitor<cute::EntityDesc>
{
    Q_OBJECT

public:
    explicit EntityTypeEditor(cute::GameWorld* world, QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~EntityTypeEditor();

    QString selectedEntityType() const;

private:
    Ui::EntityTypeEditor *ui;
    cute::GameWorld* m_world;

    bool addEntityDesc(cute::EntityDesc* desc);
    virtual void onResourceAdded(cute::EntityDesc *ptr);

private slots:
    void addNewEntityType();
    void removeEntityType();
};

#endif // ENTITYTYPEEDITOR_HPP
