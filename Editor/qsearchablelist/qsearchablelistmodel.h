#ifndef QSEARCHABLELISTMODEL_H_
#define QSEARCHABLELISTMODEL_H_

#include <QAbstractListModel>
#include <QLinkedList>

class QSearchableListModel;

class QAbstractSearchableListItem
{
public:
    virtual ~QAbstractSearchableListItem();

    virtual QString name() const = 0;
};

class QSimpleSearchableListItem : public QAbstractSearchableListItem
{
public:
    QSimpleSearchableListItem(const QString& value);

    virtual QString name() const;

private:
    QString m_value;
};

class QListQuery : public QObject
{
    friend class QSearchableListModel;
    Q_OBJECT

public:
    enum QueryFlag
    {
        None,
        CaseSensitive
    };

    int flags() const;

    QString phrase() const;
    void appendToPhrase(const QString& text);
    void setPhrase(const QString& text);

    QString itemName(int index) const;
    QAbstractSearchableListItem* item(int index);
    const QAbstractSearchableListItem* item(int index) const;
    int itemsCount() const;

    void itemAboutToBeRemoved(QAbstractSearchableListItem* item);

    bool contains(const QString& name) const;

signals:
    void contentChanged();

private:
    typedef QLinkedList<QAbstractSearchableListItem*> ItemList;

    int m_flags;
    QString m_phrase;
    const ItemList& m_items;
    ItemList::const_iterator m_begin, m_end;

    QListQuery(int flags, QSearchableListModel* parent, const ItemList& items);
};

class QSearchableListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    QSearchableListModel(QObject* parent = 0);
    virtual ~QSearchableListModel();

    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;

    QAbstractSearchableListItem* item(int index);
    QAbstractSearchableListItem* realItem(int index);
    void addItem(const QString& value);
    void addItem(QAbstractSearchableListItem* item);
    void removeItem(const QString& name);
    void clear();

    QListQuery* beginQuery(int flags);
    QListQuery* query();
    void endQuery();

private:
    QLinkedList<QAbstractSearchableListItem*> m_items;
    QListQuery* m_query;

private slots:
    void queryContentChanged();
};

#endif /* QSEARCHABLELISTMODEL_H_ */
