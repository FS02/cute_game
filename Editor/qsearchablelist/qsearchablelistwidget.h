#ifndef QSEARCHABLELISTWIDGET_H
#define QSEARCHABLELISTWIDGET_H

#include "qsearchablelistmodel.h"
#include <QListView>

class QSearchableListWidget : public QListView
{
    Q_OBJECT

public:
    QSearchableListWidget(QWidget* parent = 0);
    virtual ~QSearchableListWidget();

    void insertItem(const QString& name);
    void removeItem(const QString& name);
    void clear();

    QStringList selectedNames() const;

signals:
    void itemSelected(const QAbstractSearchableListItem* item);

public slots:
    void setQueryText(const QString& text);
    void clearQuery();

private:
    QSearchableListModel* m_model;

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
};

#endif // QSEARCHABLELISTWIDGET_H
