#include "qsearchablelistmodel.h"
#include <algorithm>

namespace
{
    struct Compare
    {
        bool operator ()(const QAbstractSearchableListItem* item, const QString& name) const
        {
            return item->name() < name;
        }

        bool operator ()(const QString& name, const QAbstractSearchableListItem* item) const
        {
            return name < item->name();
        }
    };

    struct SearchNCSCompare
    {
        bool operator ()(const QAbstractSearchableListItem* item, const QString& name) const
        {
            QString v = item->name().left(name.size());
            return v.compare(name, Qt::CaseInsensitive) < 0;
        }

        bool operator ()(const QString& name, const QAbstractSearchableListItem* item) const
        {
            QString v = item->name().left(name.size());
            return name.compare(v, Qt::CaseInsensitive) < 0;
        }
    };

    template<class T>
    int diff(T a, T b)
    {
        int n = 0;
        for( ; a != b; ++n, ++a);
        return n;
    }
}

/*
 * QAbstractSearchableListItem
 */

QAbstractSearchableListItem::~QAbstractSearchableListItem()
{
}

/*
 * QSimpleSearchableListItem
 */

QSimpleSearchableListItem::QSimpleSearchableListItem(const QString &value)
    : m_value(value)
{
}

QString QSimpleSearchableListItem::name() const
{
    return m_value;
}

/*
 * QListQuery
 */

int QListQuery::flags() const
{
    return m_flags;
}

QString QListQuery::phrase() const
{
    return m_phrase;
}

void QListQuery::appendToPhrase(const QString &text)
{
    if(text.isEmpty())
        return;

    m_phrase += text;
    bool modified = false;

    auto lowerBound = std::lower_bound(m_begin, m_end, m_phrase, SearchNCSCompare());
    if(lowerBound != m_begin)
    {
        m_begin = lowerBound;
        modified = true;
    }

    auto upperBound = std::upper_bound(m_begin, m_end, m_phrase, SearchNCSCompare());
    if(upperBound != m_end)
    {
        m_end = upperBound;
        modified = true;
    }

    if(modified)
        emit this->contentChanged();
}

void QListQuery::setPhrase(const QString &text)
{
    m_phrase = text;

    m_begin = std::lower_bound(m_items.begin(), m_items.end(), m_phrase, SearchNCSCompare());
    m_end = std::upper_bound(m_items.begin(), m_items.end(), m_phrase, SearchNCSCompare());

    emit this->contentChanged();
}

QString QListQuery::itemName(int index) const
{
    auto ptr = this->item(index);
    if(ptr)
        return ptr->name();
    return QString();
}

QAbstractSearchableListItem* QListQuery::item(int index)
{
    if(index < 0 || index >= this->itemsCount())
        return nullptr;
    return *(m_begin + index);
}

const QAbstractSearchableListItem* QListQuery::item(int index) const
{
    if(index < 0 || index >= this->itemsCount())
        return nullptr;
    return *(m_begin + index);
}

int QListQuery::itemsCount() const
{
    return diff(m_begin, m_end);
}

void QListQuery::itemAboutToBeRemoved(QAbstractSearchableListItem *item)
{
    if((*m_begin) == item)
        ++m_begin;
    if((*m_end) == item)
        --m_end;
}

bool QListQuery::contains(const QString &name) const
{
    return std::binary_search(m_begin, m_end, name, Compare());
}

QListQuery::QListQuery(int flags, QSearchableListModel *parent, const ItemList &items)
    : QObject(parent), m_flags(flags), m_items(items)
{
    m_begin = m_items.begin();
    m_end = m_items.end();
}

/*
 * QSearchableListModel
 */

QSearchableListModel::QSearchableListModel(QObject *parent)
    : QAbstractListModel(parent), m_query(nullptr)
{
}

QSearchableListModel::~QSearchableListModel()
{
    delete m_query;
    for(auto ptr : m_items)
        delete ptr;
    m_items.clear();
}

int QSearchableListModel::rowCount(const QModelIndex &parent) const
{
    ((void)parent);
    if(m_query)
        return m_query->itemsCount();
    return m_items.size();
}

QVariant QSearchableListModel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::DisplayRole)
    {
        const QAbstractSearchableListItem* item = nullptr;
        if(m_query)
            item = m_query->item(index.row());
        else
            item = *(m_items.begin() + index.row());

        if(item)
            return item->name();
    }

    return QVariant();
}

QAbstractSearchableListItem* QSearchableListModel::item(int index)
{
    if(m_query)
        return m_query->item(index);
    return this->realItem(index);
}
QAbstractSearchableListItem* QSearchableListModel::realItem(int index)
{
    if(index < 0 || index >= m_items.size())
        return nullptr;
    return *(m_items.begin() + index);
}

void QSearchableListModel::addItem(const QString &value)
{
    this->addItem(new QSimpleSearchableListItem(value));
}

void QSearchableListModel::addItem(QAbstractSearchableListItem *item)
{
    auto iter = std::lower_bound(m_items.begin(), m_items.end(), item->name(), Compare());
    if(iter != m_items.end() && (*iter)->name() == item->name()) // iter might be equal to end() if the list is empty
    {
        delete (*iter);
        (*iter) = item;
        emit this->dataChanged(this->createIndex(0, 0, nullptr), this->createIndex(this->rowCount(QModelIndex()) - 1, 0, nullptr));
    }
    else
    {
        int index = diff(m_items.begin(), iter);
        this->beginInsertRows(QModelIndex(), index, index);
        m_items.insert(iter, item);
        this->endInsertRows();
    }
}

void QSearchableListModel::removeItem(const QString &name)
{
    auto range = std::equal_range(m_items.begin(), m_items.end(), name, Compare());
    int index = diff(m_items.begin(), range.first);
    this->beginRemoveRows(QModelIndex(), index, index);
    if(range.first != m_items.end() && m_query)
        m_query->itemAboutToBeRemoved(*range.first);
    delete (*range.first);
    m_items.erase(range.first, range.second);
    this->endRemoveRows();
}

void QSearchableListModel::clear()
{
    this->beginRemoveRows(QModelIndex(), 0, this->rowCount(QModelIndex()));
    delete m_query;
    m_query = nullptr;
    for(auto ptr : m_items)
        delete ptr;
    m_items.clear();
    this->endRemoveRows();
}

QListQuery* QSearchableListModel::beginQuery(int flags)
{
    if(m_query)
        return nullptr;
    m_query = new QListQuery(flags, this, m_items);
    this->connect(m_query, SIGNAL(contentChanged()), SLOT(queryContentChanged()));
    return m_query;
}

QListQuery* QSearchableListModel::query()
{
    return m_query;
}

void QSearchableListModel::endQuery()
{
    delete m_query;
    m_query = nullptr;

    QModelIndex none;
    int n = this->rowCount(none);
    this->beginInsertRows(none, 0, n - 1);
    this->endInsertRows();
}

void QSearchableListModel::queryContentChanged()
{
    emit this->dataChanged(this->createIndex(0, 0, nullptr), this->createIndex(this->rowCount(QModelIndex()) - 1, 0, nullptr));
}
