#include "qsearchablelistwidget.h"

QSearchableListWidget::QSearchableListWidget(QWidget *parent)
    : QListView(parent)
{
    QAbstractItemModel* prevModel = this->model();

    m_model = new QSearchableListModel(this);
    this->setModel(m_model);

    delete prevModel;
}

QSearchableListWidget::~QSearchableListWidget()
{
}

void QSearchableListWidget::insertItem(const QString &name)
{
    m_model->addItem(name);
}

void QSearchableListWidget::removeItem(const QString &name)
{
    m_model->removeItem(name);
}

void QSearchableListWidget::clear()
{
    m_model->clear();
}

QStringList QSearchableListWidget::selectedNames() const
{
    QModelIndexList indices = this->selectedIndexes();
    QStringList names;
    for(QModelIndex index : indices)
        names.push_back(m_model->item(index.row())->name());
    return names;
}

void QSearchableListWidget::setQueryText(const QString &text)
{
    if(text.isEmpty())
    {
        this->clearQuery();
        return;
    }

    QListQuery* query = m_model->query();
    if(!query)
        query = m_model->beginQuery(QListQuery::None);

    QString ph = query->phrase();
    if(text.startsWith(ph))
        query->appendToPhrase(text.right(text.size() - ph.size()));
    else
        query->setPhrase(text);
}

void QSearchableListWidget::clearQuery()
{
    m_model->endQuery();
}

void QSearchableListWidget::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    ((void)deselected);
    if(!selected.indexes().isEmpty())
    {
        emit this->itemSelected(m_model->item(selected.indexes().front().row()));
    }
}
