#include "ActionStack.hpp"

/*
 * IAction
 */

IAction::~IAction()
{
}

/*
 * ActionGroup
 */

ActionGroup::ActionGroup(const QString &name)
    : m_name(name), m_isMacro(false)
{
}

ActionGroup::~ActionGroup()
{
    for(IAction* ac : m_actions)
        delete ac;
}

QString ActionGroup::name() const
{
    return m_name;
}

bool ActionGroup::isMacro() const
{
    return m_isMacro;
}

void ActionGroup::addAction(IAction *action)
{
    m_actions.push_back(action);
}

void ActionGroup::exec()
{
    for(IAction* action : m_actions)
        action->exec();
}

void ActionGroup::undo()
{
    for(auto iter = m_actions.rbegin(); iter != m_actions.rend(); ++iter)
        (*iter)->undo();
}

ActionGroup::ActionGroup(const QString &name, bool isMacro)
    : m_name(name), m_isMacro(isMacro)
{
}

/*
 * ActionStack
 */

ActionStack::ActionStack(QObject* parent)
    : QObject(parent), m_macro(nullptr)
{
}

ActionStack::~ActionStack()
{
    this->clear();
}

void ActionStack::clear()
{
    while(!m_undos.empty())
    {
        delete m_undos.top();
        m_undos.pop();
    }

    while(!m_redos.empty())
    {
        delete m_redos.top();
        m_redos.pop();
    }

    emit this->undoAvailable(false);
    emit this->redoAvailable(false);
}

ActionGroup* ActionStack::beginMacro(const QString &name)
{
    if(m_macro)
        return nullptr;

    m_macro = new ActionGroup(name, true);
    return m_macro;
}

void ActionStack::endMacro(ActionGroup *macro)
{
    if(m_macro == macro)
    {
        m_macro = nullptr;
        this->exec(macro);
    }
}

void ActionStack::exec(ActionGroup *action)
{
    if(!action->isMacro()) // all member actions have been already executed.
        action->exec();

    if(m_macro)
    {
        m_macro->addAction(action);
    }
    else
    {
        m_undos.push(action);
        emit this->undoAvailable(true);
    }
}

void ActionStack::exec(const QString& name, const std::vector<IAction *> &action)
{
    ActionGroup* gr = new ActionGroup(name);
    for(IAction* a : action)
        gr->addAction(a);
    this->exec(gr);
}

bool ActionStack::anyUndos() const
{
    return !m_undos.empty();
}

bool ActionStack::anyRedos() const
{
    return !m_redos.empty();
}

QString ActionStack::topUndoName() const
{
    if(m_undos.empty())
        return QString();
    return m_undos.top()->name();
}

QString ActionStack::topRedoName() const
{
    if(m_redos.empty())
        return QString();
    return m_redos.top()->name();
}

void ActionStack::undo()
{
    if(!m_undos.empty())
    {
        m_undos.top()->undo();
        m_redos.push(m_undos.top());
        m_undos.pop();

        if(m_undos.empty())
            emit this->undoAvailable(false);
        else
            emit this->undoAvailable(true);
        emit this->redoAvailable(true);
    }
}

void ActionStack::redo()
{
    if(!m_redos.empty())
    {
        m_redos.top()->exec();
        m_undos.push(m_redos.top());
        m_redos.pop();

        if(m_redos.empty())
            emit this->redoAvailable(false);
        else
            emit this->redoAvailable(true);
        emit this->undoAvailable(true);
    }
}
