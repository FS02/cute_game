#ifndef PROJECTMODEL_HPP
#define PROJECTMODEL_HPP

#include <CuteEngine/World/GameWorld.hpp>
#include <QString>
#include <QObject>
#include <exception>

struct Version
{
    int vMajor = 0, vMinor = 0, vRevision = 0;

    bool operator ==(const Version& rv) const;
    bool operator !=(const Version& rv) const;
};

class ProjectProperties
{
public:
    QString author;
    Version ver;
    QString comments;

    bool operator ==(const ProjectProperties& rv) const;
};

class ProjectSaveException : public std::exception
{
public:
    const std::string information;

    ProjectSaveException(const std::string& info) noexcept;
    virtual ~ProjectSaveException() noexcept;

    virtual const char* what() const noexcept;
};

class ProjectModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString fileName READ fileName NOTIFY fileNameChanged)
    Q_PROPERTY(QString worldFile READ worldFile WRITE setWorldFile NOTIFY worldFileChanged)
    Q_PROPERTY(bool unsaved READ isUnsaved WRITE setUnsaved NOTIFY unsavedChanged)

public:
    ProjectModel();
    ProjectModel(const ProjectModel&) = delete;
    virtual ~ProjectModel();

    QString fileName() const;

    const ProjectProperties& properties() const;
    void setProperties(const ProjectProperties& props);

    QString worldFile() const;
    void setWorldFile(const QString& fn);
    cute::GameWorld* world();

    bool isUnsaved() const;
    void setUnsaved(bool unsaved);

    bool open(QWidget* parent);
    bool save(QWidget* parent);
    bool saveAs(QWidget* parent);

signals:
    void fileNameChanged();
    void worldFileChanged();
    void propertiesChanged();
    void unsavedChanged();

private:
    QString m_fileName;
    QString m_worldFile;
    ProjectProperties m_props;
    bool m_unsaved;

    cute::GameWorld* m_world;

    void _save();
};

#endif // PROJECTMODEL_HPP
