#include "ProjectModel.hpp"
#include <CuteEngine/World/Component/LocationComponent.hpp>
#include <CuteEngine/World/Component/GraphicsComponent.hpp>
#include <CuteEngine/World/Component/SoundComponent.hpp>
#include <CuteEngine/World/Component/PhysicsComponent.hpp>
#include <CuteEngine/World/Component/ScriptsComponent.hpp>
#include <QDir>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>

/*
 * Version
 */

bool Version::operator ==(const Version& rv) const
{
    if(vMajor != rv.vMajor)
        return false;
    if(vMinor != rv.vMinor)
        return false;
    if(vRevision != rv.vRevision)
        return false;
    return true;
}

bool Version::operator !=(const Version& rv) const
{
    return !(*this == rv);
}

/*
 * ProjectProperties
 */

bool ProjectProperties::operator ==(const ProjectProperties& rv) const
{
    if(author != rv.author)
        return false;
    if(ver != rv.ver)
        return false;
    if(comments != rv.comments)
        return false;
    return true;
}

/*
 * ProjectSaveException
 */

ProjectSaveException::ProjectSaveException(const std::string &info) noexcept
    : information(info)
{
}

ProjectSaveException::~ProjectSaveException() noexcept
{
}

const char* ProjectSaveException::what() const noexcept
{
    return information.c_str();
}

/*
 * ProjectModel
 */

ProjectModel::ProjectModel()
    : m_fileName("<unnamed>"), m_unsaved(false)
{
    m_world = new cute::GameWorld(true);

    m_world->addComponentMgr(new cute::LocationCompMgr());
    m_world->addComponentMgr(new cute::GraphicsComponentMgr());
    m_world->addComponentMgr(new cute::SoundComponentMgr());
    m_world->addComponentMgr(new cute::PhysicsCompMgr());
    m_world->addComponentMgr(new cute::ScriptsComponentMgr());
    // add other component managers here
    m_world->initialize();
}

ProjectModel::~ProjectModel()
{
    delete m_world;
}

QString ProjectModel::fileName() const
{
    return m_fileName;
}

const ProjectProperties& ProjectModel::properties() const
{
    return m_props;
}

void ProjectModel::setProperties(const ProjectProperties &props)
{
    if(m_props == props)
        return;

    m_props = props;
    emit propertiesChanged();
    this->setUnsaved(true);
}

QString ProjectModel::worldFile() const
{
    return m_worldFile;
}

void ProjectModel::setWorldFile(const QString &fn)
{
    if(m_worldFile == fn)
        return;

    m_worldFile = fn;
    emit this->worldFileChanged();
    this->setUnsaved(true);
}

cute::GameWorld* ProjectModel::world()
{
    return m_world;
}

bool ProjectModel::isUnsaved() const
{
    return m_unsaved;
}

void ProjectModel::setUnsaved(bool unsaved)
{
    m_unsaved = unsaved;
    emit unsavedChanged();
}

bool ProjectModel::open(QWidget* parent)
{
    QString fn = QFileDialog::getOpenFileName(parent, "Open", m_fileName, "*.cepro");
    if(fn.isEmpty())
        return false;

    QFile file(fn);
    if(!file.open(QFile::ReadOnly))
        return false;

    QByteArray data = file.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if(!doc.isObject())
        return false;

    m_fileName = fn;

    QDir parentDir(m_fileName);
    parentDir.cdUp();
    cute::Paths::SetRoot(parentDir.path().toStdString());

    QJsonObject root = doc.object();

    QString worldFn = root.value("World").toString();
    this->setWorldFile(worldFn);

    QString absWorldFn = parentDir.absoluteFilePath(m_worldFile);
    m_world->clear();
    if(!m_worldFile.isEmpty())
        m_world->loadScene(absWorldFn.toStdString());

    QJsonObject props = root.value("Properties").toObject();
    {
        ProjectProperties pp;
        pp.author = props.value("Author").toString();
        QJsonArray ver = props.value("Version").toArray();
        pp.ver.vMajor = ver.at(0).toDouble();
        pp.ver.vMinor = ver.at(1).toDouble();
        pp.ver.vRevision = ver.at(2).toDouble();
        pp.comments = props.value("Comments").toString();
        m_props = pp;
    }

    emit fileNameChanged();
    this->setUnsaved(false);
    return true;
}

bool ProjectModel::save(QWidget* parent)
{
    QFile file(m_fileName);
    if(file.exists())
    {
        try
        {
            this->_save();
        }
        catch(const std::exception& exc)
        {
            QString msg = "Could not save the project!\n";
            msg += exc.what();
            QMessageBox::critical(parent, "CuteEngine editor - error", msg, QMessageBox::Ok);
            return false;
        }

        return true;
    }
    else
        return this->saveAs(parent);
}

bool ProjectModel::saveAs(QWidget* parent)
{
    QString fn = QFileDialog::getSaveFileName(parent, "Save as", m_fileName, "*.cepro");
    if(fn.isEmpty())
        return false;

    QString prevFn = m_fileName;
    m_fileName  = fn;
    try
    {
        this->_save();
    }
    catch(const std::exception& exc)
    {
        m_fileName = prevFn;
        QString msg = "Could not save the project!\n";
        msg += exc.what();
        QMessageBox::critical(parent, "CuteEngine editor - error", msg, QMessageBox::Ok);
        return false;
    }

    emit fileNameChanged();
    return true;
}

void ProjectModel::_save()
{    
    QFile file(m_fileName);
    if(!file.open(QFile::WriteOnly))
        throw ProjectSaveException("Could not open the file!");

    if(m_worldFile.isEmpty())
        throw ProjectSaveException("World file has not been set!");

    QDir parentDir(m_fileName);
    parentDir.cdUp();
    QString worldFn = parentDir.absoluteFilePath(m_worldFile);
    m_world->writeScene(worldFn.toStdString());

    QJsonObject root;

    root.insert("World", m_worldFile);

    QJsonObject props;
    props.insert("Author", QJsonValue(m_props.author));
    QJsonArray ver;
    ver.push_back(QJsonValue(m_props.ver.vMajor));
    ver.push_back(m_props.ver.vMinor);
    ver.push_back(m_props.ver.vRevision);
    props.insert("Version", ver);
    props.insert("Comments", m_props.comments);

    root.insert("Properties", props);

    QByteArray data = QJsonDocument(root).toJson();

    file.write(data);
    this->setUnsaved(false);
}
