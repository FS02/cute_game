#ifndef PROJECTPROPSEDITOR_HPP
#define PROJECTPROPSEDITOR_HPP

#include "Project/ProjectModel.hpp"
#include <QDialog>

namespace Ui {
    class ProjectPropsEditor;
}

class ProjectPropsEditor : public QDialog
{
    Q_OBJECT

public:
    explicit ProjectPropsEditor(ProjectModel* pro, QWidget *parent = 0);
    ~ProjectPropsEditor();

public slots:
    void accept();

private:
    Ui::ProjectPropsEditor *ui;
    ProjectModel* m_pro;

private slots:
    void _browseWorldFile();
};

#endif // PROJECTPROPSEDITOR_HPP
