#include "SetNameDialog.hpp"
#include "ui_SetNameDialog.h"

SetNameDialog::SetNameDialog(const QString& msg, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetNameDialog)
{
    ui->setupUi(this);
    ui->message->setText(msg);
}

SetNameDialog::~SetNameDialog()
{
    delete ui;
}

QString SetNameDialog::selectedName() const
{
    return m_name;
}

void SetNameDialog::accept()
{
    m_name = ui->nameEdit->text();

    if(!m_name.isEmpty())
    {
        this->done(QDialog::Accepted);
        this->hide();
    }
}
