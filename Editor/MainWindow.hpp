#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "Project/ProjectModel.hpp"
#include "ActionStack.hpp"
#include <QMainWindow>
#include <QSearchableListWidget>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow, public cute::IGameWorldMonitor
{
    Q_OBJECT

public:
    enum GameMode
    {
        GM_Editor,
        GM_Simulation
    };

    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

    virtual void onEntityAdded(const cute::Entity *ent);
    virtual void onEntityRemoved(const cute::Entity *ent);

private:
    Ui::MainWindow *ui;
    ActionStack* m_actionStack;
    ActionGroup* m_macro;
    ProjectModel* m_pro;
    GameMode m_mode;
    cute::Entity* m_selectedEnt;
    std::vector<QWidget*> m_editors;

    void _setProCtrlEnabled(bool e);
    void _updateEntProps();

    virtual void closeEvent(QCloseEvent *evt);
    virtual void childEvent(QChildEvent *evt);

private slots:
    void _newProject();
    void _closeProject();

    void _open();
    void _save();
    void _saveAs();

    void _setTitle();

    void _editProperties();

    void _storeWindowGeom();
    void _restoreWindowGeom();

    void _addEntity();
    void _removeEntity();
    void _entitySelected(const QAbstractSearchableListItem* item);

    void _changeMode(GameMode mode);
    void _toggleFrozen();
    void _toggleDebugPhysics(bool e);

    void _openEntityTypeEditor();

    void _undoAvailable(bool available);
    void _redoAvailable(bool available);
    void _helpAndAbout();
};

#endif // MAINWINDOW_HPP
