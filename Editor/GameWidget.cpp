#include "GameWidget.hpp"
#include "QtRenderWindow.hpp"
#include <SDL.h>

GameWidget::GameWidget(QWidget *parent)
    : QGLWidget(parent), m_window(new QtRenderWindow(this)), m_graph(nullptr), m_world(nullptr), m_frozenWorld(true)
{
    this->setRefreshRate(30);
    SDL_Init(SDL_INIT_AUDIO);
}

GameWidget::~GameWidget()
{
    delete m_graph;
    delete m_window;
    SDL_Quit();
}

void GameWidget::setWorld(cute::GameWorld *world)
{
    m_world = world;
}

void GameWidget::setRefreshRate(int fps)
{
    m_timer.setInterval(1000/fps);
}

void GameWidget::freezeWorld()
{
    m_frozenWorld = true;
}

void GameWidget::unfreezeWorld()
{
    m_frozenWorld = false;
}

bool GameWidget::isWorldFrozen() const
{
    return m_frozenWorld;
}

void GameWidget::initializeGL()
{
    m_graph = new cute::Graphics(m_window);

    this->connect(&m_timer, SIGNAL(timeout()), SLOT(updateGL()));
    m_timer.start();
}

void GameWidget::paintGL()
{
    m_graph->clear(0, 0, 0, 0);

    if(m_world)
    {
        double delta = 0.0;
        if(!m_frozenWorld)
            delta = ((double)m_timer.interval())/1000.0;

        m_graph->begin();
        m_world->update(delta);
        m_graph->end();
    }
}
