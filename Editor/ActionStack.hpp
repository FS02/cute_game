#ifndef ACTIONSTACK_HPP
#define ACTIONSTACK_HPP

#include <QObject>
#include <stack>
#include <vector>

class IAction
{
public:
    virtual ~IAction();

    virtual void exec() = 0;
    virtual void undo() = 0;
};

class ActionGroup : public IAction
{
    friend class ActionStack;

public:
    ActionGroup(const QString& name);
    ~ActionGroup();

    QString name() const;
    bool isMacro() const;

    void addAction(IAction* action);

    virtual void exec();
    virtual void undo();

private:
    QString m_name;
    bool m_isMacro;
    std::vector<IAction*> m_actions;

    ActionGroup(const QString &name, bool isMacro);
};

class ActionStack : public QObject
{
    Q_OBJECT

public:
    ActionStack(QObject* parent = nullptr);
    virtual ~ActionStack();

    void clear();

    ActionGroup* beginMacro(const QString& name);
    void endMacro(ActionGroup* macro);

    void exec(ActionGroup* action);
    void exec(const QString& name, const std::vector<IAction*>& action);

    bool anyUndos() const;
    bool anyRedos() const;

    QString topUndoName() const;
    QString topRedoName() const;

public slots:
    void undo();
    void redo();

signals:
    void undoAvailable(bool available);
    void redoAvailable(bool available);

private:
    std::stack<ActionGroup*> m_undos;
    std::stack<ActionGroup*> m_redos;
    ActionGroup* m_macro;
};

#endif // ACTIONSTACK_HPP
