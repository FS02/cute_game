#include "EntityActions.hpp"
#include <iostream>

/*
 * AddEntityAction
 */

AddEntityAction::AddEntityAction(cute::GameWorld *world, const std::string &typeName, const std::string &entityName)
    : m_world(world), m_typeName(typeName), m_entityName(entityName)
{
}

void AddEntityAction::exec()
{
    m_world->createEntity(m_entityName, m_typeName);
}

void AddEntityAction::undo()
{
    cute::Entity* ent = m_world->entity(m_entityName);
    if(ent)
        ent->markAsRedundant();
    else
        std::clog << "[AddEntityAction::undo] Something went wrong! There is no entity called " << m_entityName << "\n";
}

/*
 * RemoveEntityAction
 */

RemoveEntityAction::RemoveEntityAction(cute::Entity *entity)
{
    m_world = entity->world();
    m_name = entity->name();
    m_descName = entity->description()->fileName();
}

void RemoveEntityAction::exec()
{
    cute::Entity* ent = m_world->entity(m_name);
    if(ent)
    {
        m_state.clear();
        ent->saveState(m_state);
        ent->markAsRedundant();
    }
}

void RemoveEntityAction::undo()
{
    cute::Entity* ent = m_world->createEntity(m_name, m_descName);
    if(ent)
        ent->loadState(m_state);
    else
        std::clog << "Error! An entity with name " << m_name << " already exists, cannot undo!\n";
}

/*
 * SetEntityPropertyAction
 */

SetEntityPropertyAction::SetEntityPropertyAction(cute::Entity *ent, const cute::Property *prop, const cute::Value& newVal)
{
    const cute::IComponent* comp = (const cute::IComponent*)prop->owner();

    m_world = ent->world();
    m_entityName = ent->name();
    m_compType = comp->typeName();
    m_propName = prop->name();
    m_oldVal = prop->toValue();
    m_newVal = newVal;
}

void SetEntityPropertyAction::exec()
{
    cute::Entity* ent = m_world->entity(m_entityName);
    cute::IComponent* comp = ent->componentByType(m_compType);
    comp->setProperty(m_propName, m_newVal);
}

void SetEntityPropertyAction::undo()
{
    cute::Entity* ent = m_world->entity(m_entityName);
    cute::IComponent* comp = ent->componentByType(m_compType);
    comp->setProperty(m_propName, m_oldVal);
}

/*
 * StoreWorldStateAction
 */

StoreWorldStateAction::StoreWorldStateAction(cute::GameWorld *world)
    : m_world(world)
{
}

void StoreWorldStateAction::exec()
{
    if(m_state.empty())
    {
        std::function<bool(cute::Entity*)> func = std::bind(&StoreWorldStateAction::_storeEntState, this, std::placeholders::_1);
        m_world->forEachEntity(func);
    }
    else
    {
        for(auto& pr : m_state)
        {
            cute::Entity* ent = m_world->entity(pr.first);
            if(ent)
            {
                cute::ConfStorage temp;
                ent->saveState(temp);
                ent->loadState(pr.second);
                pr.second = std::move(temp);
            }
            else
            {
                std::clog << "[StoreWorldStateAction::undo] Something went wrong! There is no entity called " << pr.first << "\n";
            }
        }
    }
}

void StoreWorldStateAction::undo()
{
    for(auto& pr : m_state)
    {
        cute::Entity* ent = m_world->entity(pr.first);
        if(ent)
        {
            cute::ConfStorage temp;
            ent->saveState(temp);
            ent->loadState(pr.second);
            pr.second = std::move(temp);
        }
        else
        {
            std::clog << "[StoreWorldStateAction::undo] Something went wrong! There is no entity called " << pr.first << "\n";
        }
    }
}

bool StoreWorldStateAction::_storeEntState(cute::Entity *ent)
{
    cute::ConfStorage conf;
    ent->saveState(conf);
    m_state[ent->name()] = std::move(conf);
    return true;
}
