#ifndef SETNAMEDIALOG_HPP
#define SETNAMEDIALOG_HPP

#include <QDialog>

namespace Ui {
    class SetNameDialog;
}

class SetNameDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetNameDialog(const QString& msg = "Enter name", QWidget *parent = 0);
    ~SetNameDialog();

    QString selectedName() const;

public slots:
    virtual void accept();

private:
    Ui::SetNameDialog *ui;
    QString m_name;
};

#endif // SETNAMEDIALOG_HPP
