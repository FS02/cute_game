#ifndef ENTITYTYPEWIZARD_HPP
#define ENTITYTYPEWIZARD_HPP

#include <CuteEngine/World/GameWorld.hpp>
#include <QDialog>

namespace Ui {
    class EntityTypeWizard;
}

class EntityTypeWizard : public QDialog
{
    Q_OBJECT

public:
    explicit EntityTypeWizard(cute::GameWorld* world, cute::EntityDesc* entDesc = 0, QWidget *parent = 0);
    ~EntityTypeWizard();

public slots:
    virtual void accept();

private:
    Ui::EntityTypeWizard *ui;
    cute::GameWorld* m_world;
    cute::EntityDesc* m_entDesc;

private slots:
    void _browseDescFile();

    void _addComponentType();
    void _addComponentType(const QString& typeName);
    void _removeComponentType();
    void _removeAllComponentTypes();
};

#endif // ENTITYTYPEWIZARD_HPP
