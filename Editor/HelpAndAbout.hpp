#ifndef HELPANDABOUT_HPP
#define HELPANDABOUT_HPP

#include <QDialog>

namespace Ui {
    class HelpAndAbout;
}

class HelpAndAbout : public QDialog
{
    Q_OBJECT

public:
    explicit HelpAndAbout(QWidget *parent = 0);
    ~HelpAndAbout();

private:
    Ui::HelpAndAbout *ui;
};

#endif // HELPANDABOUT_HPP
