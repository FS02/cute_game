#include "ProjectPropsEditor.hpp"
#include "ui_ProjectPropsEditor.h"
#include <QDir>
#include <QFileDialog>

ProjectPropsEditor::ProjectPropsEditor(ProjectModel* pro, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProjectPropsEditor),
    m_pro(pro)
{
    ui->setupUi(this);

    this->connect(ui->browseWorldFile, SIGNAL(clicked()), SLOT(_browseWorldFile()));

    const ProjectProperties& props = m_pro->properties();

    ui->authorEdit->setText(props.author);
    ui->majorVerBox->setValue(props.ver.vMajor);
    ui->minorVerBox->setValue(props.ver.vMinor);
    ui->revisionVerBox->setValue(props.ver.vRevision);
    ui->commentsEdit->setPlainText(props.comments);
    ui->worldFile->setText(m_pro->worldFile());
}

ProjectPropsEditor::~ProjectPropsEditor()
{
    delete ui;
}

void ProjectPropsEditor::accept()
{
    ProjectProperties props;
    props.author = ui->authorEdit->text();
    props.ver.vMajor = ui->majorVerBox->value();
    props.ver.vMinor = ui->minorVerBox->value();
    props.ver.vRevision = ui->revisionVerBox->value();
    props.comments = ui->commentsEdit->toPlainText();
    m_pro->setProperties(props);
    m_pro->setWorldFile(ui->worldFile->text());

    this->hide();
    this->setResult(QDialog::Accepted);
}

void ProjectPropsEditor::_browseWorldFile()
{
    QDir parentDir(m_pro->fileName());
    parentDir.cdUp();
    QString fn = QFileDialog::getSaveFileName(this, "CuteEngine editor", parentDir.path(), "*.ceworld");
    if(!fn.isEmpty())
        ui->worldFile->setText(parentDir.relativeFilePath(fn));
}
