#include "qlineeditex.h"
#include <QToolButton>
#include <QStyle>

QLineEditEx::QLineEditEx(QWidget *parent)
    : QLineEdit(parent), m_hasClearBtt(true)
{
    m_clearBtt = new QToolButton(this);
    m_clearBtt->setCursor(Qt::ArrowCursor);
    QPixmap pixmap(":/Data/clean.png");
    m_clearBtt->setIcon(QIcon(pixmap));
    m_clearBtt->setStyleSheet("QToolButton { border: none; padding: 0px; }");
    m_clearBtt->hide();

    this->connect(m_clearBtt, SIGNAL(clicked()), SLOT(clear()));
    this->connect(this, SIGNAL(textChanged(QString)), SLOT(updateButtons(QString)));
}

bool QLineEditEx::hasClearButton() const
{
    return m_hasClearBtt;
}

void QLineEditEx::toggleClearButton(bool enable)
{
    m_hasClearBtt = enable;
    this->updateButtons(this->text());
    emit this->hasClearButtonChanged();
}

void QLineEditEx::resizeEvent(QResizeEvent *)
{
    QSize sz = m_clearBtt->sizeHint();
    int frameWidth = this->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    m_clearBtt->move(this->rect().right() - frameWidth - sz.width(), (this->rect().bottom() + 1 - sz.height())/2);
}

void QLineEditEx::updateButtons(const QString &text)
{
    m_clearBtt->setVisible(m_hasClearBtt && (!text.isEmpty()));
}
