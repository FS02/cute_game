#ifndef QLINEEDITEX_H
#define QLINEEDITEX_H

#include <QLineEdit>

class QLineEditEx : public QLineEdit
{
    Q_OBJECT
    Q_PROPERTY(bool hasClearButton READ hasClearButton WRITE toggleClearButton NOTIFY hasClearButtonChanged)

public:
    QLineEditEx(QWidget* parent = 0);

    bool hasClearButton() const;
    void toggleClearButton(bool enable);

signals:
    void hasClearButtonChanged();

private:
    bool m_hasClearBtt;
    class QToolButton* m_clearBtt;

    void resizeEvent(QResizeEvent *);

private slots:
    void updateButtons(const QString& text);
};

#endif // QLINEEDITEX_H
