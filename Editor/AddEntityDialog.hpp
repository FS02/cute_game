#ifndef ADDENTITYDIALOG_HPP
#define ADDENTITYDIALOG_HPP

#include <QDialog>
#include <CuteEngine/World/GameWorld.hpp>
#include "EntityTypeEditor.hpp"

class AddEntityDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddEntityDialog(cute::GameWorld* world, QWidget* parent = 0);
    virtual ~AddEntityDialog();

    QString selectedEntityName() const;
    QString selectedEntityType() const;

public slots:
    virtual void accept();

private:
    EntityTypeEditor* m_editor;
    QString m_selectedEntityName;
    QString m_selectedEntityType;
};

#endif // ADDENTITYDIALOG_HPP
