#include "EntityTypeWizard.hpp"
#include "ui_EntityTypeWizard.h"
#include <CuteEngine/World/ComponentDescFactory.hpp>
#include <CuteEngine/Core/Paths.hpp>
#include <QFileDialog>
#include <QMessageBox>

EntityTypeWizard::EntityTypeWizard(cute::GameWorld* world, cute::EntityDesc* entDesc, QWidget *parent)
    :QDialog(parent), ui(new Ui::EntityTypeWizard), m_world(world), m_entDesc(entDesc)
{
    ui->setupUi(this);

    this->connect(ui->browseDescFileBtt, SIGNAL(clicked()), SLOT(_browseDescFile()));
    this->connect(ui->addCompTypeBtt, SIGNAL(clicked()), SLOT(_addComponentType()));
    this->connect(ui->removeCompTypeBtt, SIGNAL(clicked()), SLOT(_removeComponentType()));
    this->connect(ui->removeAllCompTypesBtt, SIGNAL(clicked()), SLOT(_removeAllComponentTypes()));

    auto creators = cute::ComponentDescFactory::Instance().listCreators();
    for(const cute::IComponentDescCreator* type : creators)
        ui->componentTypes->insertItem(QString::fromStdString(type->typeName()));

    if(m_entDesc)
    {
        ui->descFileName->setText(QString::fromStdString(m_entDesc->fileName()));
        int i;
        cute::IComponentDesc* comp;
        for(i = 0, comp = m_entDesc->component(0); comp; comp = m_entDesc->component(++i))
            this->_addComponentType(QString::fromStdString(comp->typeName));
    }
}

EntityTypeWizard::~EntityTypeWizard()
{
    delete ui;
}

void EntityTypeWizard::accept()
{
    if(ui->descFileName->text().isEmpty())
    {
        QMessageBox::information(this, "CuteEngine", "Enter file name", QMessageBox::Ok);
        return;
    }

    cute::EntityDesc desc(ui->descFileName->text().toStdString());
    for(int i = 0; i < ui->addedCompList->count(); ++i)
    {
        QString type = ui->addedCompList->item(i)->data(Qt::DisplayRole).toString();
        desc.addComponent(cute::ComponentDescFactory::Instance().createDesc(type.toStdString()));
    }
    desc.write();

    m_world->entityDescMgr().resource(desc.fileName());

    this->hide();
    this->done(QDialog::Accepted);
}

void EntityTypeWizard::_browseDescFile()
{
    QString fn = QFileDialog::getSaveFileName(this, "CuteEngine", QString(), "*.ceent");
    if(!fn.isEmpty())
    {
        QDir root(QString::fromStdString(cute::Paths::Root()));
        fn = root.relativeFilePath(fn);
        if(!fn.endsWith(".ceent"))
            fn += ".ceent";
        ui->descFileName->setText(fn);
    }
}

void EntityTypeWizard::_addComponentType()
{
    QStringList selTypes = ui->componentTypes->selectedNames();
    for(const QString& type : selTypes)
    {
        ui->addedCompList->addItem(type);
        ui->componentTypes->removeItem(type);
    }
}

void EntityTypeWizard::_addComponentType(const QString &typeName)
{
    ui->componentTypes->removeItem(typeName);
    ui->addedCompList->addItem(typeName);
}

void EntityTypeWizard::_removeComponentType()
{
    QList<QListWidgetItem*> items = ui->addedCompList->selectedItems();
    for(QListWidgetItem* item : items)
    {
        ui->componentTypes->insertItem(item->data(Qt::DisplayRole).toString());
        ui->addedCompList->takeItem(ui->addedCompList->row(item));
        delete item;
    }
}

void EntityTypeWizard::_removeAllComponentTypes()
{
    while(ui->addedCompList->count())
    {
        QListWidgetItem* item = ui->addedCompList->item(0);
        ui->componentTypes->insertItem(item->data(Qt::DisplayRole).toString());
        ui->addedCompList->takeItem(0);
        delete item;
    }
}
