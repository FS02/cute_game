#include "AddEntityDialog.hpp"
#include "SetNameDialog.hpp"
#include <QDialogButtonBox>
#include <QVBoxLayout>

AddEntityDialog::AddEntityDialog(cute::GameWorld* world, QWidget *parent)
    : QDialog(parent)
{
    this->resize(640, 480);

    QVBoxLayout* layout = new QVBoxLayout();

    m_editor = new EntityTypeEditor(world, this);
    layout->addWidget(m_editor);

    QDialogButtonBox* buttons = new QDialogButtonBox(this);
    buttons->addButton("Select", QDialogButtonBox::AcceptRole);
    buttons->addButton("Cancel", QDialogButtonBox::RejectRole);
    layout->addWidget(buttons);

    this->connect(buttons, SIGNAL(accepted()), SLOT(accept()));
    this->connect(buttons, SIGNAL(rejected()), SLOT(reject()));

    this->setLayout(layout);
}

AddEntityDialog::~AddEntityDialog()
{
}

QString AddEntityDialog::selectedEntityName() const
{
    return m_selectedEntityName;
}

QString AddEntityDialog::selectedEntityType() const
{
    return m_selectedEntityType;
}

void AddEntityDialog::accept()
{
    m_selectedEntityType = m_editor->selectedEntityType();

    if(m_selectedEntityType.isEmpty())
        return;

    SetNameDialog* dlg = new SetNameDialog("Enter entity's name", this);
    if(dlg->exec() == QDialog::Accepted)
    {
        m_selectedEntityName = dlg->selectedName();
        delete dlg;
    }
    else
        return;

    this->hide();
    this->done(QDialog::Accepted);
}
