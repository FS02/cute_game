#-------------------------------------------------
#
# Project created by QtCreator 2013-09-23T00:55:00
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Editor
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11 -W -Wall -Wextra -pedantic

INCLUDEPATH += .. qpropertysheet qsearchablelist qlineeditex ../CuteEngine/Squirrel/include
LIBS += -L../build-CuteEngine-Desktop-Debug -lCuteEngine -lBox2D

unix{
    INCLUDEPATH += /usr/include/SDL2 /usr/local/include/SDL2
    LIBS += -lSDL2 -lSDL2_mixer -lSDL2_image -lGL -ldl
}

win32{
}

SOURCES += \
    qpropertysheet/qvariantconv.cpp \
    qpropertysheet/qpropertysheet.cpp \
    qpropertysheet/qpropertymodel.cpp \
    qpropertysheet/qpropertyeditors.cpp \
    qpropertysheet/qproperty.cpp \
    Project/ProjectModel.cpp \
    Main.cpp \
    MainWindow.cpp \
    ProjectPropsEditor.cpp \
    EntityTypeEditor.cpp \
    GameWidget.cpp \
    qsearchablelist/qsearchablelistmodel.cpp \
    qsearchablelist/qsearchablelistwidget.cpp \
    CuteProperty.cpp \
    qpropertysheet/qnumproperty.cpp \
    ActionStack.cpp \
    EntityActions.cpp \
    HelpAndAbout.cpp \
    qlineeditex/qlineeditex.cpp \
    EntityTypeWizard.cpp \
    AddEntityDialog.cpp \
    SetNameDialog.cpp

HEADERS  += \
    qpropertysheet/QVariantConv \
    qpropertysheet/qvariantconv.h \
    qpropertysheet/QPropertySheet \
    qpropertysheet/qpropertysheet.h \
    qpropertysheet/qpropertymodel.h \
    qpropertysheet/qpropertyeditors.h \
    qpropertysheet/qproperty.h \
    Project/ProjectModel.hpp \
    MainWindow.hpp \
    ProjectPropsEditor.hpp \
    EntityTypeEditor.hpp \
    GameWidget.hpp \
    QtRenderWindow.hpp \
    qsearchablelist/qsearchablelistmodel.h \
    qsearchablelist/qsearchablelistwidget.h \
    qsearchablelist/QSearchableListWidget \
    CuteProperty.hpp \
    qpropertysheet/qnumproperty.h \
    ActionStack.hpp \
    EntityActions.hpp \
    HelpAndAbout.hpp \
    qlineeditex/qlineeditex.h \
    qlineeditex/QLineEditEx \
    EntityTypeWizard.hpp \
    AddEntityDialog.hpp \
    SetNameDialog.hpp

FORMS    += \
    MainWindow.ui \
    ProjectPropsEditor.ui \
    EntityTypeEditor.ui \
    HelpAndAbout.ui \
    EntityTypeWizard.ui \
    SetNameDialog.ui

RESOURCES += \
    Data.qrc
