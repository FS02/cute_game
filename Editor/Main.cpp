#define CUTE_EDITOR_MODE
#include "MainWindow.hpp"
#include <QApplication>
#include <QStyleFactory>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    qApp->setStyle(QStyleFactory::create("Fusion"));

    MainWindow* wnd = new MainWindow(nullptr);
    wnd->show();

    return app.exec();
}
