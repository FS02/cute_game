#include "EntityTypeEditor.hpp"
#include "ui_EntityTypeEditor.h"
#include "EntityTypeWizard.hpp"

EntityTypeEditor::EntityTypeEditor(cute::GameWorld* world, QWidget *parent, Qt::WindowFlags flags) :
    QWidget(parent, flags), ui(new Ui::EntityTypeEditor), m_world(world)
{
    ui->setupUi(this);

    this->connect(ui->addBtt, SIGNAL(clicked()), SLOT(addNewEntityType()));
    this->connect(ui->removeBtt, SIGNAL(clicked()), SLOT(removeEntityType()));

    std::function<bool(cute::EntityDesc*)> func = std::bind(&EntityTypeEditor::addEntityDesc, this, std::placeholders::_1);
    m_world->entityDescMgr().forEachResource(func);
    m_world->entityDescMgr().addMonitor(this);
}

EntityTypeEditor::~EntityTypeEditor()
{
    m_world->entityDescMgr().removeMonitor(this);
    delete ui;
}

QString EntityTypeEditor::selectedEntityType() const
{
    QStringList types = ui->typeList->selectedNames();
    if(!types.isEmpty())
        return types.front();
    return QString();
}

bool EntityTypeEditor::addEntityDesc(cute::EntityDesc *desc)
{
    ui->typeList->insertItem(QString::fromStdString(desc->fileName()));
    return true;
}

void EntityTypeEditor::onResourceAdded(cute::EntityDesc *ptr)
{
    ui->typeList->insertItem(QString::fromStdString(ptr->fileName()));
}

void EntityTypeEditor::addNewEntityType()
{
    EntityTypeWizard* wiz = new EntityTypeWizard(m_world, nullptr, this);
    wiz->exec();
}

void EntityTypeEditor::removeEntityType()
{
}
