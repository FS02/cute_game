#include "MainWindow.hpp"
#include "ui_MainWindow.h"
#include "ProjectPropsEditor.hpp"
#include "EntityTypeEditor.hpp"
#include "CuteProperty.hpp"
#include "EntityActions.hpp"
#include "HelpAndAbout.hpp"
#include "EntityTypeEditor.hpp"
#include "AddEntityDialog.hpp"
#include <CuteEngine/World/Component/PhysicsComponent.hpp>
#include <QMessageBox>
#include <QCloseEvent>
#include <QVBoxLayout>
#include <QSettings>

namespace
{
    const char* g_orgName = "CuteEngine Dev Team";
    const char* g_appName = "CuteEngine Editor";
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow), m_pro(nullptr), m_mode(GM_Editor), m_selectedEnt(nullptr)
{
    ui->setupUi(this);
    this->_setTitle();
    this->_setProCtrlEnabled(false);

    this->connect(ui->actionNew_project, SIGNAL(triggered()), SLOT(_newProject()));
    this->connect(ui->actionOpen_project, SIGNAL(triggered()), SLOT(_open()));
    this->connect(ui->actionQuit, SIGNAL(triggered()), SLOT(close()));

    m_actionStack = new ActionStack(this);
    m_macro = nullptr;
    m_actionStack->connect(ui->actionUndo, SIGNAL(triggered()), SLOT(undo()));
    m_actionStack->connect(ui->actionRedo, SIGNAL(triggered()), SLOT(redo()));
    ui->actionUndo->connect(m_actionStack, SIGNAL(undoAvailable(bool)), SLOT(setEnabled(bool)));
    ui->actionRedo->connect(m_actionStack, SIGNAL(redoAvailable(bool)), SLOT(setEnabled(bool)));
    this->connect(m_actionStack, SIGNAL(undoAvailable(bool)), SLOT(_undoAvailable(bool)));
    this->connect(m_actionStack, SIGNAL(redoAvailable(bool)), SLOT(_redoAvailable(bool)));

    this->_restoreWindowGeom();
}

MainWindow::~MainWindow()
{
    delete m_pro;
    delete ui;
}

void MainWindow::onEntityAdded(const cute::Entity *ent)
{
    QString name(ent->name().c_str());
    ui->entityList->insertItem(name);
}

void MainWindow::onEntityRemoved(const cute::Entity *ent)
{
    if(m_selectedEnt == ent)
    {
        m_selectedEnt = nullptr;
        this->_updateEntProps();
    }

    QString name(ent->name().c_str());
    ui->entityList->removeItem(name);
}

void MainWindow::_setProCtrlEnabled(bool e)
{
    ui->actionSave_project->setEnabled(e);
    ui->actionSave_project_as->setEnabled(e);
    ui->actionClose_project->setEnabled(e);
    ui->actionEdit_properties->setEnabled(e);
    ui->actionEdit_data_folders->setEnabled(e);

    ui->actionUndo->setEnabled(e);
    ui->actionRedo->setEnabled(e);

    ui->actionEntity_type_editor->setEnabled(e);

    ui->worldControls->setEnabled(e);
}

void MainWindow::_updateEntProps()
{
    if(m_selectedEnt)
    {
        ui->selectedEntName->setText(m_selectedEnt->name().c_str());
        ui->selectedEntProps->clear();
        BuildPropertyTree(m_actionStack, m_selectedEnt, ui->selectedEntProps->rootGroup());
    }
    else
    {
        ui->selectedEntName->setText(QString());
        ui->selectedEntProps->clear();
    }
}

void MainWindow::closeEvent(QCloseEvent *evt)
{
    this->_storeWindowGeom();

    if(m_pro)
    {
        if(!m_pro->isUnsaved())
        {
            evt->accept();
            return;
        }

        QMessageBox::StandardButtons buttons = QMessageBox::Save|QMessageBox::Discard|QMessageBox::Cancel;
        QMessageBox::StandardButton defBtt = QMessageBox::Cancel;
        int res = QMessageBox::question(this, "CuteEngine editor", "Do you want to save changes before quitting?", buttons, defBtt);
        if(res == QMessageBox::Save)
        {
            bool ask = m_pro->isUnsaved();
            while(ask)
            {
                if(!m_pro->save(this))
                {
                    int btt = QMessageBox::question(this, "CuteEngine editor", "Could not save changes. Do you want to discard them?", QMessageBox::Yes|QMessageBox::No, QMessageBox::No);
                    if(btt == QMessageBox::Yes)
                        ask = false;
                }
                else
                    ask = false;
            }
            evt->accept();
        }
        else if(res == QMessageBox::Discard)
            evt->accept();
        else if(res == QMessageBox::Cancel)
            evt->ignore();
    }
    else
        evt->accept();
}

void MainWindow::childEvent(QChildEvent *evt)
{
    if(evt->removed())
    {
        for(size_t i = 0; i < m_editors.size(); ++i)
        {
            if(m_editors[i] == evt->child())
            {
                m_editors[i] = m_editors.back();
                m_editors.pop_back();
                break;
            }
        }
    }

    this->QMainWindow::childEvent(evt);
}

void MainWindow::_newProject()
{
    this->_closeProject();

    m_pro = new ProjectModel();
    m_pro->world()->addMonitor(this);
    ui->gameCanvas->setWorld(m_pro->world());
    ui->entSearchEdit->clear();

    this->connect(m_pro, SIGNAL(fileNameChanged()), SLOT(_setTitle()));
    this->connect(m_pro, SIGNAL(unsavedChanged()), SLOT(_setTitle()));
    this->_setTitle();

    this->connect(ui->actionSave_project, SIGNAL(triggered()), SLOT(_save()));
    this->connect(ui->actionSave_project_as, SIGNAL(triggered()), SLOT(_saveAs()));
    this->connect(ui->actionClose_project, SIGNAL(triggered()), SLOT(_closeProject()));

    this->connect(ui->actionEdit_properties, SIGNAL(triggered()), SLOT(_editProperties()));
    this->connect(ui->actionEntity_type_editor, SIGNAL(triggered()), SLOT(_openEntityTypeEditor()));

    this->connect(ui->addEntity, SIGNAL(clicked()), SLOT(_addEntity()));
    this->connect(ui->removeEntity, SIGNAL(clicked()), SLOT(_removeEntity()));
    this->connect(ui->entityList, SIGNAL(itemSelected(const QAbstractSearchableListItem*)), SLOT(_entitySelected(const QAbstractSearchableListItem*)));
    ui->entityList->connect(ui->entSearchEdit, SIGNAL(textChanged(QString)), SLOT(setQueryText(QString)));

    this->connect(ui->toggleFrozenBtt, SIGNAL(clicked()), SLOT(_toggleFrozen()));
    this->connect(ui->debugPhysics, SIGNAL(toggled(bool)), SLOT(_toggleDebugPhysics(bool)));

    this->_setProCtrlEnabled(true);
    this->ui->actionUndo->setEnabled(false);
    this->ui->actionRedo->setEnabled(false);

    this->connect(ui->actionHelp_about, SIGNAL(triggered()), SLOT(_helpAndAbout()));

    this->_toggleDebugPhysics(false);
}

void MainWindow::_closeProject()
{
    if(!m_pro)
        return;

    bool ask = m_pro->isUnsaved();
    while(ask)
    {
        if(!m_pro->save(this))
        {
            int btt = QMessageBox::question(this, "CuteEngine editor", "Could not save changes. Do you want to discard them?", QMessageBox::Yes|QMessageBox::No, QMessageBox::No);
            if(btt == QMessageBox::Yes)
                ask = false;
        }
        else
            ask = false;
    }

    for(QWidget* w : m_editors)
        w->close();
    m_editors.clear();

    ui->entityList->clear();
    ui->selectedEntName->clear();
    ui->selectedEntProps->clear();

    ui->gameCanvas->setWorld(nullptr);
    this->_setProCtrlEnabled(false);
    delete m_pro;
    m_pro = nullptr;
    m_actionStack->clear();
    this->_setTitle();
}

void MainWindow::_open()
{
    this->_closeProject();
    this->_newProject();
    m_pro->open(this);
}

void MainWindow::_save()
{
    if(m_pro)
        m_pro->save(this);
}

void MainWindow::_saveAs()
{
    if(m_pro)
        m_pro->saveAs(this);
}

void MainWindow::_setTitle()
{
    if(m_pro)
    {
        QString title;
        if(m_pro->isUnsaved())
            title = "* ";
        title += m_pro->fileName();
        title += " - CuteEngine editor";
        this->setWindowTitle(title);
    }
    else
        this->setWindowTitle("CuteEngine editor");
}

void MainWindow::_editProperties()
{
    if(!m_pro)
        return;

    ProjectPropsEditor* ed = new ProjectPropsEditor(m_pro, this);
    ed->exec();
    delete ed;
}

void MainWindow::_storeWindowGeom()
{
    QSettings settings(g_orgName, g_appName);
    settings.setValue("geometry", this->saveGeometry());
    settings.setValue("maximized", this->isMaximized());
}

void MainWindow::_restoreWindowGeom()
{
    QSettings settings(g_orgName, g_appName);
    this->restoreGeometry(settings.value("geometry").toByteArray());
    bool maximized = settings.value("maximized").toBool();
    if(maximized)
        this->showMaximized();
}

void MainWindow::_addEntity()
{
    AddEntityDialog* dlg = new AddEntityDialog(m_pro->world(), this);
    if(dlg->exec() == QDialog::Accepted)
        m_actionStack->exec("Add entity", { new AddEntityAction(m_pro->world(), dlg->selectedEntityType().toStdString(), dlg->selectedEntityName().toStdString()) });
}

void MainWindow::_removeEntity()
{
    if(!m_selectedEnt)
        return;

    QString msg = "Are you sure that you want to remove this item?";
    int btt = QMessageBox::question(this, "CuteEngine editor", msg, QMessageBox::Yes|QMessageBox::No, QMessageBox::No);
    if(btt == QMessageBox::Yes)
        m_actionStack->exec("Remove entity", { new RemoveEntityAction(m_selectedEnt) });
}

void MainWindow::_entitySelected(const QAbstractSearchableListItem *item)
{
    if(!m_pro)
        return;

    if(m_mode == GM_Simulation)
    {
        m_selectedEnt = nullptr;
    }
    else
    {
        QString name = item->name();
        m_selectedEnt = m_pro->world()->entity(name.toStdString());
    }

    this->_updateEntProps();
}

void MainWindow::_changeMode(GameMode mode)
{
    if(mode == GM_Editor)
    {
        ui->toggleFrozenBtt->setText("Unfreeze");
        ui->entityPropsDock->setEnabled(true);

        m_actionStack->endMacro(m_macro);
        m_macro = nullptr;
    }
    else if(mode == GM_Simulation)
    {
        ui->toggleFrozenBtt->setText("Freeze");
        ui->entityPropsDock->setEnabled(false);

        m_selectedEnt = nullptr;
        this->_updateEntProps();

        m_macro = m_actionStack->beginMacro("Simulation");
        m_actionStack->exec("Simulation", { new StoreWorldStateAction(m_pro->world()) });
    }

    m_mode = mode;
}

void MainWindow::_toggleFrozen()
{
    if(m_mode == GM_Editor)
    {
        this->_changeMode(GM_Simulation);
        ui->gameCanvas->unfreezeWorld();
        ui->toggleFrozenBtt->setText("Freeze");
    }
    else if(m_mode == GM_Simulation)
    {
        this->_changeMode(GM_Editor);
        ui->gameCanvas->freezeWorld();
        ui->toggleFrozenBtt->setText("Unfreeze");
    }
}

void MainWindow::_toggleDebugPhysics(bool e)
{
    cute::PhysicsCompMgr* phyMgr = (cute::PhysicsCompMgr*)m_pro->world()->componentMgr("Physics");
    if(phyMgr)
        phyMgr->enableDebugDraw(e);
}

void MainWindow::_openEntityTypeEditor()
{
    QWidget* ed = new EntityTypeEditor(m_pro->world(), this, Qt::Window);
    ed->show();
    m_editors.push_back(ed);
}

void MainWindow::_undoAvailable(bool available)
{
    if(available)
        ui->actionUndo->setText(QString("Undo: %1").arg(m_actionStack->topUndoName()));
    else
        ui->actionUndo->setText("Undo");
}

void MainWindow::_redoAvailable(bool available)
{
    if(available)
        ui->actionRedo->setText(QString("Redo: %1").arg(m_actionStack->topRedoName()));
    else
        ui->actionRedo->setText("Redo");
}

void MainWindow::_helpAndAbout()
{
    HelpAndAbout* help = new HelpAndAbout(this);
    help->show();
    help->exec();
    delete help;
}
