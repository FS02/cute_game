#ifndef CUTEPROPERTY_HPP
#define CUTEPROPERTY_HPP

#include <CuteEngine/World/Entity.hpp>
#include <QPropertySheet>
#include "ActionStack.hpp"
#include "qnumproperty.h"

class CuteProperty : public QPropertyGroup, public cute::IPropertyWatch
{
public:
    CuteProperty(ActionStack* actionStack, cute::Property* prop);
    virtual ~CuteProperty();

    virtual QVariant data(Qt::PropertySheetColumn column, int role) const;
    virtual bool hasEditableData(Qt::PropertySheetColumn column) const;
    virtual void setData(const QVariant& data);

    virtual void propagateChanges();

    virtual void propertyChanged(cute::Property *prop);

private:
    ActionStack* m_actionStack;
    cute::Property* m_prop;
    QFloatProperty* m_x, * m_y; //  used only by Vector2 properties.
    bool m_isFrozen;
    cute::Value m_oldVal;

    void setVec2(float x, float y);

protected:
    virtual void set(ActionStack* stack, cute::Property* prop, const cute::Value& val);
};

class CuteEntityProperty : public CuteProperty
{
public:
    CuteEntityProperty(ActionStack* stack, cute::Entity* ent, cute::Property* prop);

protected:
    cute::Entity* m_ent;

    virtual void set(ActionStack *stack, cute::Property *prop, const cute::Value &val);
};

void BuildPropertyTree(ActionStack* actionStack, cute::Entity* ent, QPropertyGroup* root);

#endif // CUTEPROPERTY_HPP
