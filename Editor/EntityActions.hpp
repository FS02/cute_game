#ifndef ENTITYACTIONS_HPP
#define ENTITYACTIONS_HPP

#include "ActionStack.hpp"
#include <CuteEngine/World/GameWorld.hpp>
#include <CuteEngine/Core/Value.hpp>

class AddEntityAction : public IAction
{
public:
    AddEntityAction(cute::GameWorld* world, const std::string& typeName, const std::string& entityName);

    virtual void exec();
    virtual void undo();

private:
    cute::GameWorld* m_world;
    std::string m_typeName, m_entityName;
};

class RemoveEntityAction : public IAction
{
public:
    RemoveEntityAction(cute::Entity* entity);

    virtual void exec();
    virtual void undo();

private:
    cute::GameWorld* m_world;
    std::string m_name;
    std::string m_descName;
    cute::ConfStorage m_state;
};

class SetEntityPropertyAction : public IAction
{
public:
    SetEntityPropertyAction(cute::Entity* ent, const cute::Property* prop, const cute::Value& newVal);

    virtual void exec();
    virtual void undo();

private:
    cute::GameWorld* m_world;
    std::string m_entityName;
    std::string m_compType;
    std::string m_propName;
    cute::Value m_oldVal, m_newVal;
};

class StoreWorldStateAction : public IAction
{
public:
    StoreWorldStateAction(cute::GameWorld* world);

    virtual void exec();
    virtual void undo();

private:
    cute::GameWorld* m_world;
    std::map<std::string, cute::ConfStorage> m_state;

    bool _storeEntState(cute::Entity* ent);
};

#endif // ENTITYACTIONS_HPP
