#ifndef QPROPERTY_H
#define QPROPERTY_H

#include <QString>
#include <QVariant>
#include <QItemEditorCreatorBase>
#include <vector>

class QAbstractProperty;
class QPropertyGroup;
class QWidget;

namespace Qt
{
    enum PropertySheetColumn
    {
        NameColumn,
        ValueColumn
    };
}

class QAbstractPropertyProxy
{
public:
    virtual ~QAbstractPropertyProxy();

    virtual void propertyAboutToBeAdded(const QAbstractProperty* prop) = 0;
    virtual void propertyJustAdded(const QAbstractProperty* prop) = 0;

    virtual void propertyAboutToBeRemoved(const QAbstractProperty* prop) = 0;
    virtual void propertyJustRemoved(const QAbstractProperty* prop) = 0;

    virtual void propertyUpdated(const QAbstractProperty* prop) = 0;
};

class QAbstractProperty
{
    friend class QPropertyGroup;

public:
    QAbstractProperty(const QString& name);
    virtual ~QAbstractProperty();

    virtual void setProxy(QAbstractPropertyProxy* proxy);

    QPropertyGroup* parent();
    const QPropertyGroup* parent() const;

    void setName(const QString& name);
    const QString& name() const;

    virtual bool isGroup() const = 0;
    virtual QVariant data(Qt::PropertySheetColumn column, int role) const = 0;
    virtual bool hasEditableData(Qt::PropertySheetColumn column) const = 0; // role = Qt::DisplayRole
    virtual void setData(const QVariant& data) = 0;

protected:
    QAbstractPropertyProxy* m_proxy;
    QPropertyGroup* m_parent;
    QString m_name;
};

class QProperty : public QAbstractProperty
{
public:
    QProperty(const QString& name = QString());

    virtual bool isGroup() const;
    virtual QVariant data(Qt::PropertySheetColumn column, int role) const = 0;
    virtual bool hasEditableData(Qt::PropertySheetColumn column) const = 0;
    virtual void setData(const QVariant& data) = 0;
};

class QPropertyGroup : public QAbstractProperty
{
public:
    QPropertyGroup(const QString& name = QString(), const QString& info = QString());
    virtual ~QPropertyGroup();

    virtual void setProxy(QAbstractPropertyProxy *proxy);

    void setInfo(const QString& info);
    const QString& info() const;

    void addChild(QAbstractProperty* prop);
    QAbstractProperty* child(int index);
    const QAbstractProperty* child(int index) const;
    int indexOfChild(const QAbstractProperty* ch) const;
    int size() const;
    int totalSize() const;

    virtual bool isGroup() const;
    virtual QVariant data(Qt::PropertySheetColumn column, int role) const;
    virtual bool hasEditableData(Qt::PropertySheetColumn column) const;
    virtual void setData(const QVariant& data);

    virtual void propagateChanges();

private:
    QString m_info;
    std::vector<QAbstractProperty*> m_children;
    int m_totalSize;

    void _incTotalSize(int n = 1);
    void _decTotalSize(int n = 1);
};

#endif // QPROPERTY_H
