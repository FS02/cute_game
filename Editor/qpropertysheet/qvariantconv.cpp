#include "qvariantconv.h"
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QReadLocker>
#include <QWriteLocker>

Q_GLOBAL_STATIC(QVariantConv, g_defaultConv)

namespace
{
    QString qDefConv(const QVariant& in)
    {
        return in.toString();
    }

    QString qVec2Conv(const QVariant& in)
    {
        QVector2D vec = in.value<QVector2D>();
        return QString("%1, %2").arg(vec.x()).arg(vec.y());
    }

    QString qVec3Conv(const QVariant& in)
    {
        QVector3D vec = in.value<QVector3D>();
        return QString("%1, %2, %3").arg(vec.x()).arg(vec.y()).arg(vec.z());
    }

    QString qVec4Conv(const QVariant& in)
    {
        QVector4D vec = in.value<QVector4D>();
        return QString("%1, %2, %3, %4").arg(vec.x()).arg(vec.y()).arg(vec.z()).arg(vec.w());
    }
}

/*
 * QVariantConv
 */

QVariantConv::QVariantConv(QObject *parent)
    : QObject(parent)
{
    static const int defTypes[] =
    {
        QMetaType::QString,
        QMetaType::Bool,
        QMetaType::QByteArray,
        QMetaType::QChar,
        QMetaType::QDate,
        QMetaType::QDateTime,
        QMetaType::Double,
        QMetaType::Int,
        QMetaType::LongLong,
        QMetaType::QStringList,
        QMetaType::QTime,
        QMetaType::UInt,
        QMetaType::ULongLong,
        QMetaType::Float
    };

    static const int nDefTypes = sizeof(defTypes)/sizeof(defTypes[0]);

    for(int i = 0; i < nDefTypes; ++i)
        this->registerType(defTypes[i], &qDefConv);

    // non-standard conversions
    this->registerType(QMetaType::QVector2D, &qVec2Conv);
    this->registerType(QMetaType::QVector3D, &qVec3Conv);
    this->registerType(QMetaType::QVector4D, &qVec4Conv);
}

QString QVariantConv::convert(const QVariant &input, bool *ok) const
{
    QReadLocker locker(&m_lock);

    if(ok)
        (*ok) = true;
    int type = input.userType();
    Function func = m_conv.value(type, 0);
    if(func)
        return func(input);
    if(ok)
        (*ok) = false;
    return QString();
}

void QVariantConv::registerType(int type, Function func)
{
    QWriteLocker locker(&m_lock);
    m_conv.insert(type, func);
}

const QVariantConv* QVariantConv::defaultConverter()
{
    return g_defaultConv();
}
