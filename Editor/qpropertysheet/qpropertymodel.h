#ifndef QPROPERTYMODEL_H
#define QPROPERTYMODEL_H

#include "qproperty.h"
#include <QAbstractItemModel>

class QPropertyModelProxy;
class QVariantConv;

class QPropertyModel : public QAbstractItemModel
{
    friend class QPropertyModelProxy;

public:
    QPropertyModel(QObject* parent = 0);
    virtual ~QPropertyModel();

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &child) const;

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual bool hasChildren(const QModelIndex &parent = QModelIndex()) const;

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    void setVariantConverter(const QVariantConv* conv);
    const QVariantConv* variantConverter() const;

    QAbstractProperty* propertyByIndex(const QModelIndex& index);
    const QAbstractProperty* propertyByIndex(const QModelIndex& index) const;
    QModelIndex indexOfProperty(const QAbstractProperty* prop) const;

    void clear();

    QPropertyGroup* root();
    const QPropertyGroup* root() const;

private:
    QAbstractPropertyProxy* m_proxy;
    QPropertyGroup* m_root;
    const QVariantConv* m_varConv;
};

#endif // QPROPERTYMODEL_H
