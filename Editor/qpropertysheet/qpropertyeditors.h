#ifndef QPROPERTYEDITORS_H
#define QPROPERTYEDITORS_H

#include <QLineEdit>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

class QFloatPropertyEditor : public QLineEdit
{
    Q_OBJECT

public:
    QFloatPropertyEditor(QWidget* parent = 0);
};

class QVector2DPropertyEditor : public QLineEdit
{
    Q_OBJECT
    Q_PROPERTY(QVector2D value READ value WRITE setValue NOTIFY valueChanged)

public:
    QVector2DPropertyEditor(QWidget* parent = 0);

    void setValue(const QVector2D& val);
    QVector2D value() const;

signals:
    void valueChanged(const QVector2D& value);

private:
    QVector2D m_value;

private slots:
    void onEditingFinished();
};

class QVector3DPropertyEditor : public QLineEdit
{
    Q_OBJECT
    Q_PROPERTY(QVector3D value READ value WRITE setValue NOTIFY valueChanged)

public:
    QVector3DPropertyEditor(QWidget* parent = 0);

    void setValue(const QVector3D& val);
    QVector3D value() const;

signals:
    void valueChanged(const QVector3D& value);

private:
    QVector3D m_value;

private slots:
    void onEditingFinished();
};

#endif // QPROPERTYEDITORS_H
