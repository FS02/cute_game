#ifndef QVARIANTCONV_H
#define QVARIANTCONV_H

#include <QVariant>
#include <QString>
#include <QMap>
#include <QReadWriteLock>

class QVariantConv : public QObject
{
    Q_OBJECT

public:
    typedef QString(*Function)(const QVariant&);

    QVariantConv(QObject* parent = 0);

    QString convert(const QVariant& input, bool* ok = 0) const;
    void registerType(int type, Function func);

    static const QVariantConv* defaultConverter();

private:
    mutable QReadWriteLock m_lock;
    QMap<int, Function> m_conv;
};

#endif // QVARIANTCONV_H
