#include "qpropertyeditors.h"
#include <QValidator>

namespace
{
    QStringList qSplitWs(const QString& input)
    {
        QStringList result;
        result.push_back(QString());

        for(int i = 0; i < input.size(); ++i)
        {
            if(input[i].isSpace())
            {
                if(!result.back().isEmpty())
                    result.push_back(QString());
            }
            else
                result.back().append(input[i]);
        }

        if(result.back().isEmpty())
            result.pop_back();

        return result;
    }

    QString qVec2ToStr(const QVector2D& vec)
    {
        return QString("%1, %2").arg(vec.x()).arg(vec.y());
    }

    QString qVec3ToStr(const QVector3D& vec)
    {
        return QString("%1, %2, %3").arg(vec.x()).arg(vec.y()).arg(vec.z());
    }

    std::vector<float> qStrToVec(const QString& str, int len, bool* ok = 0)
    {
        if(ok) (*ok) = true;

        if(str.isEmpty())
            return std::vector<float>();

        QStringList elems;
        std::vector<float> vec;

        int nCommas = str.count(',');
        if(nCommas == 0)
            elems = qSplitWs(str);
        else if(nCommas == len - 1)
        {
            elems = str.split(",");
            for(int i = 0; i < elems.size(); ++i)
                elems[i] = elems[i].trimmed();
        }
        else
        {
            if(ok) (*ok) = false;
            return std::vector<float>();
        }

        vec.resize(len);
        for(int i = 0; i < len; ++i)
            vec[i] = elems[i].toFloat(ok);

        if(ok && !(*ok))
            return std::vector<float>();

        return vec;
    }

    QVector2D qStrToVec2(const QString& str, bool* ok = 0)
    {
        std::vector<float> v = qStrToVec(str, 2, ok);
        if(ok && !(*ok))
            return QVector2D();
        return QVector2D(v[0], v[1]);
    }

    QVector3D qStrToVec3(const QString& str, bool* ok = 0)
    {
        std::vector<float> v = qStrToVec(str, 3, ok);
        if(ok && !(*ok))
            return QVector3D();
        return QVector3D(v[0], v[1], v[2]);
    }
}

/*
 * QFloatPropertyEditor
 */

QFloatPropertyEditor::QFloatPropertyEditor(QWidget *parent)
    : QLineEdit(parent)
{
    this->setValidator(new QDoubleValidator(this));
}

/*
 * QVector2DPropertyEditor2
 */

QVector2DPropertyEditor::QVector2DPropertyEditor(QWidget *parent)
    : QLineEdit(parent)
{
    this->setValue(QVector2D());
    this->connect(this, SIGNAL(editingFinished()), SLOT(onEditingFinished()));
}

void QVector2DPropertyEditor::setValue(const QVector2D &val)
{
    m_value = val;
    this->setText(qVec2ToStr(m_value));
    emit valueChanged(m_value);
}

QVector2D QVector2DPropertyEditor::value() const
{
    return m_value;
}

void QVector2DPropertyEditor::onEditingFinished()
{
    QVector2D vec = qStrToVec2(this->text());
    this->setValue(vec);
}

/*
 * QVector3DPropertyEditor
 */

QVector3DPropertyEditor::QVector3DPropertyEditor(QWidget *parent)
    : QLineEdit(parent)
{
    this->setValue(QVector3D());
    this->connect(this, SIGNAL(editingFinished()), SLOT(onEditingFinished()));
}

void QVector3DPropertyEditor::setValue(const QVector3D &val)
{
    m_value = val;
    this->setText(qVec3ToStr(m_value));
    emit valueChanged(m_value);
}

QVector3D QVector3DPropertyEditor::value() const
{
    return m_value;
}

void QVector3DPropertyEditor::onEditingFinished()
{
    QVector3D vec = qStrToVec3(this->text());
    this->setValue(vec);
}
