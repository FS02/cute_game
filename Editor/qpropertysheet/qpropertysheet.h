#ifndef QPROPERTYSHEET_H
#define QPROPERTYSHEET_H

#include "qpropertymodel.h"
#include <QTreeView>
#include <QItemEditorCreatorBase>
#include <QItemEditorFactory>

class QPropertySheet : public QTreeView
{
public:
    QPropertySheet(QWidget* parent = 0);
    virtual ~QPropertySheet();

    QPropertyGroup* rootGroup();
    const QPropertyGroup* rootGroup() const;

    void clear();

    void registerPropertyType(int userType, QItemEditorCreatorBase* creator);

private:
    QPropertyModel* m_model;
    QItemEditorFactory* m_editorFactory;

    QPropertySheet(const QPropertySheet&);
};

#endif // QPROPERTYSHEET_H
