#ifndef QNUMPROPERTY_H
#define QNUMPROPERTY_H

#include "qproperty.h"

class QFloatProperty : public QProperty
{
public:
    QFloatProperty(const QString& name = QString(), float value = 0.0f);

    void setValue(float v);
    float value() const;

    virtual QVariant data(Qt::PropertySheetColumn column, int role) const;
    virtual bool hasEditableData(Qt::PropertySheetColumn column) const;
    virtual void setData(const QVariant& data);

private:
    float m_value;
};

#endif // QNUMPROPERTY_H
