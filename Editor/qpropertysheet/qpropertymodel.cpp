#include "qpropertymodel.h"
#include "qvariantconv.h"
#include <cassert>

/*
 * QPropertyModelProxy
 */

class QPropertyModelProxy : public QAbstractPropertyProxy
{
public:
    QPropertyModelProxy(QPropertyModel* model)
        : m_model(model)
    {
    }

    virtual void propertyAboutToBeAdded(const QAbstractProperty* prop)
    {
        QModelIndex index = m_model->indexOfProperty(prop->parent());
        int first = 0;
        if(prop->parent())
            first = prop->parent()->size();
        int n = 1;
        m_model->beginInsertRows(index, first, first + n);
    }

    virtual void propertyJustAdded(const QAbstractProperty* prop)
    {
        ((void)prop);
        m_model->endInsertRows();
    }

    virtual void propertyAboutToBeRemoved(const QAbstractProperty* prop)
    {
        ((void)prop);
    }

    virtual void propertyJustRemoved(const QAbstractProperty* prop)
    {
        ((void)prop);
    }

    virtual void propertyUpdated(const QAbstractProperty *prop)
    {
        QModelIndex index = m_model->indexOfProperty(prop);
        emit m_model->dataChanged(index, index);
    }

private:
    QPropertyModel* m_model;
};

/*
 * QPropertyModel
 */

QPropertyModel::QPropertyModel(QObject *parent)
    : QAbstractItemModel(parent), m_root(new QPropertyGroup("Root", "Root")), m_varConv(QVariantConv::defaultConverter())
{
    m_proxy = new QPropertyModelProxy(this);
    m_root->setProxy(m_proxy);
}

QPropertyModel::~QPropertyModel()
{
    delete m_root;
    delete m_proxy;
}

QVariant QPropertyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    ((void)orientation);

    if(section == 0)
    {
        if(role == Qt::DisplayRole)
            return QVariant(tr("Name"));
    }
    else if(section == 1)
    {
        if(role == Qt::DisplayRole)
            return QVariant(tr("Value"));
    }

    return QVariant();
}

QModelIndex QPropertyModel::index(int row, int column, const QModelIndex &parent) const
{
    const QPropertyGroup* root = m_root;
    if(parent.isValid())
    {
        const QAbstractProperty* prop = this->propertyByIndex(parent);
        if(prop->isGroup())
            root = (const QPropertyGroup*)prop;
        else
            return QModelIndex();
    }

    if(row < 0 || row >= root->size())
        return QModelIndex();

    return this->createIndex(row, column, (void*)root->child(row));
}

QModelIndex QPropertyModel::parent(const QModelIndex &child) const
{
    const QAbstractProperty* prop = this->propertyByIndex(child);
    if(!prop)
        return QModelIndex();
    const QPropertyGroup* propParent = prop->parent();
    if(propParent)
    {
        int row = 0;
        if(propParent->parent())
            row = propParent->parent()->indexOfChild(propParent);
        return this->createIndex(row, 0, (void*)(const QAbstractProperty*)propParent);
    }
    return QModelIndex();
}

int QPropertyModel::rowCount(const QModelIndex &parent) const
{
    const QAbstractProperty* prop = m_root;
    if(parent.isValid())
        prop = this->propertyByIndex(parent);

    if(prop->isGroup())
    {
        const QPropertyGroup* gr = (const QPropertyGroup*)prop;
        return gr->size();
    }

    return 0;
}

int QPropertyModel::columnCount(const QModelIndex &parent) const
{
    ((void)parent);
    return 2;
}

Qt::ItemFlags QPropertyModel::flags(const QModelIndex &index) const
{
    if(index.column() == 0)
        return Qt::ItemIsEnabled|Qt::ItemIsSelectable;
    else if(index.column() == 1)
        return Qt::ItemIsEnabled|Qt::ItemIsSelectable|Qt::ItemIsEditable;

    return Qt::NoItemFlags;
}

bool QPropertyModel::hasChildren(const QModelIndex &parent) const
{
    const QAbstractProperty* prop = this->propertyByIndex(parent);
    if(prop && prop->isGroup())
    {
        const QPropertyGroup* gr = (const QPropertyGroup*)prop;
        return gr->size() > 0;
    }
    return false;
}

QVariant QPropertyModel::data(const QModelIndex &index, int role) const
{
    const QAbstractProperty* prop = this->propertyByIndex(index);
    if(!prop)
        return QVariant();

    if(index.column() == Qt::NameColumn)
        return prop->data(Qt::NameColumn, role);
    else if(index.column() == Qt::ValueColumn)
        return m_varConv->convert(prop->data(Qt::ValueColumn, role));

    return QVariant();
}

void QPropertyModel::setVariantConverter(const QVariantConv *conv)
{
    m_varConv = conv;
}

const QVariantConv* QPropertyModel::variantConverter() const
{
    return m_varConv;
}

QAbstractProperty* QPropertyModel::propertyByIndex(const QModelIndex &index)
{
    if(!index.isValid())
        return m_root;
    return (QAbstractProperty*)index.internalPointer();
}

const QAbstractProperty* QPropertyModel::propertyByIndex(const QModelIndex &index) const
{
    if(!index.isValid())
        return m_root;
    return (const QAbstractProperty*)index.internalPointer();
}

QModelIndex QPropertyModel::indexOfProperty(const QAbstractProperty *prop) const
{
    if(!prop)
        return QModelIndex();

    const QPropertyGroup* parent = prop->parent();
    if(parent)
    {
        int row = parent->indexOfChild(prop);
        return this->createIndex(row, 0, (void*)const_cast<QAbstractProperty*>(prop));
    }
    else
        return this->createIndex(0, 0, (void*)const_cast<QAbstractProperty*>(prop));
}

void QPropertyModel::clear()
{
    this->beginRemoveRows(QModelIndex(), 0, 0);
    this->endRemoveRows();

    QPropertyGroup* prev = m_root;

    this->beginInsertRows(QModelIndex(), 0, 0);
    m_root = new QPropertyGroup("Root", "Root");
    m_root->setProxy(m_proxy);
    this->endInsertRows();

    delete prev;
    QModelIndex index = this->createIndex(0, 0, (void*)m_root);
    emit this->dataChanged(index, index);
}

QPropertyGroup* QPropertyModel::root()
{
    return m_root;
}

const QPropertyGroup* QPropertyModel::root() const
{
    return m_root;
}
