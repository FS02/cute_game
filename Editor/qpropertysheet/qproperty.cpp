#include "qproperty.h"
#include <QLabel>

/*
 * QAbstractPropertyProxy
 */

QAbstractPropertyProxy::~QAbstractPropertyProxy()
{
}

/*
 * QAbstractProperty
 */

QAbstractProperty::QAbstractProperty(const QString &name)
    : m_proxy(0), m_parent(0), m_name(name)
{
}

QAbstractProperty::~QAbstractProperty()
{
}

void QAbstractProperty::setProxy(QAbstractPropertyProxy *proxy)
{
    m_proxy = proxy;
}

QPropertyGroup* QAbstractProperty::parent()
{
    return m_parent;
}

const QPropertyGroup* QAbstractProperty::parent() const
{
    return m_parent;
}

void QAbstractProperty::setName(const QString &name)
{
    m_name = name;
}

const QString& QAbstractProperty::name() const
{
    return m_name;
}

/*
 * QProperty
 */

QProperty::QProperty(const QString &name)
    : QAbstractProperty(name)
{
}

bool QProperty::isGroup() const
{
    return false;
}

/*
 * QPropertyGroup
 */

QPropertyGroup::QPropertyGroup(const QString &name, const QString &info)
    : QAbstractProperty(name), m_info(info), m_totalSize(0)
{
}

QPropertyGroup::~QPropertyGroup()
{
    this->_decTotalSize(m_children.size());
    for(int i = 0; i < (int)m_children.size(); ++i)
        delete m_children[i];
}

void QPropertyGroup::setProxy(QAbstractPropertyProxy *proxy)
{
    m_proxy = proxy;
    for(int i = 0; i < (int)m_children.size(); ++i)
        m_children[i]->setProxy(proxy);
}

void QPropertyGroup::setInfo(const QString &info)
{
    m_info = info;
}

const QString& QPropertyGroup::info() const
{
    return m_info;
}

void QPropertyGroup::addChild(QAbstractProperty *prop)
{
    if(!prop || prop->m_parent != 0)
        return;

    prop->m_parent = this;
    prop->setProxy(m_proxy);

    if(m_proxy)
        m_proxy->propertyAboutToBeAdded(prop);

    m_children.push_back(prop);
    this->_incTotalSize();

    if(prop->isGroup())
    {
        QPropertyGroup* gr = (QPropertyGroup*)prop;
        this->_incTotalSize(gr->totalSize());
    }

    if(m_proxy)
        m_proxy->propertyJustAdded(prop);
}

QAbstractProperty* QPropertyGroup::child(int index)
{
    if(index < 0 || index >= (int)m_children.size())
        return 0;
    return m_children[index];
}

const QAbstractProperty* QPropertyGroup::child(int index) const
{
    if(index < 0 || index >= (int)m_children.size())
        return 0;
    return m_children[index];
}

int QPropertyGroup::indexOfChild(const QAbstractProperty *ch) const
{
    for(int i = 0; i < (int)m_children.size(); ++i)
        if(m_children[i] == ch)
            return i;
    return -1;
}

int QPropertyGroup::size() const
{
    return m_children.size();
}

int QPropertyGroup::totalSize() const
{
    return m_totalSize;
}

bool QPropertyGroup::isGroup() const
{
    return true;
}

QVariant QPropertyGroup::data(Qt::PropertySheetColumn column, int role) const
{
    if(column == Qt::NameColumn)
    {
        if(role == Qt::DisplayRole)
            return QVariant(m_name);
    }
    else if(column == Qt::ValueColumn)
    {
        if(role == Qt::DisplayRole)
            return QVariant(m_info);
    }

    return QVariant();
}

bool QPropertyGroup::hasEditableData(Qt::PropertySheetColumn column) const
{
    ((void)column);
    return false;
}

void QPropertyGroup::setData(const QVariant &data)
{
    ((void)data);
}

void QPropertyGroup::propagateChanges()
{
}

void QPropertyGroup::_incTotalSize(int n)
{
    m_totalSize += n;
    if(m_parent)
        m_parent->_incTotalSize(n);
}

void QPropertyGroup::_decTotalSize(int n)
{
    m_totalSize -= n;
    if(m_parent)
        m_parent->_decTotalSize(n);
}
