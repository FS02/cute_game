#include "qnumproperty.h"

/*
 * QFloatProperty
 */

QFloatProperty::QFloatProperty(const QString &name, float value)
    : QProperty(name), m_value(value)
{
}

void QFloatProperty::setValue(float v)
{
    m_value = v;
    if(m_parent)
        m_parent->propagateChanges();
}

float QFloatProperty::value() const
{
    return m_value;
}

QVariant QFloatProperty::data(Qt::PropertySheetColumn column, int role) const
{
    if(column == Qt::NameColumn)
    {
        if(role == Qt::DisplayRole)
            return QVariant(m_name);
    }
    else if(column == Qt::ValueColumn)
    {
        if(role == Qt::DisplayRole)
            return QVariant(m_value);
    }

    return QVariant();
}

bool QFloatProperty::hasEditableData(Qt::PropertySheetColumn column) const
{
    if(column == Qt::ValueColumn)
        return true;

    return false;
}

void QFloatProperty::setData(const QVariant &data)
{
    m_value = data.toFloat();
    if(m_parent)
        m_parent->propagateChanges();
}
