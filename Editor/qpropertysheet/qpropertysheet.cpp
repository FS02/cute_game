#include "qpropertysheet.h"
#include "qpropertyeditors.h"
#include <QStyledItemDelegate>
#include <QLineEdit>

namespace
{
    class QPropertyItemDelegate : public QStyledItemDelegate
    {
    public:
        QPropertyItemDelegate(QObject* parent = 0)
            : QStyledItemDelegate(parent)
        {
        }

        virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
        {
            ((void)option);

            const QPropertyModel* model = (const QPropertyModel*)index.model();
            const QAbstractProperty* prop = model->propertyByIndex(index);

            if(index.column() == 1 && prop->hasEditableData(Qt::ValueColumn))
            {
                const QItemEditorFactory* factory = this->itemEditorFactory();
                int type = prop->data(Qt::ValueColumn, Qt::DisplayRole).userType();
                QWidget* wid = factory->createEditor(type, parent);
                return wid;
            }

            return 0;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            const QPropertyModel* model = (const QPropertyModel*)index.model();
            const QAbstractProperty* prop = model->propertyByIndex(index);
            QItemEditorFactory* factory = this->itemEditorFactory();

            QVariant data = prop->data(Qt::ValueColumn, Qt::DisplayRole);
            QByteArray qpropName = factory->valuePropertyName(data.userType());
            editor->setProperty(qpropName.data(), data);
        }

        virtual void setModelData(QWidget *editor, QAbstractItemModel *_model, const QModelIndex &index) const
        {
            QPropertyModel* model = (QPropertyModel*)_model;
            QAbstractProperty* prop = model->propertyByIndex(index);
            QItemEditorFactory* factory = this->itemEditorFactory();

            int type = prop->data(Qt::ValueColumn, Qt::DisplayRole).userType();
            QByteArray qpropName = factory->valuePropertyName(type);
            prop->setData(editor->property(qpropName.data()));
        }
    };
}

/*
 * QPropertySheet
 */

QPropertySheet::QPropertySheet(QWidget *parent)
    : QTreeView(parent)
{
    m_model = new QPropertyModel(this);
    QAbstractItemModel* prevModel = this->model();
    this->setModel(m_model);
    delete prevModel;

    QPropertyItemDelegate* dlg = new QPropertyItemDelegate(this);
    m_editorFactory = new QItemEditorFactory();
    dlg->setItemEditorFactory(m_editorFactory);
    QAbstractItemDelegate* prevDlg = this->itemDelegate();
    this->setItemDelegate(dlg);
    delete prevDlg;

    this->setEditTriggers(QAbstractItemView::CurrentChanged|QAbstractItemView::SelectedClicked);

    // register creator of string property editor
    {
        QItemEditorCreator<QLineEdit>* creator = new QItemEditorCreator<QLineEdit>("text");
        this->registerPropertyType(QVariant::String, creator);
    }

    // register creator of QVector2D property editor
    {
        QItemEditorCreator<QVector2DPropertyEditor>* creator = new QItemEditorCreator<QVector2DPropertyEditor>("value");
        this->registerPropertyType(QVariant::Vector2D, creator);
    }

    // register creator of QVector3D property editor
    {
        QItemEditorCreator<QVector3DPropertyEditor>* creator = new QItemEditorCreator<QVector3DPropertyEditor>("value");
        this->registerPropertyType(QVariant::Vector3D, creator);
    }
}

QPropertySheet::~QPropertySheet()
{
}

QPropertyGroup* QPropertySheet::rootGroup()
{
    return m_model->root();
}

const QPropertyGroup* QPropertySheet::rootGroup() const
{
    return m_model->root();
}

void QPropertySheet::clear()
{
    m_model->clear();
}

void QPropertySheet::registerPropertyType(int userType, QItemEditorCreatorBase *creator)
{
    m_editorFactory->registerEditor(userType, creator);
}
