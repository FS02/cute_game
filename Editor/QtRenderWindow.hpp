#ifndef QTRENDERWINDOW_HPP
#define QTRENDERWINDOW_HPP

#include <CuteEngine/Core/IRenderWindow.hpp>
#include <QWidget>

class QtRenderWindow : public cute::IRenderWindow
{
public:
    QtRenderWindow(QWidget* realWidget)
        : m_realWidget(realWidget)
    {
    }

    virtual ~QtRenderWindow()
    {
    }

    virtual void create(const cute::WindowDesc& desc)
    {
        ((void)desc);
    }

    virtual void destroy()
    {
    }

    virtual bool isOpen() const
    {
        return true;
    }

    virtual void close()
    {
    }

    virtual cute::Vector2 size() const
    {
        QSize sz = m_realWidget->size();
        return cute::Vector2(sz.width(), sz.height());
    }

    virtual void update()
    {
    }

private:
    QWidget* m_realWidget;
};

#endif // QTRENDERWINDOW_HPP
