#include "CuteProperty.hpp"
#include "EntityActions.hpp"
#include <QVector2D>
#include <iostream>

namespace
{
    cute::Vector2 qvec(const QVector2D& v)
    {
        return cute::Vector2(v.x(), v.y());
    }

    cute::Value qVarToVal(const cute::Property* prop, const QVariant& var)
    {
        if(prop->isBool())
            return cute::Value::FromBool(var.toBool());
        else if(prop->isFloat())
            return cute::Value::FromFloat(var.toFloat());
        else if(prop->isVec2())
            return cute::Value::FromVec2(qvec(var.value<QVector2D>()));
        else if(prop->isString())
            return cute::Value::FromString(var.toString().toStdString());

        return cute::Value();
    }
}

/*
 * CuteProperty
 */

CuteProperty::CuteProperty(ActionStack* actionStack, cute::Property *prop)
    : QPropertyGroup(QString::fromStdString(prop->name())), m_actionStack(actionStack), m_prop(prop), m_x(nullptr), m_y(nullptr), m_isFrozen(false)
{
    m_prop->setWatch(this);
    if(m_prop->isVec2())
    {
        m_x = new QFloatProperty("x", m_prop->toVec2().x);
        this->addChild(m_x);
        m_y = new QFloatProperty("y", m_prop->toVec2().y);
        this->addChild(m_y);
    }

    m_oldVal = m_prop->toValue();
}

CuteProperty::~CuteProperty()
{
    m_prop->setWatch(nullptr);
}

QVariant CuteProperty::data(Qt::PropertySheetColumn column, int role) const
{
    if(column == Qt::NameColumn)
    {
        if(role == Qt::DisplayRole)
            return this->name();
    }
    else if(column == Qt::ValueColumn)
    {
        if(role == Qt::DisplayRole)
        {
            if(m_prop->isBool())
                return QVariant(m_prop->toBool());
            else if(m_prop->isFloat())
                return QVariant(m_prop->toFloat());
            else if(m_prop->isVec2())
            {
                cute::Vector2 v = m_prop->toVec2();
                return QVariant(QVector2D(v.x, v.y));
            }
            else if(m_prop->isString())
                return QVariant(QString::fromStdString(m_prop->toString()));
        }
    }

    return QVariant();
}

bool CuteProperty::hasEditableData(Qt::PropertySheetColumn column) const
{
    return (column == Qt::ValueColumn) && m_prop->isMutable();
}

void CuteProperty::setData(const QVariant &data)
{    
    cute::Value newVal = qVarToVal(m_prop, data);
    if(newVal == m_oldVal)
        return;

    this->set(m_actionStack, m_prop, newVal);
    m_oldVal = m_prop->toValue();
    if(m_prop->isVec2())
    {
        QVector2D v = data.value<QVector2D>();
        this->setVec2(v.x(), v.y());
    }
}

void CuteProperty::propagateChanges()
{
    if(!m_isFrozen)
    {
        m_isFrozen = true;

        if(m_prop->isVec2())
        {
            cute::Vector2 v;
            v.x = m_x->value();
            v.y = m_y->value();
            this->set(m_actionStack, m_prop, cute::Value::FromVec2(v));
            m_prop->setVec2(v);
        }

        m_isFrozen = false;
    }

    if(this->parent())
        this->parent()->propagateChanges();

    if(m_proxy)
        m_proxy->propertyUpdated(this);
}

void CuteProperty::propertyChanged(cute::Property *prop)
{
    if(prop != m_prop)
        return;

    if(m_prop->isVec2())
    {
        cute::Vector2 v = m_prop->toVec2();
        this->setVec2(v.x, v.y);
    }

    m_oldVal = prop->toValue();

    if(m_proxy)
        m_proxy->propertyUpdated(this);
}

void CuteProperty::setVec2(float x, float y)
{
    m_isFrozen = true;
    m_prop->setVec2(cute::Vector2(x, y));
    m_x->setValue(x);
    m_y->setValue(y);
    m_isFrozen = false;
}

void CuteProperty::set(ActionStack *stack, cute::Property *prop, const cute::Value &val)
{
    ((void)stack);
    prop->set(val);
}

/*
 * CuteEntityProperty
 */

CuteEntityProperty::CuteEntityProperty(ActionStack *stack, cute::Entity *ent, cute::Property *prop)
    : CuteProperty(stack, prop), m_ent(ent)
{
}

void CuteEntityProperty::set(ActionStack *stack, cute::Property *prop, const cute::Value &val)
{
    stack->exec("Change entity property", { new SetEntityPropertyAction(m_ent, prop, val) });
}

/*
 * BuildPropertyTree
 */

void BuildPropertyTree(ActionStack* actionStack, cute::Entity* ent, QPropertyGroup* root)
{
    int i = 0;
    for(cute::IComponent* comp = ent->component(i); comp; comp = ent->component(++i))
    {
        QPropertyGroup* gr = new QPropertyGroup(QString::fromStdString(comp->typeName()), "Component");
        cute::PropList props = comp->listProperties();
        for(cute::Property* p : props)
            gr->addChild(new CuteEntityProperty(actionStack, ent, p));
        root->addChild(gr);
    }
}
