# CuteGame Editor

Version 0.1

## Key shortcuts

* Ctrl + N - new project

* Ctrl + O - open project

* Ctrl + S - save project

* Ctrl + Shift + S - save project as



* Ctrl + Z - undo

* Ctrl + Shift + Z - redo
