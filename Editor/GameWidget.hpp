#ifndef GAMEWIDGET_HPP
#define GAMEWIDGET_HPP

#include <CuteEngine/World/GameWorld.hpp>
#include <CuteEngine/Core/Graphics.hpp>
#include <QGLWidget>
#include <QTimer>

class GameWidget : public QGLWidget
{
public:
    GameWidget(QWidget* parent = nullptr);
    virtual ~GameWidget();

    void setWorld(cute::GameWorld* world);
    void setRefreshRate(int fps);

    void freezeWorld();
    void unfreezeWorld();
    bool isWorldFrozen() const;

private:
    cute::IRenderWindow* m_window;
    cute::Graphics* m_graph;
    cute::GameWorld* m_world;
    bool m_frozenWorld;
    QTimer m_timer;

    void initializeGL();
    void paintGL();
};

#endif // GAMEWIDGET_HPP
