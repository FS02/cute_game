#include "HelpAndAbout.hpp"
#include "ui_HelpAndAbout.h"

HelpAndAbout::HelpAndAbout(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelpAndAbout)
{
    ui->setupUi(this);
}

HelpAndAbout::~HelpAndAbout()
{
    delete ui;
}
