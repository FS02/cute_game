#ifndef INITIALIZER_HPP
#define INITIALIZER_HPP

#include <type_traits>

namespace cute
{
    template<class T>
    /*
     * Returns initialized value.
     * For fundamental types (int, bool, etc.), it's zero.
     * For other types, it's T().
     */
    inline T Initializer();
}

/*
 * Implementation
 */

namespace _cute
{
    template<class T, bool IsFundamental>
    struct InitializerHelper
    {
        static T Get()
        {
            return T();
        }
    };

    template<class T>
    struct InitializerHelper<T, true>
    {
        static T Get()
        {
            return (T)0;
        }
    };
}

template<class T>
T cute::Initializer()
{
    return _cute::InitializerHelper<T, std::is_fundamental<T>::value>::Get();
}

#endif /* INITIALIZER_HPP */
