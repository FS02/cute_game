#include <CuteEngine/Core/SDL2/SDL2Texture.hpp>
#include <SDL_image.h>
#include <cassert>

using namespace cute;

namespace
{
    template <class T>
    T next_power_of_two(T value)
    {
        if ((value & (value - 1)) == 0)
            return value;
        value -= 1;
        for (size_t i = 1; i < sizeof(T) * 8; i <<= 1)
            value = value | value >> i;
        return value + 1;
    }
}

/*
 * Texture
 */

SDL2Texture::SDL2Texture(const std::string &fileName)
    : IResource(fileName), m_texture(0)
{
}

SDL2Texture::~SDL2Texture()
{
    // Delete texture
    glDeleteTextures(1, &m_texture);
}

bool SDL2Texture::load()
{
    //Load the image from the file into SDL's surface representation
    SDL_Surface* surface = IMG_Load((ResourcePath + "Sprites/" + m_fileName).c_str());
    if (surface == NULL) { //If it failed, say why and don't continue loading the texture
        printf("Error : \"%s, %s\"\n",m_fileName.c_str(), SDL_GetError()); return false;
    }

    m_width = surface->w;
    m_height = surface->h;

    if (!SDL_GL_ExtensionSupported("GL_ARB_texture_non_power_of_two"))
    {
        m_width = next_power_of_two(m_width);
        m_height = next_power_of_two(m_height);
    }

    assert(surface->w == (int)m_width && (int)surface->h == (int)m_height);

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &m_texture);
    glBindTexture(GL_TEXTURE_2D, m_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    GLenum format;
    switch (surface->format->BytesPerPixel) {
    case 4:
        format = (surface->format->Rmask == 0x000000ff) ? GL_RGBA : GL_BGRA;
        break;
    case 3:
        format = (surface->format->Rmask == 0x000000ff) ? GL_RGB : GL_BGR;
        break;
    default:
        assert(false);
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_width, m_height, 0, format, GL_UNSIGNED_BYTE, surface->pixels);

    SDL_FreeSurface(surface);
    return true;
}

unsigned int SDL2Texture::width()
{
    return m_width;
}

unsigned int SDL2Texture::height()
{
    return m_height;
}

void SDL2Texture::bind(const SDL2Texture *t)
{
    if (t && t->m_texture) glBindTexture(GL_TEXTURE_2D, t->m_texture);
    else glBindTexture(GL_TEXTURE_2D, 0);
}

SDL2Texture* SDL2Texture::Create(const std::string &fileName)
{
    return new SDL2Texture(fileName);
}
