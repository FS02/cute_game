#ifndef cuteNO_SDL2

#include <CuteEngine/Core/SDL2/SDL2RenderWindow.hpp>
#include <CuteEngine/Core/OpenGL.hpp>


using namespace cute;

/*
 * IRenderWindow
 */

IRenderWindow* IRenderWindow::CreateSDL2Window()
{
    return new SDL2::RenderWindow();
}

/*
 * SDL2::RenderWindow
 */

namespace
{
    int EventWatch(void* ptr, SDL_Event* event)
    {
        SDL2::RenderWindow* wnd = (SDL2::RenderWindow*)ptr;

        if(wnd)
        {
            if(event->type == SDL_QUIT)
                wnd->close();
        }

        return 1;
    }
}

SDL2::RenderWindow::RenderWindow()
    : m_window(nullptr), m_context(nullptr), m_isOpen(false)
{
}

SDL2::RenderWindow::~RenderWindow()
{
    this->destroy();
}

void SDL2::RenderWindow::create(const WindowDef &desc)
{
    this->destroy();

    m_window = SDL_CreateWindow(desc.title.c_str(), 0, 0, desc.size.x, desc.size.y, SDL_WINDOW_OPENGL);
    SDL_AddEventWatch(&EventWatch, (void*)this);
    m_context = SDL_GL_CreateContext(m_window);

    Vector2 wndSize = this->size();
    float aspectRatio = (float)wndSize.x/wndSize.y;

    // initialze opengl
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-5.0f*aspectRatio, 5.0f*aspectRatio, -5, 5, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_TEXTURE_2D);

    m_isOpen = true;
}

void SDL2::RenderWindow::destroy()
{
    if(m_window)
    {
        SDL_GL_DeleteContext(m_context);
        m_context = nullptr;

        SDL_DelEventWatch(&EventWatch, (void*)this);
        SDL_DestroyWindow(m_window);
        m_window = nullptr;

        m_isOpen = false;
    }
}

bool SDL2::RenderWindow::isOpen() const
{
    return m_isOpen;
}

void SDL2::RenderWindow::close()
{
    m_isOpen = false;
}

Vector2 SDL2::RenderWindow::size() const
{
    int w, h;
    SDL_GetWindowSize(m_window, &w, &h);
    return Vector2(w, h);
}

void SDL2::RenderWindow::clear(const Color &color)
{
    glClearColor(color.r, color.g, color.b, color.a);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}

void SDL2::RenderWindow::display()
{
    SDL_PumpEvents();
    SDL_GL_SwapWindow(m_window);
}

#else // cuteNO_SDL2

#include "../IRenderWindow.hpp"

/*
 * IRenderWindow
 */

IRenderWindow* IRenderWindow::CreateSDL2Window()
{
    return nullptr;
}

#endif // cuteNO_SDL2
