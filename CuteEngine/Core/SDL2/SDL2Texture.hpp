#ifndef SDL2TEXTURE_HPP
#define SDL2TEXTURE_HPP

#include <CuteEngine/Core/OpenGL.hpp>
#include <CuteEngine/Core/ResourceMgr.hpp>

namespace cute
{
    class SDL2Texture : public IResource
    {
    public:
        SDL2Texture(const std::string& fileName);
        ~SDL2Texture();

        bool load();

        unsigned int width();
        unsigned int height();
        static void bind(const SDL2Texture* t);

        static SDL2Texture* Create(const std::string& fileName);

    private:
        unsigned int m_width, m_height;
        GLuint m_texture;
    };

    typedef ResourceMgr<SDL2Texture> TextureMgr;
}

#endif // SDL2TEXTURE_HPP
