#ifndef cuteNO_SDL2

#ifndef SDL2RENDERWINDOW_HPP
#define SDL2RENDERWINDOW_HPP

#include "../IRenderWindow.hpp"
#include <SDL.h>

namespace cute
{
    namespace SDL2
    {
        /*!
         * \brief IRenderWindow implmentation for SDL2
         */
        class RenderWindow : public IRenderWindow
        {
        public:
            RenderWindow();
            virtual ~RenderWindow();

            virtual void create(const WindowDef& desc);
            virtual void destroy();

            virtual bool isOpen() const;
            virtual void close();

            virtual Vector2 size() const;

            virtual void clear(const Color &color);
            virtual void display();

        private:
            SDL_Window* m_window;
            SDL_GLContext m_context;
            bool m_isOpen;
        };
    }
}

#endif // SDL2RENDERWINDOW_HPP

#endif // cuteNO_SDL2
