#include <CuteEngine/Core/Camera.hpp>

using namespace cute;

Camera2::Camera2()
    : m_Rotation(0.f)
{

}

Camera2::Camera2(const Camera2 &cam)
    : m_Center(cam.getCenter()), m_Size(cam.getSize()), m_Rotation(cam.getRotation()),
      m_Viewport(cam.getViewport())
{

}

Camera2::~Camera2()
{

}

void Camera2::setCenter(float x, float y)
{
    m_Center.x = x;
    m_Center.y = y;
}

void Camera2::setCenter(const Vector2 &center)
{
    m_Center = center;
}

void Camera2::setRotation(float angle)
{
    m_Rotation = angle;
}

void Camera2::setSize(float width, float height)
{
    m_Size.x = width;
    m_Size.y = height;
}

void Camera2::setSize(const Vector2 &size)
{
    m_Size = size;
}

void Camera2::setViewport(const Rect &rect)
{
    m_Viewport = rect;
}

const Vector2& Camera2::getCenter() const
{
    return m_Center;
}

const Vector2& Camera2::getSize() const
{
    return m_Size;
}

float Camera2::getRotation() const
{
    return m_Rotation;
}

const Rect& Camera2::getViewport() const
{
    return m_Viewport;
}

void Camera2::move(float offsetX, float offsetY)
{
    m_Center.x += offsetX;
    m_Center.y += offsetY;
}

void Camera2::move(const Vector2 &offset)
{
    m_Center += offset;
}

void Camera2::rotate(float angle)
{
    m_Rotation += angle;
}

void Camera2::zoom(float factor)
{
    m_Size *= factor;
}

void Camera2::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Camera2> _Camera;
    _Camera.SquirrelFunc("constructor", &Camera2::SqConstructor);
    _Camera.Overload<void (Camera2::*)(float, float)>("setCenter", &Camera2::setCenter);
    _Camera.Overload<void (Camera2::*)(const Vector2&)>("setCenter", &Camera2::setCenter);
    _Camera.Overload<void (Camera2::*)(float, float)>("setSize", &Camera2::setSize);
    _Camera.Overload<void (Camera2::*)(const Vector2&)>("setSize", &Camera2::setSize);
    _Camera.Func("setRotation", &Camera2::setRotation);
    _Camera.Func("setViewport", &Camera2::setViewport);
    _Camera.Func("getCenter", &Camera2::getCenter);
    _Camera.Func("getSize", &Camera2::getSize);
    _Camera.Func("getRotation", &Camera2::getRotation);
    _Camera.Func("getViewport", &Camera2::getViewport);
    _Camera.Overload<void (Camera2::*)(float, float)>("move", &Camera2::move);
    _Camera.Overload<void (Camera2::*)(const Vector2&)>("move", &Camera2::move);
    _Camera.Func("rotate", &Camera2::rotate);
    _Camera.Func("zoom", &Camera2::zoom);
    Sqrat::RootTable(vm).Bind("Camera2DDef", _Camera);
}

SQInteger Camera2::SqConstructor(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 1) { // no manually put arguments? default constructor. note that the first argument is always a class or a environment in Squirrel functions
        Camera2* instance = new Camera2();
        sq_setinstanceup(vm, 1, instance);
        sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Camera2>::Delete);
        return 0;
    } else if (sq_gettop(vm) == 2) {
        Sqrat::Var<const Camera2&> copy(vm, 2); // copy constructor
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Camera2* instance = new Camera2(copy.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Camera2>::Delete);
            return 0;
        }
        Sqrat::Error::Instance().Clear(vm);
        return sq_throwerror(vm, Sqrat::Error::FormatTypeError(vm, 2, Sqrat::ClassType<Camera2>::ClassName(vm)).c_str());
    }
    return sq_throwerror(vm, _SC("wrong number of parameters"));
}
