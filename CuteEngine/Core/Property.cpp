#include "Property.hpp"
#include "Initializer.hpp"

#define GVT(type, val) template<> inline ValueType GetValueType<type>(){ return val; }

using namespace cute;

/*
 * _IPropImpl
 */

namespace _cute
{
    enum ValueType
    {
        VT_Null,
        VT_Bool,
        VT_Float,
        VT_Vec2,
        VT_String,
        VT_StringList,
        VT_Rect,
        VT_Color
    };

    template<class T>
    inline ValueType GetValueType(){ return VT_Null; }
    GVT(bool, VT_Bool)
    GVT(float, VT_Float)
    GVT(Vector2, VT_Vec2)
    GVT(std::string, VT_String)
    GVT(std::vector<std::string>, VT_StringList)
    GVT(Rect, VT_Rect)
    GVT(Color, VT_Color)

    class IPropImpl
    {
    public:
        virtual ~IPropImpl()
        {
        }

        virtual ValueType type() const = 0;
    };

    template<class T>
    class TPropImpl : public IPropImpl
    {
    public:
        typedef std::function<void(const T&)> Setter;

        T value;
        Setter setter;

        TPropImpl(const T& v, const Setter& s)
            : value(v), setter(s)
        {
        }

        virtual ValueType type() const{ return GetValueType<T>(); }
    };

    template<class T>
    void setter(bool& insideSetter, IPropImpl* data, const T& value)
    {
        if(insideSetter)
            return;

        insideSetter = true;

        if(data->type() == GetValueType<T>())
        {
            TPropImpl<T>* d = (TPropImpl<T>*)data;
            d->value = value;
            d->setter(value);
        }

        insideSetter = false;
    }

    template<class T>
    T get(IPropImpl* data)
    {
        if(data->type() == GetValueType<T>())
        {
            TPropImpl<T>* d = (TPropImpl<T>*)data;
            return d->value;
        }
        return Initializer<T>();
    }
}

/*
 * IPropertyWatch
 */

IPropertyWatch::~IPropertyWatch()
{
}

/*
 * Property
 */

Property::Property(IEditable* owner, const std::string& name, const BoolSetter &setter, bool isMutable)
    : m_owner(owner), m_name(name), m_isMutable(isMutable), m_insideSetter(false), m_insidePropertyChanged(false), m_impl(new _cute::TPropImpl<bool>(Initializer<bool>(), setter)), m_watch(nullptr)
{
}

Property::Property(IEditable* owner, const std::string& name, const FloatSetter &setter, bool isMutable)
    : m_owner(owner), m_name(name), m_isMutable(isMutable), m_insideSetter(false), m_insidePropertyChanged(false), m_impl(new _cute::TPropImpl<float>(Initializer<float>(), setter)), m_watch(nullptr)
{
}

Property::Property(IEditable* owner, const std::string& name, const Vec2Setter &setter, bool isMutable)
    : m_owner(owner), m_name(name), m_isMutable(isMutable), m_insideSetter(false), m_insidePropertyChanged(false), m_impl(new _cute::TPropImpl<Vector2>(Initializer<Vector2>(), setter)), m_watch(nullptr)
{
}

Property::Property(IEditable* owner, const std::string& name, const StringSetter &setter, bool isMutable)
    : m_owner(owner), m_name(name), m_isMutable(isMutable), m_insideSetter(false), m_insidePropertyChanged(false), m_impl(new _cute::TPropImpl<std::string>(Initializer<std::string>(), setter)), m_watch(nullptr)
{
}

Property::Property(IEditable *owner, const std::string &name, const StringListSetter &setter, bool isMutable)
    : m_owner(owner), m_name(name), m_isMutable(isMutable), m_insideSetter(false), m_insidePropertyChanged(false), m_impl(new _cute::TPropImpl<StringList>(Initializer<StringList>(), setter)), m_watch(nullptr)
{
}

Property::Property(IEditable *owner, const std::string &name, const RectSetter &setter, bool isMutable)
    : m_owner(owner), m_name(name), m_isMutable(isMutable), m_insideSetter(false), m_insidePropertyChanged(false), m_impl(new _cute::TPropImpl<Rect>(Initializer<Rect>(), setter)), m_watch(nullptr)
{
}

Property::Property(IEditable *owner, const std::string &name, const ColorSetter &setter, bool isMutable)
    : m_owner(owner), m_name(name), m_isMutable(isMutable), m_insideSetter(false), m_insidePropertyChanged(false), m_impl(new _cute::TPropImpl<Color>(Initializer<Color>(), setter)), m_watch(nullptr)
{
}

Property::~Property()
{
    delete m_impl;
}

IEditable* Property::owner()
{
    return m_owner;
}

const IEditable* Property::owner() const
{
    return m_owner;
}

const std::string& Property::name() const
{
    return m_name;
}

bool Property::isMutable() const
{
    return m_isMutable;
}

void Property::setBool(bool v)
{
    if(m_isMutable && !m_insideSetter)
    {
        _cute::setter(m_insideSetter, m_impl, v);
        this->_propertyChanged();
    }
}

void Property::setFloat(float v)
{
    if(m_isMutable && !m_insideSetter)
    {
        _cute::setter(m_insideSetter, m_impl, v);
        this->_propertyChanged();
    }
}

void Property::setVec2(const Vector2 &v)
{
    if(m_isMutable && !m_insideSetter)
    {
        _cute::setter(m_insideSetter, m_impl, v);
        this->_propertyChanged();
    }
}

void Property::setString(const std::string &v)
{
    if(m_isMutable && !m_insideSetter)
    {
        _cute::setter(m_insideSetter, m_impl, v);
        this->_propertyChanged();
    }
}

void Property::setStringList(const StringList &v)
{
    if(m_isMutable && !m_insideSetter)
    {
        _cute::setter(m_insideSetter, m_impl, v);
        this->_propertyChanged();
    }
}

void Property::setRect(const Rect &v)
{
    if(m_isMutable && !m_insideSetter)
    {
        _cute::setter(m_insideSetter, m_impl, v);
        this->_propertyChanged();
    }
}

void Property::setColor(const Color &v)
{
    if(m_isMutable && !m_insideSetter)
    {
        _cute::setter(m_insideSetter, m_impl, v);
        this->_propertyChanged();
    }
}

void Property::set(const Value &v)
{
    if(v.isBool())
        this->setBool(v.toBool());
    else if(v.isFloat())
        this->setFloat(v.toFloat());
    else if(v.isVec2())
        this->setVec2(v.toVec2());
    else if(v.isString())
        this->setString(v.toString());
    else if(v.isStringList())
        this->setStringList(v.toStringList());
    else if(v.isRect())
        this->setRect(v.toRect());
}

bool Property::isBool() const
{
    return m_impl->type() == _cute::VT_Bool;
}

bool Property::toBool() const
{
    return _cute::get<bool>(m_impl);
}

bool Property::isFloat() const
{
    return m_impl->type() == _cute::VT_Float;
}

float Property::toFloat() const
{
    return _cute::get<float>(m_impl);
}

bool Property::isVec2() const
{
    return m_impl->type() == _cute::VT_Vec2;
}

Vector2 Property::toVec2() const
{
    return _cute::get<Vector2>(m_impl);
}

bool Property::isString() const
{
    return m_impl->type() == _cute::VT_String;
}

std::string Property::toString() const
{
    return _cute::get<std::string>(m_impl);
}

bool Property::isStringList() const
{
    return m_impl->type() == _cute::VT_StringList;
}

Property::StringList Property::toStringList() const
{
    return _cute::get<StringList>(m_impl);
}

bool Property::isRect() const
{
    return m_impl->type() == _cute::VT_Rect;
}

Rect Property::toRect() const
{
    return _cute::get<Rect>(m_impl);
}

bool Property::isColor() const
{
    return m_impl->type() == _cute::VT_Color;
}

Color Property::toColor() const
{
    return _cute::get<Color>(m_impl);
}

Value Property::toValue() const
{
    Value val;
    if(this->isBool()) val.setBool(this->toBool());
    else if(this->isFloat()) val.setFloat(this->toFloat());
    else if(this->isVec2()) val.setVec2(this->toVec2());
    else if(this->isString()) val.setString(this->toString());
    else if(this->isStringList()) val.setStringList(this->toStringList());
    else if(this->isRect()) val.setRect(this->toRect());
    return val;
}

void Property::setWatch(IPropertyWatch *watch)
{
    m_watch = watch;
}

void Property::_propertyChanged()
{
    if(!m_watch || m_insidePropertyChanged)
        return;

    m_insidePropertyChanged = true;
    m_watch->propertyChanged(this);
    m_insidePropertyChanged = false;
}
