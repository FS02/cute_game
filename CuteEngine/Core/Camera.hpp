#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <CuteEngine/Utils/Vector2.hpp>
#include <CuteEngine/Utils/Rect.hpp>

namespace cute
{
    /*!
    * \brief 2D Camera.
    *
    * This class will handle Graphics View and Sound Listener.
    */
    class Camera2
    {
    public:
        /*!
         * \brief Camera default constructor.
         */
        Camera2();

        /*!
         * \brief Camera copy constructor.
         * \param Read only refference to other camera.
         */
        Camera2(const Camera2& cam);

        /*!
         * \brief Camera default destructor.
         */
        ~Camera2();

        /*!
         * \brief Set camera center.
         *
         * Set the camera placement or position
         * \param x coordinate.
         * \param y coordinate.
         */
        void setCenter(float x, float y);

        /*!
         * \brief Set camera center.
         * \param Camera center.
         */
        void setCenter(const cute::Vector2& center);

        /*!
         * \brief Set Camera size or scale.
         * \param width size.
         * \param height size.
         */
        void setSize(float width, float height);

        /*!
         * \brief Set Camera size or scale.
         * \param Size.
         */
        void setSize(const cute::Vector2& size);

        /*!
         * \brief Set camera rotation.
         * \param angle.
         */
        void setRotation(float angle);

        /*!
         * \brief Set viewport.
         * \param rectangle of the viewport.
         */
        void setViewport(const Rect& rect);

        /*!
         * \brief Get camera center.
         * \return camera center.
         */
        const cute::Vector2& getCenter() const;

        /*!
         * \brief Get camera size.
         * \return return camera size.
         */
        const cute::Vector2& getSize() const;

        /*!
         * \brief Get camera rotation.
         * \return camera rotation.
         */
        float getRotation() const;

        /*!
         * \brief Get camera viewport.
         * \return Recto of the viewport.
         */
        const Rect& getViewport() const;

        /*!
         * \brief Move the camera related to current position.
         * \param x offset to move.
         * \param y offset to move.
         */
        void move(float offsetX, float offsetY);

        /*!
         * \brief Move the camera related to current position.
         * \param offset to move.
         */
        void move(const cute::Vector2& offset);

        /*!
         * \brief Rotate camera related to current angle.
         * \param angle to rotate.
         */
        void rotate(float angle);

        /*!
         * \brief Zoom camera
         * \param Zoom factor
         */
        void zoom(float factor);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static SQInteger SqConstructor(HSQUIRRELVM vm);

        cute::Vector2 m_Center;
        cute::Vector2 m_Size;
        float m_Rotation;
        Rect m_Viewport;
    };
}

#endif // CAMERA_HPP
