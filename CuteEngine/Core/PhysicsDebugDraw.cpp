#include <CuteEngine/Core/PhysicsDebugDraw.hpp>
#include <CuteEngine/Core/OpenGL.hpp>
#include <climits>

using namespace cute;

PhysicsDebugDraw::PhysicsDebugDraw(float thickness, float tformScale)
    : m_capacity(4096), m_count(0), m_thickness(thickness), m_xfScale(tformScale)
{
    m_data = new Vertex[m_capacity];
}

PhysicsDebugDraw::~PhysicsDebugDraw()
{
    delete[] m_data;
}

void PhysicsDebugDraw::setThickness(float t)
{
    m_thickness = t;
}

void PhysicsDebugDraw::setTransformScale(float s)
{
    m_xfScale = s;
}

void PhysicsDebugDraw::DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
{
    for(int i = 0; i < vertexCount; ++i)
    {
        int next = i + 1;
        if(next == vertexCount)
            next = 0;
        this->DrawSegment(vertices[i], vertices[next], color);
    }
}

void PhysicsDebugDraw::DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
{
    // TODO!!!

//    Vertex v;
//    v.r = (unsigned char)(color.r * 255.0f);
//    v.g = (unsigned char)(color.g * 255.0f);
//    v.b = (unsigned char)(color.b * 255.0f);
//    v.a = 255;

//    if(m_count + vertexCount > m_capacity)
//        this->flush();

    this->DrawPolygon(vertices, vertexCount, color);
}

void PhysicsDebugDraw::DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color)
{
    constexpr int segments = 16;
    constexpr float step = 2.0f*M_PI / segments;
    float angle = 0.0f;
    b2Vec2 a, b;
    for(int i = 0; i < segments; ++i)
    {
        a.x = std::cos(angle)*radius + center.x;
        a.y = std::sin(angle)*radius + center.y;
        b.x = std::cos(angle + step)*radius + center.x;
        b.y = std::sin(angle + step)*radius + center.y;
        this->DrawSegment(a, b, color);

        angle += step;
    }
}

void PhysicsDebugDraw::DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color)
{
    constexpr int segments = 16;
    constexpr float step = 2.0f*M_PI / segments;
    float angle = 0.0f;
    Vertex c, a, b;
    c.x = center.x;
    c.y = center.y;
    c.r = (unsigned char)(color.r * 255.0f);
    c.g = (unsigned char)(color.g * 255.0f);
    c.b = (unsigned char)(color.b * 255.0f);
    c.a = 255;
    a = b = c;

    if(m_count + segments*3 > m_capacity)
        this->flush();

    for(int i = 0; i < segments; ++i)
    {
        a.x = std::cos(angle)*radius + center.x;
        a.y = std::sin(angle)*radius + center.y;
        b.x = std::cos(angle + step)*radius + center.x;
        b.y = std::sin(angle + step)*radius + center.y;

        m_data[m_count++] = c;
        m_data[m_count++] = a;
        m_data[m_count++] = b;

        angle += step;
    }

    b2Vec2 e = axis;
    e *= radius;
    e += center;
    this->DrawSegment(center, e, color);
}

void PhysicsDebugDraw::DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color)
{
    b2Vec2 v = p2 - p1;
    v.Normalize();
    v *= m_thickness * 0.5f;

    Vertex vert;
    vert.r = (unsigned char)(color.r * 255.0f);
    vert.g = (unsigned char)(color.g * 255.0f);
    vert.b = (unsigned char)(color.b * 255.0f);
    vert.a = 255;

    if(m_count + 6 > m_capacity)
        this->flush();

    // first triangle
    vert.x = p1.x - v.y;
    vert.y = p1.y + v.x;
    m_data[m_count++] = vert;

    vert.x = p1.x + v.y;
    vert.y = p1.y - v.x;
    m_data[m_count++] = vert;

    vert.x = p2.x - v.y;
    vert.y = p2.y + v.x;
    m_data[m_count++] = vert;

    // second triangle
    vert.x = p2.x - v.y;
    vert.y = p2.y + v.x;
    m_data[m_count++] = vert;

    vert.x = p1.x + v.y;
    vert.y = p1.y - v.x;
    m_data[m_count++] = vert;

    vert.x = p2.x + v.y;
    vert.y = p2.y - v.x;
    m_data[m_count++] = vert;
}

void PhysicsDebugDraw::DrawTransform(const b2Transform &xf)
{
    b2Vec2 a, b;
    a = xf.q.GetXAxis();
    a *= m_xfScale;
    a += xf.p;
    b = xf.q.GetYAxis();
    b *= m_xfScale;
    b += xf.p;

    this->DrawSegment(xf.p, a, b2Color(1, 0, 0));
    this->DrawSegment(xf.p, b, b2Color(0, 1, 0));
}

void PhysicsDebugDraw::flush()
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(0, 0, 0.9f);

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, sizeof(Vertex), m_data);
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Vertex), &m_data[0].r);

    glDrawArrays(GL_TRIANGLES, 0, m_count);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

    glPopMatrix();

    m_count = 0;
}
