#ifndef PHYSICSDEBUGDRAW_HPP
#define PHYSICSDEBUGDRAW_HPP

#include <Box2D/Box2D.h>
#include <CuteEngine/Utils/Vertex.hpp>

namespace cute
{
    /*!
     * \brief Renders debug information about physics.
     * Implements Box2D's interface allowing to render useful debug information, like bounding boxes, joints, etc.
     */
    class PhysicsDebugDraw : public b2Draw
    {
    public:
        /*!
         * \brief Contructor.
         * \param thickness Thickness of segments.
         * \param tformScale Size of transformation information (length local y and x axes).
         */
        PhysicsDebugDraw(float thickness = 0.01f, float tformScale = 0.1f);
        PhysicsDebugDraw(const PhysicsDebugDraw&) = delete;
        virtual ~PhysicsDebugDraw();

        //! Sets thickness of segments.
        void setThickness(float t);
        //! Sets size of transformation information (length of local y and x axes).
        void setTransformScale(float s);

        virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
        virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
        virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
        virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
        virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
        virtual void DrawTransform(const b2Transform& xf);

        //! Drawing is batched to improve performance. Call this method to ensure everything is drawn.
        void flush();

    private:
        const unsigned int m_capacity;
        unsigned int m_count;
        Vertex* m_data;
        float m_thickness, m_xfScale;
    };
}

#endif // PHYSICSDEBUGDRAW_HPP
