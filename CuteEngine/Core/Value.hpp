#ifndef VALUE_HPP
#define VALUE_HPP

#include <CuteEngine/Utils/Vector2.hpp>
#include <CuteEngine/Utils/Rect.hpp>
#include <CuteEngine/Utils/Color.h>
#include <string>
#include <vector>

namespace _cute{ class IValueImpl; }

namespace cute
{
    class Value
    {
    public:
        typedef std::vector<std::string> StringList;

        Value();
        Value(const Value& cp);
        virtual ~Value();

        Value& operator =(const Value& rv);
        bool operator ==(const Value& rv) const;

        void setBool(bool v);
        bool isBool() const;
        bool toBool() const;

        void setFloat(float v);
        bool isFloat() const;
        float toFloat() const;

        void setVec2(const cute::Vector2& v);
        bool isVec2() const;
        cute::Vector2 toVec2() const;

        virtual void setString(const std::string& v);
        bool isString() const;
        std::string toString() const;

        void setStringList(const StringList& v);
        bool isStringList() const;
        StringList toStringList() const;

        void setRect(const Rect& v);
        bool isRect() const;
        Rect toRect() const;

        void setColor(const Color& v);
        bool isColor() const;
        Color toColor() const;

        static Value FromBool(const bool& v);
        static Value FromFloat(const float& v);
        static Value FromVec2(const cute::Vector2& v);
        static Value FromString(const std::string& v);
        static Value FromStringList(const StringList& v);
        static Value FromRect(const cute::Rect& v);
        static Value FromColor(const cute::Color& v);

    private:
        _cute::IValueImpl* m_impl;
    };
}

#endif // VALUE_HPP
