#ifndef PROPERTY_HPP
#define PROPERTY_HPP

#include <string>
#include <CuteEngine/Utils/Vector2.hpp>
#include <CuteEngine/Utils/Rect.hpp>
#include <CuteEngine/Core/Value.hpp>
#include <functional>
#include <CuteEngine/Utils/Color.h>

namespace _cute{ class IPropImpl; }

namespace cute
{
    class Property;
    class IEditable;

    class IPropertyWatch
    {
    public:
        virtual ~IPropertyWatch();

        virtual void propertyChanged(Property* prop) = 0;
    };

    class Property
    {
    public:
        typedef std::vector<std::string> StringList;

        typedef std::function<void(const bool&)> BoolSetter;
        typedef std::function<void(const float&)> FloatSetter;
        typedef std::function<void(const cute::Vector2&)> Vec2Setter;
        typedef std::function<void(const std::string&)> StringSetter;
        typedef std::function<void(const StringList&)> StringListSetter;
        typedef std::function<void(const cute::Rect&)> RectSetter;
        typedef std::function<void(const cute::Color&)> ColorSetter;

        Property(IEditable* owner, const std::string& name, const BoolSetter& setter, bool isMutable);
        Property(IEditable* owner, const std::string& name, const FloatSetter& setter, bool isMutable);
        Property(IEditable* owner, const std::string& name, const Vec2Setter& setter, bool isMutable);
        Property(IEditable* owner, const std::string& name, const StringSetter& setter, bool isMutable);
        Property(IEditable* owner, const std::string& name, const StringListSetter& setter, bool isMutable);
        Property(IEditable* owner, const std::string& name, const RectSetter& setter, bool isMutable);
        Property(IEditable *owner, const std::string& name, const ColorSetter& setter, bool isMutable);
        Property(IEditable* owner, const std::string& name, const Property&) = delete;
        ~Property();

        Property& operator =(const Property&) = delete;

        IEditable* owner();
        const IEditable* owner() const;
        const std::string& name() const;
        bool isMutable() const;

        void setBool(bool v);
        void setFloat(float v);
        void setVec2(const cute::Vector2& v);
        void setString(const std::string& v);
        void setStringList(const StringList& v);
        void setRect(const cute::Rect& v);
        void setColor(const cute::Color& v);
        void set(const Value& v);

        bool isBool() const;
        bool toBool() const;

        bool isFloat() const;
        float toFloat() const;

        bool isVec2() const;
        cute::Vector2 toVec2() const;

        bool isString() const;
        std::string toString() const;

        bool isStringList() const;
        StringList toStringList() const;

        bool isRect() const;
        cute::Rect toRect() const;

        bool isColor() const;
        cute::Color toColor() const;

        Value toValue() const;

        void setWatch(IPropertyWatch* watch);

    private:
        IEditable* m_owner;
        const std::string m_name;
        const bool m_isMutable;
        bool m_insideSetter;
        bool m_insidePropertyChanged;
        _cute::IPropImpl* m_impl;
        IPropertyWatch* m_watch;

        void _propertyChanged();
    };
}

#endif // PROPERTY_HPP
