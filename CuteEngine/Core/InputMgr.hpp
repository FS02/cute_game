#ifndef INPUTMGR_HPP
#define INPUTMGR_HPP

#include <CuteEngine/Utils/Vector2.hpp>
#include <SDL.h>
#include <string>
#include <map>

namespace cute
{
    enum ControlState
    {
        CS_Up,
        CS_JustPressed,
        CS_Down,
        CS_JustReleased
    };

    enum ControlType
    {
        CT_Key,
        CT_MouseButton
    };

    enum MouseButton
    {
        MB_Invalid = 0,
        MB_Left = 1,
        MB_Middle = 2,
        MB_Right = 3
    };

    struct CtrlData
    {
        ControlType type;
        SDL_Scancode key;
        MouseButton mouseBtt;
        ControlState state;
    };

    class InputMgr
    {
    public:
        void clear();
        bool loadFromJSON(const std::string& fileName);

        void setKeyBinding(const std::string& id, SDL_Scancode code);
        void setMouseBinding(const std::string& id, MouseButton btt);

        ControlState controlState(const std::string& id) const;

        bool control(const std::string& id) const;
        bool controlDown(const std::string& id) const;
        bool controlUp(const std::string& id) const;

        Vector2 pointerPos() const;
        Vector2 mouseWheel() const;

        void update();
        const SDL_Event& SDLEvent() const;

        static SDL_Scancode KeyNameToCode(const std::string& name);
        static MouseButton MouseButtonNameToCode(const std::string& name);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        std::map<std::string, CtrlData> m_controls;
        Vector2 m_pointerPos;
        int m_mouseWheelDir;
        SDL_Event m_event;
    };
}

#endif // INPUTMGR_HPP
