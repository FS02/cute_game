#ifndef RESOURCEMGR_HPP
#define RESOURCEMGR_HPP

#include <string>
#include <map>
#include <functional>
#include <iostream>
#include <queue>
#include <vector>

namespace cute
{
    static std::string ResourcePath = "Data/Resources/";

    /*!
     * \brief Interface for resources.
     * Since ResourceMgr is template class, resources don't have to be derived from IResource, but they have to provide the same methods as this class.
     * This class is provided for convenience.
     */
    class IResource
    {
    public:
        //! Resources are loaded using the load method.
        inline IResource(const std::string& fileName);
        virtual ~IResource(){}

        inline const std::string& fileName() const;

        virtual bool load() = 0;

    protected:
        const std::string m_fileName;
    };

    //! Policy of resource loading.
    enum LoadPolicy
    {
        LP_Immediate,   //!< ResourceMgr will load the resource immediately
        LP_Deferred     //!< ResourceMgr will load the resource in next call to ResourceMgr::deferredLoad
    };

    template<class ResT>
    class ResourceMgrMonitor
    {
    public:
        virtual ~ResourceMgrMonitor(){}

        virtual void onResourceAdded(ResT* ptr) = 0;
    };

    template<class ResT>
    /*!
     * \brief Generic resource manager.
     * Stores a map with all resources and allows fast searching by file name.
     *
     * Advice: use typedefs, e.g.:
     * typedef ResourceMgr<SpriteResource> SpriteResourceMgr; // or SpriteMgr...
     */
    class ResourceMgr
    {
    public:
        typedef std::function<ResT*(const std::string& fileName)> FactoryFunc;

        //! Pass function which which allocates new instance of ResT.
        inline ResourceMgr(const FactoryFunc& func);
        ResourceMgr(const ResourceMgr<ResT>&) = delete;
        inline ~ResourceMgr();

        /*!
         * \brief Sets load policy. See LoadPolicy. Should be called during initialization.
         * \param policy Loading policy.
         */
        inline void setLoadPolicy(LoadPolicy policy);

        /*!
         * \brief Obtain a pointer to resource.
         * If the file hasn't been loaded yet, resource mgr. will create a resource and try to load it.
         * \param fileName
         * \return Pointer to the resource.
         */
        inline ResT* resource(const std::string& fileName);

        /*!
         * \brief Loads all resources in the queue. Useful only when ResourceMgr works with LP_Deferred policy.
         */
        inline void deferredLoad();
        /*!
         * \brief Unloads all resources. Use it wisely, dangling pointers are not fun!
         */
        inline void unloadAll();

        /*!
         * \brief Iterates over all resources, and calls func for each of them.
         * \param func Callback function. If it returns false, method returns immediately.
         * \return Returns number of visited resources.
         */
        inline int forEachResource(const std::function<bool(ResT*)>& func);

        inline void addMonitor(ResourceMgrMonitor<ResT>* mon);
        inline void removeMonitor(ResourceMgrMonitor<ResT>* mon);

    private:
        FactoryFunc m_factoryFunc;
        std::map<std::string, ResT*> m_resources;
        LoadPolicy m_loadPolicy;
        std::queue<ResT*> m_loadQueue;
        std::vector<ResourceMgrMonitor<ResT>*> m_monitors;

        inline void _onResAdded(ResT* ptr);
    };

    /*
     * Implementation
     */

    // IResource

    IResource::IResource(const std::string &fileName)
        : m_fileName(fileName)
    {
    }

    const std::string& IResource::fileName() const
    {
        return m_fileName;
    }

    // ResourceMgr<>

    template<class ResT>
    ResourceMgr<ResT>::ResourceMgr(const FactoryFunc& func)
        : m_factoryFunc(func), m_loadPolicy(LP_Immediate)
    {
    }

    template<class ResT>
    ResourceMgr<ResT>::~ResourceMgr()
    {
        for(auto iter = m_resources.begin(); iter != m_resources.end(); ++iter)
            delete iter->second;
    }

    template<class ResT>
    void ResourceMgr<ResT>::setLoadPolicy(LoadPolicy policy)
    {
        m_loadPolicy = policy;
    }

    template<class ResT>
    ResT* ResourceMgr<ResT>::resource(const std::string &fileName)
    {
        auto iter = m_resources.find(fileName);
        if(iter != m_resources.end())
            return iter->second;

        ResT* ptr = m_factoryFunc(fileName);
        m_resources[fileName] = ptr;

        if(m_loadPolicy == LP_Immediate)
        {
            if(!ptr->load())
                std::clog << "Could not load a resource from " << fileName << "\n";
        }
        else if(m_loadPolicy == LP_Deferred)
            m_loadQueue.push(ptr);

        this->_onResAdded(ptr);

        return ptr;
    }

    template<class ResT>
    void ResourceMgr<ResT>::deferredLoad()
    {
        while(!m_loadQueue.empty())
        {
            if(!m_loadQueue.front()->load())
                std::clog << "Could not load a resource from " << m_loadQueue.front()->fileName() << "\n";
            m_loadQueue.pop();
        }
    }

    template<class ResT>
    void ResourceMgr<ResT>::unloadAll()
    {
        for(auto iter = m_resources.begin(); iter != m_resources.end(); ++iter)
            delete iter->second;
        m_resources.clear();
    }

    template<class ResT>
    int ResourceMgr<ResT>::forEachResource(const std::function<bool(ResT*)>& func)
    {
        if(!func)
            return 0;

        int n = 0;
        for(auto& pr : m_resources)
        {
            ++n;
            if(!func(pr.second))
                return n;
        }

        return n;
    }

    template<class ResT>
    void ResourceMgr<ResT>::addMonitor(ResourceMgrMonitor<ResT>* mon)
    {
        m_monitors.push_back(mon);
    }

    template<class ResT>
    void ResourceMgr<ResT>::removeMonitor(ResourceMgrMonitor<ResT>* mon)
    {
        for(size_t i = 0; i < m_monitors.size(); ++i)
        {
            if(m_monitors[i] == mon)
            {
                m_monitors[i] = m_monitors.back();
                m_monitors.pop_back();
                return;
            }
        }
    }

    template<class ResT>
    void ResourceMgr<ResT>::_onResAdded(ResT* ptr)
    {
        for(auto mon : m_monitors)
            mon->onResourceAdded(ptr);
    }
}

#endif // RESOURCEMGR_HPP
