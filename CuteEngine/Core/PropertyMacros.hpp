#ifndef PROPERTYMACROS_HPP
#define PROPERTYMACROS_HPP

#define cuteEDITABLE protected: virtual void _setupPropertyStore();

#define cutePROPERTY(type, name, member)  \
    public: inline void set##name(const type& value){ m_propStore->setProperty(#name, value); member = value; } inline type get##name() const{ return member; }

#define cutePROPERTY_UPDATE(type, name, member, updateMethod)   \
    public: inline void set##name(const type& value){ m_propStore->setProperty(#name, value); member = value; this->updateMethod(#name); } inline type get##name() const{ return member; }

#define cutePROPERTY_WITH_SETTER_AND_GETTER(type, name, setter, getter) \
    public: inline void set##name(const type& value){ m_propStore->setProperty(#name, this->setter(value)); } inline type get##name() const{ return this->getter(); }

#define cutePROPERTY_READ_ONLY(type, name, member)  \
    public inline type get##name() const{ return member; }

#define cuteBEGIN_PROPERTY_IMPL(type) void type::_setupPropertyStore() {

#define cutePROPERTY_IMPL(Type, name, isMutable) m_propStore->initProperty(#name, std::function<void(const Type&)>(std::bind(&std::remove_pointer<decltype(this)>::type::set##name, this, std::placeholders::_1)), isMutable);

#define cutePROPERTY_READ_ONLY_IMPL(Type, name) m_propStore->initProperty(#name, std::function<void(const Type&)>(), false);

#define cuteEND_PROPERTY_IMPL }

#define cutePREPARE_PROPERTIES() std::remove_pointer<decltype(this)>::type::_setupPropertyStore()

#endif // PROPERTYMACROS_HPP
