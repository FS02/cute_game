#include <CuteEngine/Core/InputMgr.hpp>
#include <CuteEngine/Utils/JSON.hpp>
#include <fstream>
#include <iostream>

using namespace cute;

/*
 * InputMgr
 */

void InputMgr::clear()
{
    m_controls.clear();
}

bool InputMgr::loadFromJSON(const std::string &fileName)
{
    json::Document doc;

    std::ifstream file(fileName.c_str());
    json::StdTokenStream tokenStr(file);
    json::StdErrorCallback errorCb(std::clog, fileName);

    if(!doc.load(&tokenStr, &errorCb))
        return false;

    json::ValueHandle hRoot = doc.root();
    for(unsigned int i = 0; i < hRoot.numChildren(); ++i)
    {
        json::PairHandle hBnd = hRoot.childPair(i);
        std::string type = hBnd.value().childPair("Type").value().string();
        if(type == "Key")
        {
            std::string name = hBnd.value().childPair("Key").value().string();
            SDL_Scancode code = KeyNameToCode(name);
            this->setKeyBinding(hBnd.name(), code);
        }
        else if(type == "Mouse")
        {
            std::string name = hBnd.value().childPair("Button").value().string();
            MouseButton code = MouseButtonNameToCode(name);
            this->setMouseBinding(hBnd.name(), code);
        }
    }

    return true;
}

void InputMgr::setKeyBinding(const std::string &id, SDL_Scancode code)
{
    CtrlData data;
    data.type = CT_Key;
    data.key = code;
    data.state = CS_Up;
    m_controls[id] = data;
}

void InputMgr::setMouseBinding(const std::string &id, MouseButton btt)
{
    if(btt == MB_Invalid)
        return;

    CtrlData data;
    data.type = CT_MouseButton;
    data.mouseBtt = btt;
    data.state = CS_Up;
    m_controls[id] = data;
}

ControlState InputMgr::controlState(const std::string &id) const
{
    auto iter = m_controls.find(id);
    if(iter != m_controls.end())
        return iter->second.state;
    return CS_Up;
}

bool InputMgr::control(const std::string &id) const
{
    ControlState cs = this->controlState(id);
    return (cs == CS_Down || cs == CS_JustPressed);
}

bool InputMgr::controlDown(const std::string &id) const
{
    ControlState cs = this->controlState(id);
    return (cs == CS_JustPressed);
}

bool InputMgr::controlUp(const std::string &id) const
{
    ControlState cs = this->controlState(id);
    return (cs == CS_JustReleased);
}

Vector2 InputMgr::pointerPos() const
{
    return m_pointerPos;
}

Vector2 InputMgr::mouseWheel() const
{
    return Vector2(m_event.wheel.x, m_event.wheel.y);
}

void InputMgr::update()
{
    SDL_PumpEvents(); // updates state of keyboard, mouse, etc.

    const Uint8* keys = SDL_GetKeyboardState(nullptr);
    int mx = (int)m_pointerPos.x, my = (int)m_pointerPos.y;
    Uint32 msButtons = SDL_GetMouseState(&mx, &my);
    m_pointerPos.Set((float)mx, (float)my);

    for(auto& pr : m_controls)
    {
        CtrlData& cd = pr.second;

        if(cd.type == CT_Key)
        {
            if(keys[cd.key])
            {
                if(cd.state == CS_Up || cd.state == CS_JustReleased)
                    cd.state = CS_JustPressed;
                else
                    cd.state = CS_Down;
            }
            else
            {
                if(cd.state == CS_Down || cd.state == CS_JustPressed)
                    cd.state = CS_JustReleased;

                else
                    cd.state = CS_Up;
            }
        }
        else if(cd.type == CT_MouseButton)
        {
            if(SDL_BUTTON(cd.mouseBtt) & msButtons)
            {
                if(cd.state == CS_Up || cd.state == CS_JustReleased)
                    cd.state = CS_JustPressed;
                else
                    cd.state = CS_Down;
            }
            else
            {
                if(cd.state == CS_Down || cd.state == CS_JustPressed)
                    cd.state = CS_JustReleased;
                else
                    cd.state = CS_Up;
            }
        }
    }

    SDL_PollEvent(&m_event);
}

const SDL_Event& InputMgr::SDLEvent() const
{
    return m_event;
}

SDL_Scancode InputMgr::KeyNameToCode(const std::string &name)
{
    return SDL_GetScancodeFromName(name.c_str());
}

MouseButton InputMgr::MouseButtonNameToCode(const std::string &name)
{
    static const std::string buttons[] =
    {
        "Invalid",
        "Left",
        "Middle",
        "Right"
    };
    static const int numButtons = sizeof(buttons)/sizeof(buttons[0]);

    for(int i = 0; i < numButtons; ++i)
    {
        if(buttons[i] == name)
            return (MouseButton)i;
    }

    return MB_Invalid;
}

void InputMgr::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Enumeration _ControlState;
    _ControlState.Const("CS_Up", 0);
    _ControlState.Const("CS_JustPressed", 1);
    _ControlState.Const("CS_Down", 2);
    _ControlState.Const("CS_JustReleased",3);
    Sqrat::RootTable(vm).Bind("ControlState", _ControlState);

    Sqrat::Class<InputMgr> _InputMgr;
    _InputMgr.Func("clear", &InputMgr::clear);
    _InputMgr.Func("controlState", &InputMgr::controlState);
    _InputMgr.Func("control", &InputMgr::control);
    _InputMgr.Func("controlDown", &InputMgr::controlDown);
    _InputMgr.Func("controlUp", &InputMgr::controlUp);
    _InputMgr.Func("loadFromJSON", &InputMgr::loadFromJSON);
    _InputMgr.Func("pointerPos", &InputMgr::pointerPos);
    _InputMgr.Func("mouseWheel", &InputMgr::mouseWheel);
    Sqrat::RootTable(vm).Bind("InputMgr", _InputMgr);
}
