#ifndef IEDITABLE_HPP
#define IEDITABLE_HPP

#include <CuteEngine/Core/Property.hpp>
#include <CuteEngine/Core/Initializer.hpp>
#include <CuteEngine/Utils/JSON.hpp>
#include <CuteEngine/Core/ConfStorage.hpp>
#include <vector>
#include <functional>
#include <type_traits>
#include "PropertyMacros.hpp"

namespace cute
{
    class IEditable;
    typedef std::vector<Property*> PropList;

    class PropertyStore
    {
    public:
        PropertyStore(IEditable* owner);
        PropertyStore(const PropertyStore&) = delete;
        ~PropertyStore();

        PropList listProperties();

        void initProperty(const std::string &name, const Property::BoolSetter& setter, bool isMutable);
        void initProperty(const std::string &name, const Property::FloatSetter& setter, bool isMutable);
        void initProperty(const std::string &name, const Property::Vec2Setter& setter, bool isMutable);
        void initProperty(const std::string &name, const Property::StringSetter& setter, bool isMutable);
        void initProperty(const std::string &name, const Property::StringListSetter &setter, bool isMutable);
        void initProperty(const std::string &name, const Property::RectSetter &setter, bool isMutable);
        void initProperty(const std::string &name, const Property::ColorSetter &setter, bool isMutable);

        void setProperty(const std::string &name, bool value);
        void setProperty(const std::string &name, float value);
        void setProperty(const std::string &name, const cute::Vector2& value);
        void setProperty(const std::string &name, const std::string& value);
        void setProperty(const std::string &name, const std::vector<std::string>& value);
        void setProperty(const std::string &name, const cute::Rect& value);
        void setProperty(const std::string &name, const cute::Color &value);

        void toJSON(json::ObjectValue* obj) const;

    private:
        IEditable* m_owner;
        std::vector<Property*> m_props;

        Property* _get(const std::string& name);
        void _rm(const std::string& name);
    };

    /*!
     * \brief An object with properties.
     * Added to simplify code in the editor - thanks to it we can use the same code for IComponent and IComponentDesc.
     */
    class IEditable
    {
    public:
        IEditable();
        virtual ~IEditable();

        //! Returns a list of all properties exposed to the editor.
        PropList listProperties();
        void setProperty(const std::string& name, json::ValueHandle data);
        void setProperty(const std::string &name, const Value& value);
        void loadState(const ConfStorage& storage, const std::string& prefix);
        json::ObjectValue* toJSON() const;

    protected:
        PropertyStore* m_propStore;

        virtual void _setupPropertyStore();
    };
}

#endif // IEDITABLE_HPP
