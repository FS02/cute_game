#include "IEditable.hpp"
#include <cassert>

using namespace cute;

/*
 * PropertyStore
 */

PropertyStore::PropertyStore(IEditable* owner)
    : m_owner(owner)
{
}

PropertyStore::~PropertyStore()
{
    for(Property* prop : m_props)
        delete prop;
}
PropList PropertyStore::listProperties()
{
    return m_props;
}

void PropertyStore::initProperty(const std::string &name, const Property::BoolSetter &setter, bool isMutable)
{
    this->_rm(name);
    m_props.push_back(new Property(m_owner, name, setter, isMutable));
}

void PropertyStore::initProperty(const std::string &name, const Property::FloatSetter &setter, bool isMutable)
{
    this->_rm(name);
    m_props.push_back(new Property(m_owner, name, setter, isMutable));
}

void PropertyStore::initProperty(const std::string &name, const Property::Vec2Setter &setter, bool isMutable)
{
    this->_rm(name);
    m_props.push_back(new Property(m_owner, name, setter, isMutable));
}

void PropertyStore::initProperty(const std::string &name, const Property::StringSetter &setter, bool isMutable)
{
    this->_rm(name);
    m_props.push_back(new Property(m_owner, name, setter, isMutable));
}

void PropertyStore::initProperty(const std::string &name, const Property::StringListSetter &setter, bool isMutable)
{
    this->_rm(name);
    m_props.push_back(new Property(m_owner, name, setter, isMutable));
}

void PropertyStore::initProperty(const std::string &name, const Property::RectSetter &setter, bool isMutable)
{
    this->_rm(name);
    m_props.push_back(new Property(m_owner, name, setter, isMutable));
}

void PropertyStore::initProperty(const std::string &name, const Property::ColorSetter &setter, bool isMutable)
{
    this->_rm(name);
    m_props.push_back(new Property(m_owner, name, setter, isMutable));
}

void PropertyStore::setProperty(const std::string &name, bool value)
{
    Property* prop = this->_get(name);
    if(prop)
        prop->setBool(value);
}

void PropertyStore::setProperty(const std::string &name, float value)
{
    Property* prop = this->_get(name);
    if(prop)
        prop->setFloat(value);
}

void PropertyStore::setProperty(const std::string &name, const Vector2 &value)
{
    Property* prop = this->_get(name);
    if(prop)
        prop->setVec2(value);
}

void PropertyStore::setProperty(const std::string &name, const std::string &value)
{
    Property* prop = this->_get(name);
    if(prop)
        prop->setString(value);
}

void PropertyStore::setProperty(const std::string &name, const std::vector<std::string> &value)
{
    Property* prop = this->_get(name);
    if(prop)
        prop->setStringList(value);
}

void PropertyStore::setProperty(const std::string &name, const Rect &value)
{
    Property* prop = this->_get(name);
    if(prop)
        prop->setRect(value);
}

void PropertyStore::setProperty(const std::string &name, const Color &value)
{
    Property* prop = this->_get(name);
    if(prop)
        prop->setColor(value);
}

void PropertyStore::toJSON(json::ObjectValue *obj) const
{
    for(size_t i = 0; i < m_props.size(); ++i)
    {
        const Property* prop = m_props[i];
        json::Pair* pr = new json::Pair(prop->name());
        obj->addChild(pr);

        if(prop->isBool())
            pr->value = new json::BoolValue(prop->toBool());
        else if(prop->isFloat())
            pr->value = new json::NumberValue((double)prop->toFloat());
        else if(prop->isVec2())
        {
            json::ArrayValue* arr = new json::ArrayValue();
            Vector2 v = prop->toVec2();
            arr->appendChild(new json::NumberValue((double)v.x));
            arr->appendChild(new json::NumberValue((double)v.y));
            pr->value = arr;
        }
        else if(prop->isString())
            pr->value = new json::StringValue(prop->toString());
        else if(prop->isStringList())
        {
            json::ArrayValue* arr = new json::ArrayValue();
            Property::StringList list = prop->toStringList();
            for(const auto& str : list)
                arr->appendChild(new json::StringValue(str));
            pr->value = arr;
        }
    }
}

Property* PropertyStore::_get(const std::string &name)
{
    for(Property* prop : m_props)
        if(prop->name() == name)
            return prop;
    return nullptr;
}

void PropertyStore::_rm(const std::string &name)
{
    for(size_t i = 0; i < m_props.size(); ++i)
    {
        if(m_props[i]->name() == name)
        {
            delete m_props[i];
            m_props[i] = m_props.back();
            m_props.pop_back();
            return;
        }
    }
}

/*
 * IEditable
 */

IEditable::IEditable()
    : m_propStore(new PropertyStore(this))
{
    cutePREPARE_PROPERTIES();
}

IEditable::~IEditable()
{
    delete m_propStore;
}

PropList IEditable::listProperties()
{
    return m_propStore->listProperties();
}

void IEditable::setProperty(const std::string &name, json::ValueHandle data)
{
    if(data.type() == json::VT_Array)
    {
        if(data.numChildren() == 2 && data.childValue(0).type() == json::VT_Number && data.childValue(1).type() == json::VT_Number)
        {
            Vector2 v;
            v.x = data.childValue(0).number();
            v.y = data.childValue(1).number();
            m_propStore->setProperty(name, v);
        }
        else
        {
            Property::StringList list;
            for(unsigned int i = 0; i < data.numChildren(); ++i)
                list.push_back(data.childValue(i).string());
            m_propStore->setProperty(name, list);
        }
    }
    else if(data.type() == json::VT_Boolean)
        m_propStore->setProperty(name, data.boolean());
    else if(data.type() == json::VT_Number)
        m_propStore->setProperty(name, (float)data.number());
    else if(data.type() == json::VT_String)
        m_propStore->setProperty(name, data.string());
}

void IEditable::setProperty(const std::string &name, const Value &value)
{
    if(value.isBool())
        m_propStore->setProperty(name, value.toBool());
    else if(value.isFloat())
        m_propStore->setProperty(name, value.toFloat());
    else if(value.isVec2())
        m_propStore->setProperty(name, value.toVec2());
    else if(value.isString())
        m_propStore->setProperty(name, value.toString());
    else if(value.isStringList())
        m_propStore->setProperty(name, value.toStringList());
}

void IEditable::loadState(const ConfStorage &storage, const std::string &prefix)
{
    PropList props = this->listProperties();
    for(Property* prop : props)
    {
        std::string name = prefix + prop->name();
        if(prop->isBool() && storage.isBool(name))
            m_propStore->setProperty(prop->name(), storage.toBool(name));
        else if(prop->isFloat() && storage.isFloat(name))
            m_propStore->setProperty(prop->name(), storage.toFloat(name));
        else if(prop->isVec2() && storage.isVec2(name))
            m_propStore->setProperty(prop->name(), storage.toVec2(name));
        else if(prop->isString() && storage.isString(name))
            m_propStore->setProperty(prop->name(), storage.toString(name));
        else if(prop->isStringList() && storage.isStringList(name))
            m_propStore->setProperty(prop->name(), storage.toStringList(name));
    }
}

json::ObjectValue* IEditable::toJSON() const
{
    json::ObjectValue* obj = new json::ObjectValue();
    m_propStore->toJSON(obj);
    return obj;
}

void IEditable::_setupPropertyStore()
{
}
