#include "ConfStorage.hpp"

using namespace cute;

namespace
{
    enum ValueType
    {
        VT_Null,
        VT_Bool,
        VT_Float,
        VT_Vec2,
        VT_Rect,
        VT_String,
        VT_StringList,
        VT_Color
    };
}

bool ConfStorage::isEmtpy() const
{
    return m_keys.empty();
}

void ConfStorage::clear()
{
    m_keys.clear();
    m_bools.clear();
    m_floats.clear();
    m_vectors.clear();
    m_strings.clear();
}

void ConfStorage::addValue(const std::string &key, bool value)
{
    Value val;
    val.type = VT_Bool;
    val.index = m_bools.size();
    m_bools.push_back(value);
    m_keys[key] = val;
}

void ConfStorage::addValue(const std::string &key, float value)
{
    Value val;
    val.type = VT_Float;
    val.index = m_floats.size();
    m_floats.push_back(value);
    m_keys[key] = val;
}

void ConfStorage::addValue(const std::string &key, const Vector2& value)
{
    Value val;
    val.type = VT_Vec2;
    val.index = m_vectors.size();
    m_vectors.push_back(value);
    m_keys[key] = val;
}

void ConfStorage::addValue(const std::string &key, const Rect &value)
{
    Value val;
    val.type = VT_Rect;
    val.index = m_rects.size();
    m_rects.push_back(value);
    m_keys[key] = val;
}

void ConfStorage::addValue(const std::string &key, const std::string& value)
{
    Value val;
    val.type = VT_String;
    val.index = m_strings.size();
    m_strings.push_back(value);
    m_keys[key] = val;
}

void ConfStorage::addValue(const std::string &key, const std::vector<std::string> &value)
{
    Value val;
    val.type = VT_StringList;
    val.index = m_strings.size();
    val.num = value.size();
    m_strings.insert(m_strings.end(), value.begin(), value.end());
    m_keys[key] = val;
}

void ConfStorage::addValue(const std::string &key, const Color &value)
{
    Value val;
    val.type = VT_Color;
    val.index = m_colors.size();
    m_colors.push_back(value);
    m_keys[key] = val;
}

void ConfStorage::addValue(const Property *prop, const std::string &prefix)
{
    std::string name = prefix + prop->name();
    if(prop->isBool())
        this->addValue(name, prop->toBool());
    else if(prop->isFloat())
        this->addValue(name, prop->toFloat());
    else if(prop->isVec2())
        this->addValue(name, prop->toVec2());
    else if(prop->isString())
        this->addValue(name, prop->toString());
}

bool ConfStorage::isBool(const std::string &key) const
{
    return this->_type(key) == VT_Bool;
}

bool ConfStorage::toBool(const std::string &key) const
{
    if(m_keys.count(key))
    {
        Value v = m_keys.find(key)->second;
        if(v.type == VT_Bool)
            return m_bools[v.index];
    }
    return false;
}

bool ConfStorage::isFloat(const std::string &key) const
{
return this->_type(key) == VT_Float;
}

float ConfStorage::toFloat(const std::string &key) const
{
    if(m_keys.count(key))
    {
        Value v = m_keys.find(key)->second;
        if(v.type == VT_Float)
            return m_floats[v.index];
    }
    return 0.0f;
}

bool ConfStorage::isVec2(const std::string &key) const
{
    return this->_type(key) == VT_Vec2;
}

Vector2 ConfStorage::toVec2(const std::string &key) const
{
    if(m_keys.count(key))
    {
        Value v = m_keys.find(key)->second;
        if(v.type == VT_Vec2)
            return m_vectors[v.index];
    }
    return Vector2();
}

bool ConfStorage::isString(const std::string &key) const
{
    return this->_type(key) == VT_String;
}

std::string ConfStorage::toString(const std::string &key) const
{
    if(m_keys.count(key))
    {
        Value v = m_keys.find(key)->second;
        if(v.type == VT_String)
            return m_strings[v.index];
    }
    return std::string();
}

bool ConfStorage::isStringList(const std::string &key) const
{
    return this->_type(key) == VT_StringList;
}

std::vector<std::string> ConfStorage::toStringList(const std::string &key) const
{
    std::vector<std::string> list;
    if(m_keys.count(key))
    {
        Value v = m_keys.find(key)->second;
        if(v.type == VT_StringList)
            list.insert(list.end(), m_strings.begin() + v.index, m_strings.begin() + v.index + v.num);
    }
    return list;
}

bool ConfStorage::isColor(const std::string &key) const
{
    return this->_type(key) == VT_Color;
}

Color ConfStorage::toColor(const std::string &key) const
{
    if(m_keys.count(key))
    {
        Value v = m_keys.find(key)->second;
        if(v.type == VT_Color)
            return m_colors[v.index];
    }
    return Color();
}

void ConfStorage::toJSON(json::ObjectValue *parent) const
{
    for(const auto& item : m_keys)
    {
        json::Pair* pr = new json::Pair(item.first);
        parent->addChild(pr);

        Value v = item.second;

        switch(v.type)
        {
        case VT_Bool:
            pr->value = new json::BoolValue(m_bools[v.index]);
            break;
        case VT_Float:
            pr->value = new json::NumberValue((double)m_floats[v.index]);
            break;
        case VT_Vec2:
            {
                json::ArrayValue* arr = new json::ArrayValue();
                arr->appendChild(new json::NumberValue((double)m_vectors[v.index].x));
                arr->appendChild(new json::NumberValue((double)m_vectors[v.index].y));
                pr->value = arr;
            }
            break;
        case VT_String:
            pr->value = new json::StringValue(m_strings[v.index]);
            break;
        case VT_StringList:
            {
                json::ArrayValue* arr = new json::ArrayValue();
                for(int i = v.index; i < v.index + v.num; ++i)
                    arr->appendChild(new json::StringValue(m_strings[i]));
                pr->value = arr;
            }
            break;
        case VT_Color:
            {
                json::ArrayValue* arr = new json::ArrayValue();
                arr->appendChild(new json::NumberValue((double)m_colors[v.index].r));
                arr->appendChild(new json::NumberValue((double)m_colors[v.index].g));
                arr->appendChild(new json::NumberValue((double)m_colors[v.index].b));
                arr->appendChild(new json::NumberValue((double)m_colors[v.index].a));
            } break;
        case VT_Rect:
            {
                json::ArrayValue* arr = new json::ArrayValue();
                arr->appendChild(new json::NumberValue((double)m_rects[v.index].x));
                arr->appendChild(new json::NumberValue((double)m_rects[v.index].y));
                arr->appendChild(new json::NumberValue((double)m_rects[v.index].width));
                arr->appendChild(new json::NumberValue((double)m_rects[v.index].height));
            } break;
        default:
            pr->value = new json::NullValue();
            break;
        }
    }
}

int ConfStorage::_type(const std::string &key) const
{
    auto iter = m_keys.find(key);
    if(iter != m_keys.end())
        return iter->second.type;
    return VT_Null;
}
