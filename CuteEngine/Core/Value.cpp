#include "Value.hpp"
#include "Initializer.hpp"

using namespace cute;

/*
 * _cute::IValueImpl
 */

namespace
{
    enum ValueType
    {
        VT_Null,
        VT_Bool,
        VT_Float,
        VT_Vec2,
        VT_String,
        VT_StringList,
        VT_Rect,
        VT_Color
    };

    template<class T>
    ValueType GetValueType(){ return VT_Null; }

    template<>
    ValueType GetValueType<bool>(){ return VT_Bool; }

    template<>
    ValueType GetValueType<float>(){ return VT_Float; }

    template<>
    ValueType GetValueType<cute::Vector2>(){ return VT_Vec2; }

    template<>
    ValueType GetValueType<std::string>(){ return VT_String; }

    template<>
    ValueType GetValueType<std::vector<std::string> >(){ return VT_StringList; }

    template<>
    ValueType GetValueType<cute::Rect >(){ return VT_Rect; }

    template<>
    ValueType GetValueType<cute::Color>(){ return VT_Color; }
}

namespace _cute
{
    class IValueImpl
    {
    public:
        const ValueType type;

        IValueImpl(ValueType type)
            : type(type)
        {
        }

        virtual ~IValueImpl()
        {
        }

        virtual IValueImpl* copy() const = 0;
        virtual bool isEqual(const IValueImpl* rv) const = 0;
    };

    template<class T>
    class TValueImpl : public IValueImpl
    {
    public:
        T value;

        TValueImpl(const T& value)
            : IValueImpl(GetValueType<T>()), value(value)
        {
        }

        virtual IValueImpl* copy() const{ return new TValueImpl<T>(value); }
        virtual bool isEqual(const IValueImpl *rv) const
        {
            if(type == rv->type)
                return value == ((const TValueImpl<T>*)rv)->value;
            return false;
        }
    };

    template<>
    class TValueImpl<void> : public IValueImpl
    {
    public:
        TValueImpl()
            : IValueImpl(VT_Null)
        {
        }

        virtual IValueImpl* copy() const{ return new TValueImpl<void>(); }
        virtual bool isEqual(const IValueImpl *rv) const{ return rv->type == type; }
    };

    template<class T>
    void set(IValueImpl*& ptr, const T& value)
    {
        delete ptr;
        ptr = new TValueImpl<T>(value);
    }

    template<class T>
    T get(IValueImpl* ptr)
    {
        if(ptr->type == GetValueType<T>())
            return ((TValueImpl<T>*)ptr)->value;
        return cute::Initializer<T>();
    }
}

/*
 * cute::Value
 */

Value::Value()
    : m_impl(new _cute::TValueImpl<void>())
{
}

Value::Value(const Value &cp)
    : m_impl(cp.m_impl->copy())
{
}

Value::~Value()
{
    delete m_impl;
}

Value& cute::Value::operator =(const Value& rv)
{
    delete m_impl;
    m_impl = rv.m_impl->copy();
    return (*this);
}

bool Value::operator ==(const Value& rv) const
{
    return m_impl->isEqual(rv.m_impl);
}

void Value::setBool(bool v)
{
    _cute::set(m_impl, v);
}

bool Value::isBool() const
{
    return m_impl->type == VT_Bool;
}

bool Value::toBool() const
{
    return _cute::get<bool>(m_impl);
}

void Value::setFloat(float v)
{
    _cute::set(m_impl, v);
}

bool Value::isFloat() const
{
    return m_impl->type == VT_Float;
}

float Value::toFloat() const
{
    return _cute::get<float>(m_impl);
}

void Value::setVec2(const Vector2 &v)
{
    _cute::set(m_impl, v);
}

bool Value::isVec2() const
{
    return m_impl->type == VT_Vec2;
}

Vector2 Value::toVec2() const
{
    return _cute::get<Vector2>(m_impl);
}

void Value::setString(const std::string &v)
{
    _cute::set(m_impl, v);
}

bool Value::isString() const
{
    return m_impl->type == VT_String;
}

std::string Value::toString() const
{
    return _cute::get<std::string>(m_impl);
}

void Value::setStringList(const StringList &v)
{
    _cute::set(m_impl, v);
}

bool cute::Value::isStringList() const
{
    return m_impl->type == VT_StringList;
}

Value::StringList Value::toStringList() const
{
    return _cute::get<StringList>(m_impl);
}

void Value::setRect(const Rect &v)
{
    _cute::set(m_impl, v);
}

bool Value::isRect() const
{
    return m_impl->type == VT_Rect;
}

Rect Value::toRect() const
{
    return _cute::get<Rect>(m_impl);
}

void Value::setColor(const Color &v)
{
    _cute::set(m_impl, v);
}

bool Value::isColor() const
{
    return m_impl->type == VT_Color;
}

Color Value::toColor() const
{
    return _cute::get<Color>(m_impl);
}

#define FROM(Type, RealType) cute::Value cute::Value::From##Type(const RealType& v){ Value val; val.set##Type(v); return val; }

FROM(Bool, bool)
FROM(Float, float)
FROM(Vec2, cute::Vector2)
FROM(String, std::string)
FROM(StringList, StringList)
FROM(Rect, cute::Rect)
FROM(Color, cute::Color)
