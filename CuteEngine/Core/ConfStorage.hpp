#ifndef CONFSTORAGE_HPP
#define CONFSTORAGE_HPP

#include <CuteEngine/Utils/Vector2.hpp>
#include <CuteEngine/Core/Property.hpp>
#include <CuteEngine/Utils/JSON.hpp>
#include <string>
#include <vector>
#include <map>

namespace cute
{
    class ConfStorage
    {
    public:
        bool isEmtpy() const;
        void clear();

        void addValue(const std::string &key, bool value);
        void addValue(const std::string &key, float value);
        void addValue(const std::string &key, const cute::Vector2 &value);
        void addValue(const std::string &key, const cute::Rect &value);
        void addValue(const std::string &key, const std::string &value);
        void addValue(const std::string &key, const std::vector<std::string>& value);
        void addValue(const std::string &key, const cute::Color &value);
        void addValue(const Property* prop, const std::string& prefix = std::string());

        bool isBool(const std::string& key) const;
        bool toBool(const std::string& key) const;

        bool isFloat(const std::string& key) const;
        float toFloat(const std::string& keY) const;

        bool isVec2(const std::string& key) const;
        cute::Vector2 toVec2(const std::string& key) const;

        bool isString(const std::string& key) const;
        std::string toString(const std::string& key) const;

        bool isStringList(const std::string& key) const;
        std::vector<std::string> toStringList(const std::string& key) const;

        bool isColor(const std::string& key) const;
        cute::Color toColor(const std::string& key) const;

        void toJSON(json::ObjectValue* parent) const;

    private:
        struct Value{ int type, index, num = 1; };

        std::map<std::string, Value> m_keys;
        std::vector<bool> m_bools;
        std::vector<float> m_floats;
        std::vector<cute::Vector2> m_vectors;
        std::vector<cute::Rect> m_rects;
        std::vector<std::string> m_strings;
        std::vector<cute::Color> m_colors;

        int _type(const std::string& key) const;
    };
}

#endif // CONFSTORAGE_HPP
