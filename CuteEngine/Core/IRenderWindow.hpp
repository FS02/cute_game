#ifndef IRENDERWINDOW_HPP
#define IRENDERWINDOW_HPP

#include <CuteEngine/Utils/Vector2.hpp>
#include <CuteEngine/Utils/Color.h>
#include <string>

namespace cute
{
    /*!
     * \brief Data passed to IRenderWindow::create
     */
    class WindowDef
    {
    public:
        std::string title;  //!< Title of a window
        Vector2 size;      //!< Size of a window

        WindowDef(const std::string& title = std::string(), const Vector2& size = Vector2(800, 600))
            : title(title), size(size)
        {
        }
    };

    /*!
     * \brief Interface for OpenGL-enabled render windows
     * The purpose of this interface is to be able to switch between different window systems without problems.
     * At the moment my plan is to have two such systems: SDL2 (inside the game) and Qt5 (for the editor).
     */
    class IRenderWindow
    {
    public:
        virtual ~IRenderWindow(){} //!< Destructor. Will destroy the window if necessary.

        /*!
         * \brief Creates a window
         * Initializes OpenGL context. After calling that you will be able to do any plain old OpenGL rendering.
         * Might not work with OpenGL 3 and newer.
         * If window was already created it will be destroyed and re-created.
         * \param desc Data used during window creation.
         */
        virtual void create(const WindowDef& desc) = 0;
        /*!
         * \brief Destroys the window. Not the same as RenderWindow::close
         */
        virtual void destroy() = 0;

        //! Returns true is window is marked as "open". It isn't open when: window wasn't created, RenderWindow::close was called, or user closed the window ("close" button, alt+f4, etc.)
        virtual bool isOpen() const = 0;
        //! Marks window as closed. Does not destroy it.
        virtual void close() = 0;

        //! Returns size of the window (without borders or title bar)
        virtual Vector2 size() const = 0;

        //! Clear window
        virtual void clear(const Color& color = Color::Black) = 0;

        //! Updates window (read: events) and display results of rendering on the screen (swap buffers).
        virtual void display() = 0;

        //! Creates SDL2 render window, if engine was compiled with SDL2 support. For cenvenience, so that you don't have to include SDL2RenderWindow.hpp which includes SDL2 headers.
        static IRenderWindow* CreateSDL2Window();
    };
}

#endif // IRENDERWINDOW_HPP
