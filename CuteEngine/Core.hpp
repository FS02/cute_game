#ifndef CORE_HPP
#define CORE_HPP

#include <CuteEngine/Core/Camera.hpp>
#include <CuteEngine/Core/ResourceMgr.hpp>
#include <CuteEngine/Core/IRenderWindow.hpp>
#include <CuteEngine/Core/InputMgr.hpp>

#endif // CORE_HPP
