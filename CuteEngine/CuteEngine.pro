TARGET = CuteEngine

TEMPLATE = lib
CONFIG += staticlib
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -W -Wall -Wextra -pedantic

# for the engine, only include dirs need to be specified, libraries aren't linked into the engine library
unix{
    # settings for linux

    INCLUDEPATH += /usr/include/SDL2 /usr/local/include/SDL2 ../ Squirrel/include
}

win32{
	# settings for windows
	
    INCLUDEPATH += E:\SDL-Libraries\SDL2-2.0.0\i686-w64-mingw32\include\SDL2 E:\SDL-Libraries\SDL2_mixer-2.0.0\i686-w64-mingw32\include\SDL2 ../ Squirrel/include
}

SOURCES += \
    Utils/Path.cpp \
    Utils/JSON.cpp \
    World/Entity.cpp \
    World/GameWorld.cpp \
    Utils/Time.cpp \
    World/Component/SoundComponent.cpp \
    Core/Camera.cpp \
    Core/SDL2/SDL2RenderWindow.cpp \
    Core/InputMgr.cpp \
    Core/PhysicsDebugDraw.cpp \
    Core/IEditable.cpp \
    Core/Property.cpp \
    Core/ConfStorage.cpp \
    Core/Value.cpp \
    World/Component/ScriptsComponent.cpp \
    Squirrel/squirrel/sqvm.cpp \
    Squirrel/squirrel/sqtable.cpp \
    Squirrel/squirrel/sqstate.cpp \
    Squirrel/squirrel/sqobject.cpp \
    Squirrel/squirrel/sqmem.cpp \
    Squirrel/squirrel/sqlexer.cpp \
    Squirrel/squirrel/sqfuncstate.cpp \
    Squirrel/squirrel/sqdebug.cpp \
    Squirrel/squirrel/sqcompiler.cpp \
    Squirrel/squirrel/sqclass.cpp \
    Squirrel/squirrel/sqbaselib.cpp \
    Squirrel/squirrel/sqapi.cpp \
    Squirrel/sqimport/sqratimport.cpp \
    Squirrel/sqstdlib/sqstdsystem.cpp \
    Squirrel/sqstdlib/sqstdstring.cpp \
    Squirrel/sqstdlib/sqstdstream.cpp \
    Squirrel/sqstdlib/sqstdrex.cpp \
    Squirrel/sqstdlib/sqstdmath.cpp \
    Squirrel/sqstdlib/sqstdio.cpp \
    Squirrel/sqstdlib/sqstdblob.cpp \
    Squirrel/sqstdlib/sqstdaux.cpp \
    World/SquirrelBindFactory.cpp \
    Utils/Vector2.cpp \
    Utils/Vector3.cpp \
    Utils/Color.cpp \
    World/Component/Graphics2DComponent.cpp \
    World/Component/Physics2DComponent.cpp \
    World/Component/SpriteComponent.cpp \
    World/Component/Camera2DComponent.cpp \
    Core/SDL2/SDL2Texture.cpp \
    World/Component/Transform2DComponent.cpp \
    World/ComponentDefFactory.cpp \
    Utils/Matrix3x3.cpp \
    Utils/Rect.cpp \
    Utils/Triangulate.cpp

HEADERS += \
    Utils/Vector3.hpp \
    Utils/Vector2.hpp \
    Utils/Path.hpp \
    Utils/Matrix3x3.hpp \
    Utils/JSON.hpp \
    World/GameWorld.hpp \
    World/Entity.hpp \
    Utils/Time.hpp \
    Core/ResourceMgr.hpp \
    Core/Camera.hpp \
    Utils/Rect.hpp \
    Core/OpenGL.hpp \
    Utils/Utils.hpp \
    Utils.hpp \
    Core.hpp \
    World.hpp \
    World/Component/SoundComponent.hpp \
    Core/IRenderWindow.hpp \
    Core/SDL2/SDL2RenderWindow.hpp \
    World/Component/ScriptsComponent.hpp \
    Squirrel/include/squirrel.h \
    Squirrel/include/sqstdsystem.h \
    Squirrel/include/sqstdstring.h \
    Squirrel/include/sqstdmath.h \
    Squirrel/include/sqstdio.h \
    Squirrel/include/sqstdblob.h \
    Squirrel/include/sqstdaux.h \
    Squirrel/include/sqratimport.h \
    Squirrel/include/sqrat.h \
    Squirrel/include/sqmodule.h \
    Squirrel/include/sqrat/sqratVM.h \
    Squirrel/include/sqrat/sqratUtil.h \
    Squirrel/include/sqrat/sqratTypes.h \
    Squirrel/include/sqrat/sqratTable.h \
    Squirrel/include/sqrat/sqratScript.h \
    Squirrel/include/sqrat/sqratOverloadMethods.h \
    Squirrel/include/sqrat/sqratObject.h \
    Squirrel/include/sqrat/sqratMemberMethods.h \
    Squirrel/include/sqrat/sqratGlobalMethods.h \
    Squirrel/include/sqrat/sqratFunction.h \
    Squirrel/include/sqrat/sqratext_Ref.h \
    Squirrel/include/sqrat/sqratConst.h \
    Squirrel/include/sqrat/sqratClassType.h \
    Squirrel/include/sqrat/sqratClass.h \
    Squirrel/include/sqrat/sqratArray.h \
    Squirrel/include/sqrat/sqratAllocator.h \
    Squirrel/squirrel/sqvm.h \
    Squirrel/squirrel/squtils.h \
    Squirrel/squirrel/squserdata.h \
    Squirrel/squirrel/sqtable.h \
    Squirrel/squirrel/sqstring.h \
    Squirrel/squirrel/sqstate.h \
    Squirrel/squirrel/sqpcheader.h \
    Squirrel/squirrel/sqopcodes.h \
    Squirrel/squirrel/sqobject.h \
    Squirrel/squirrel/sqlexer.h \
    Squirrel/squirrel/sqfuncstate.h \
    Squirrel/squirrel/sqfuncproto.h \
    Squirrel/squirrel/sqcompiler.h \
    Squirrel/squirrel/sqclosure.h \
    Squirrel/squirrel/sqclass.h \
    Squirrel/squirrel/sqarray.h \
    Squirrel/sqstdlib/sqstdstream.h \
    Squirrel/sqstdlib/sqstdblobimpl.h \
    Core/InputMgr.hpp \
    Core/PhysicsDebugDraw.hpp \
    Core/IEditable.hpp \
    Core/Initializer.hpp \
    Core/Property.hpp \
    Core/PropertyMacros.hpp \
    Core/ConfStorage.hpp \
    Core/Value.hpp \
    World/SquirrelBindFactory.hpp \
    Utils/Color.h \
    World/Component/Graphics2DComponent.hpp \
    World/Component/Physics2DComponent.hpp \
    World/Component/SpriteComponent.hpp \
    World/Component/AnimatedSpriteComponent.hpp \
    World/Component/Camera2DComponent.hpp \
    Core/SDL2/SDL2Texture.hpp \
    Utils/Vertex.hpp \
    World/Component/Transform2DComponent.hpp \
    World/ComponentDefFactory.hpp \
    Utils/Triangulate.hpp
