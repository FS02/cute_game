#ifndef WORLD_HPP
#define WORLD_HPP

#include <CuteEngine/World/GameWorld.hpp>
#include <CuteEngine/World/Entity.hpp>
#include <CuteEngine/World/ComponentDefFactory.hpp>

#include <CuteEngine/World/Component/Transform2DComponent.hpp>
#include <CuteEngine/World/Component/Graphics2DComponent.hpp>
#include <CuteEngine/World/Component/ScriptsComponent.hpp>
#include <CuteEngine/World/Component/SoundComponent.hpp>
#include <CuteEngine/World/Component/Physics2DComponent.hpp>
#include <CuteEngine/World/Component/Camera2DComponent.hpp>

#endif // WORLD_HPP
