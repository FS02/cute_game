#include <CuteEngine/World/SquirrelBindFactory.hpp>

using namespace cute;

std::queue<SquirrelBindFactory::BindFunc> SquirrelBindFactory::m_bindFuncQueue = std::queue<SquirrelBindFactory::BindFunc>();

SquirrelBindFactory::SquirrelBindFactory()
{
}

void SquirrelBindFactory::bind(const BindFunc& bindFunc)
{
    m_bindFuncQueue.push(bindFunc);
}

void SquirrelBindFactory::processQueue(HSQUIRRELVM vm)
{
    while (!m_bindFuncQueue.empty())
    {
        m_bindFuncQueue.front()(vm);
        m_bindFuncQueue.pop();
    }
}
