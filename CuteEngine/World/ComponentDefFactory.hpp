#ifndef COMPONENTDEFFACTORY_HPP
#define COMPONENTDEFFACTORY_HPP

#include "Entity.hpp"
#include "../Core/ResourceMgr.hpp"

namespace cute
{
    class IComponentDefCreator
    {
    public:
        virtual ~IComponentDefCreator();

        virtual const std::string& familyName() const = 0;
        virtual const std::string& typeName() const = 0;

        virtual IComponentDef* create() = 0;
    };

    class StdComponentDefCreator : public IComponentDefCreator
    {
    public:
        typedef std::function<IComponentDef*(const std::string& type)> Func;

        StdComponentDefCreator(const std::string& family, const std::string& type, const Func& func);

        virtual const std::string& familyName() const;
        virtual const std::string& typeName() const;

        virtual IComponentDef* create();

    private:
        std::string m_family, m_type;
        Func m_func;
    };

    /*!
     * \brief Singleton used for creating IComponentDef instances.
     * Components should be auto-registered at programs startup.
     * It can be done using global variables; see LocationComponent.cpp.
     *
     * It is an auto-singleton, i.e. it's instance will be created when Instance() is called for the first time.
     */
    class ComponentDefFactory
    {
    public:
        ComponentDefFactory(const ComponentDefFactory&) = delete;
        ~ComponentDefFactory();

        //! Adds component type.
        void addType(IComponentDefCreator* creator);
        //! Creates component description by calling appropriate creator. May return nullptr.
        IComponentDef* createDef(const std::string& type) const;

        //! Returns list of component desc. creators. Can be used to query data about specific component type.
        std::list<const IComponentDefCreator*> listCreators() const;

        //! Acces singleton instance.
        static ComponentDefFactory& Instance();

    private:
        std::map<std::string, IComponentDefCreator*> m_creators;

        ComponentDefFactory();
    };
}

#endif // COMPONENTDEFFACTORY_HPP
