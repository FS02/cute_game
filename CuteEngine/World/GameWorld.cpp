#include "GameWorld.hpp"
#include <CuteEngine/World/ComponentDefFactory.hpp>
#include <CuteEngine/World/Component/Physics2DComponent.hpp>
#include <CuteEngine/World/Component/Camera2DComponent.hpp>
#include <CuteEngine/World/Component/Graphics2DComponent.hpp>
#include <fstream>
#include <algorithm>

using namespace cute;

/*
 * IComponentMgr
 */

IComponentMgr::~IComponentMgr()
{
}

/*
 * GameWorld
 */

std::string GameWorld::path = "Data/Scenes/";

GameWorld::GameWorld(bool editor_mode)
    : m_entityDescMgr(&EntityDef::Create), m_editorMode(editor_mode)
{
}

GameWorld::~GameWorld()
{
    for(auto iter = m_entities.begin(); iter != m_entities.end(); ++iter)
        delete iter->second;

    for(unsigned int i = 0; i < m_managers.size(); ++i)
        m_managers[i]->deinitialize();

    for(unsigned int i = 0; i < m_managers.size(); ++i)
        delete m_managers[i];
}

void GameWorld::addComponentMgr(IComponentMgr *mgr)
{
    // avoid adding the same manager several times
    for(unsigned int i = 0; i < m_managers.size(); ++i)
        if(m_managers[i] == mgr)
            return;

    m_managers.push_back(mgr);
}

IComponentMgr* GameWorld::componentMgr(const std::string &family)
{
    for(unsigned int i = 0; i < m_managers.size(); ++i)
        if(m_managers[i]->familyName() == family)
            return m_managers[i];

    return nullptr;
}

void GameWorld::initialize()
{
    for(unsigned int i = 0; i < m_managers.size(); ++i)
        m_managers[i]->initialize(this);
}

void GameWorld::deinitialize()
{
    for(unsigned int i = 0; i < m_managers.size(); ++i)
        m_managers[i]->deinitialize();
}

const std::string& GameWorld::currentSceneName() const
{
    return m_currentSceneName;
}

bool GameWorld::loadScene(const std::string &fileName)
{
    if (!m_currentSceneName.empty())
    {
        this->deinitialize();
        this->initialize();
    }

    json::Document doc;

    std::ifstream file(path + fileName);
    json::StdTokenStream tokenStr(file);
    json::StdErrorCallback errorCb(std::clog, fileName);
    if(!doc.load(&tokenStr, &errorCb))
        return false;

    json::ValueHandle hMetadata = doc.root()->child("Metadata")->value;
    if (!hMetadata.childPair("Name").value().isNull())
        m_currentSceneName = hMetadata.childPair("Name").value().string();

    auto phys = (Physics2DCompMgr*)this->componentMgr("Physics2D");
    phys->enableDebugDraw(hMetadata.childPair("Physics2D.Debug").value().boolean(false));

    if (!hMetadata.childPair("Physics2D.Gravity").value().isNull())
    {
        b2Vec2 gravity;
        gravity.x = hMetadata.childPair("Physics2D.Gravity").value().childValue(0).number();
        gravity.y = hMetadata.childPair("Physics2D.Gravity").value().childValue(1).number();
        phys->world()->SetGravity(gravity);
    }

    auto graph = (Graphics2DComponentMgr*)this->componentMgr("Graphics2D");
    graph->m_isWireframeMode = hMetadata.childPair("Graphics2D.Wireframe").value().boolean(false);

    json::ValueHandle hEntities = doc.root()->child("Entities")->value;
    for(unsigned int i = 0; i < hEntities.numChildren(); ++i)
    {
        json::ValueHandle hEnt = hEntities.childValue(i);

        std::string name = hEnt.childPair("Name").value().string();
        std::string descFile = hEnt.childPair("Description").value().string();

        Entity* ent = this->createEntity(name, descFile);
        for(unsigned int j = 0; j < hEnt.numChildren(); ++j)
        {
            json::PairHandle hProp = hEnt.childPair(j);
            if(hProp.name() == "Name" || hProp.name() == "Description")
                continue;

            size_t sep = hProp.name().find('.');
            if(sep == std::string::npos)
            {
                std::clog << "[GameWorld] World " << fileName << ", entity " << ent->name() << ": redundant property " << hProp.name() << "\n";
                continue;
            }
            else
            {
                std::string compType = hProp.name().substr(0, sep);
                std::string propName = hProp.name().substr(sep+1);

                IComponent* comp = ent->componentByType(compType);
                if(!comp)
                {
                    std::clog << "[GameWorld] World " << fileName << ", entity " << ent->name() << ": component " << compType << " does not exist!\n";
                    continue;
                }
                else
                    comp->setProperty(propName, hProp.value());
            }
        }
    }

    return true;
}

bool GameWorld::writeScene(const std::string &fileName)
{
    json::Document doc;
    doc.createEmpty();

    json::ArrayValue* entArr = new json::ArrayValue();
    doc.root()->addChild(new json::Pair("Entities", entArr));

    std::function<bool(cute::Entity*)> func = [entArr](cute::Entity* ent)->bool
    {
        entArr->appendChild(ent->toJSON());
        return true;
    };
    this->forEachEntity(func);

    std::ofstream file(path + fileName);
    if(!file)
        return false;
    json::StdOutputStream stream(file);
    doc.save(&stream);

    return true;
}

bool GameWorld::saveGame(const std::string &fileName)
{
    ((void)fileName);
    std::clog << "GameWorld::saveGame has not yet been implemented!\n";
    return false;
}

bool GameWorld::loadGame(const std::string &fileName)
{
    ((void)fileName);
    std::clog << "GameWorld::loadGame has not yet been implemented!\n";
    return false;
}

void GameWorld::clear()
{
    for(auto& pr : m_entities)
        pr.second->markAsRedundant();
    m_entities.clear();

    this->update(0);
    for(Entity* e : m_forDeletion)
        delete e;
    m_forDeletion.clear();
}

EntityDescMgr& GameWorld::entityDescMgr()
{
    return m_entityDescMgr;
}

Entity* GameWorld::createEntity(const std::string &name, const EntityDef *desc)
{
    // if there already is an entity with this name, abort
    auto iter = m_entities.find(name);
    if(iter != m_entities.end())
        return nullptr;

    // create and add new entity
    Entity* ent = new Entity(name, desc);
    for(int i = 0; ; ++i)
    {
        const IComponentDef* cd = desc->component(i);
        if(!cd)
            break;

        IComponentMgr* mgr = this->componentMgr(cd->familyName);
        if(mgr)
        {
            IComponent* comp = mgr->createComponent(cd);
            ent->addComponent(comp);
        }
    }

    ent->initialize(this);
    m_entities[name] = ent;
    for(IGameWorldMonitor* mon : m_monitors)
        mon->onEntityAdded(ent);
    return ent;
}

Entity* GameWorld::createEntity(const std::string &entityName, const std::string &descFileName)
{
    EntityDef* ed = m_entityDescMgr.resource(descFileName);
    return this->createEntity(entityName, ed);
}

Entity* GameWorld::entity(const std::string &name)
{
    auto iter = m_entities.find(name);
    if(iter == m_entities.end())
        return nullptr;
    return iter->second;
}

void GameWorld::addMonitor(IGameWorldMonitor *monitor)
{
    if(!monitor)
        return;
    for(IGameWorldMonitor* m : m_monitors)
        if(m == monitor)
            return;
    m_monitors.push_back(monitor);
}

void GameWorld::removeMonitor(IGameWorldMonitor *monitor)
{
    m_monitors.erase(std::find(m_monitors.begin(), m_monitors.end(), monitor));
}

void GameWorld::update(double deltaTime)
{
    for(unsigned int i = 0; i < m_managers.size(); ++i)
        m_managers[i]->update(deltaTime);

    for(auto iter = m_forDeletion.begin(); iter != m_forDeletion.end(); ++iter)
        delete (*iter);
    m_forDeletion.clear();
}

int GameWorld::forEachEntity(const std::function<bool (Entity *)> &func)
{
    if(!func)
        return 0;

    int i = 0;

    for(auto& pr : m_entities)
    {
        ++i;

        if(!func(pr.second))
            return i;
    }

    return i;
}

void GameWorld::_markForDeletion(Entity *ent)
{
    ent->m_isRedundant = true;
    for(IGameWorldMonitor* mon : m_monitors)
        mon->onEntityRemoved(ent);
    auto iter = m_entities.find(ent->name());
    m_entities.erase(iter);
    m_forDeletion.push_back(ent);
}

bool GameWorld::editorMode() const
{
    return m_editorMode;
}

void cute::GameWorld::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<GameWorld, Sqrat::NoConstructor> _GameWorld;
    _GameWorld.Func("currentSceneName", &cute::GameWorld::currentSceneName);
    _GameWorld.Func("loadScene", &cute::GameWorld::loadScene);
    _GameWorld.Func("writeScene", &cute::GameWorld::writeScene);
    _GameWorld.Func("saveGame", &cute::GameWorld::saveGame);
    _GameWorld.Func("loadGame", &cute::GameWorld::loadGame);
    _GameWorld.Func("clear", &cute::GameWorld::clear);
    _GameWorld.Func<Entity* (GameWorld::*)(const std::string&, const std::string&)>("createEntity", &cute::GameWorld::createEntity);
    _GameWorld.Func("entity", &cute::GameWorld::entity);
    Sqrat::RootTable(vm).Bind("GameWorld", _GameWorld);
}
