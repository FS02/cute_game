#ifndef SQUIRRELBINDFACTORY_HPP
#define SQUIRRELBINDFACTORY_HPP

#include <sqrat.h>
#include <functional>
#include <queue>

namespace cute
{
    class SquirrelBindFactory
    {
    public:
        typedef std::function<void(HSQUIRRELVM)> BindFunc;

        SquirrelBindFactory();

        static void bind(const BindFunc& bindFunc);

        void processQueue(HSQUIRRELVM vm);

    private:
        static std::queue<BindFunc> m_bindFuncQueue;
    };
}

#endif // SQUIRRELBINDFACTORY_HPP
