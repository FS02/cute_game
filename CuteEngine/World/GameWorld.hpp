#ifndef GAMEWORLD_HPP
#define GAMEWORLD_HPP

#include <CuteEngine/World/Entity.hpp>
#include <map>
#include <unordered_map>
#include <list>
#include <functional>

namespace cute
{
    class GameWorld;

    /*!
     * \brief IComponentMgr is interface for component managers.
     *
     * Component managers handle the following stuff:
     * - creating components from IComponentDesc objects
     * - updating these components (every manager may have different update order to optimize the process)
     * - removing components when they are redundant (cannot simply "delete" them, since they usually will be referenced by their manager.
     */
    class IComponentMgr
    {
    public:
        virtual ~IComponentMgr(); //!< Destructor.

        /*!
         * \brief Family of components created by this manager.
         * Components are identified by family name (per manager type) and type name (per component type).
         * For example family of a sprite component could be "Graphics" and type "StaticSprite".
         */
        virtual const std::string& familyName() const = 0;

        /*!
         * \brief Initializes a manager.
         * Some managers could depend on other managers. This method is the place to obtain pointers to these managers.
         * \param world GameWorld which owns this manager.
         */
        virtual void initialize(GameWorld* world) = 0;
        /*!
         * \brief Deinitializes a manager.
         * Imagine that we have mgr. A and mgr. B that depends on A.
         * In GameWorld's dtor we delete A and then B. If B calls a method of A, app will crash.
         * To avoid this we first call deinitialize for each manager (here B would call A's members) and then delete that mgr.
         */
        virtual void deinitialize() = 0;

        //! \breif Creates a component from a description. Used internally by GameWorld::createEntity
        virtual IComponent* createComponent(const IComponentDef* desc) = 0;

        virtual void update(double deltaTime) = 0;
    };

    /*!
     * \brief This class can be used to monitor changes in GameWorld.
     */
    class IGameWorldMonitor
    {
    public:
        virtual ~IGameWorldMonitor(){}

        //! Called whenever an entity is added.
        virtual void onEntityAdded(const Entity* ent) = 0;
        //! Called whenever an entity is marked as redundant.
        virtual void onEntityRemoved(const Entity* ent) = 0;
    };

    /*!
     * \brief Top level class that contains all game logic.
     * Contains component managers, which implement specific subsystems of the engine (like graphics, sound, AI, etc.).
     * Contains entities (actors, game objects, however you call them).
     */
    class GameWorld
    {
    public:
        static std::string path;

        GameWorld(bool editor_mode = false);
        GameWorld(const GameWorld&) = delete;
        ~GameWorld();

        //! Adds a component manager. Call before GameWorld::initialize.
        void addComponentMgr(IComponentMgr* mgr);
        IComponentMgr* componentMgr(const std::string& family);

        //! Initializes the world. Has to be called after all managers have been added and before any entities have.
        void initialize();

        //! Deinitializes the world
        void deinitialize();

        const std::string& currentSceneName() const;

        //! Loads entities from a JSON file.
        bool loadScene(const std::string& fileName);
        //! Writes WHOLE world to a JSON file.
        bool writeScene(const std::string& fileName);
        /*!
         * \brief Saves game current game state.
         * Should save as little data as possible.
         * The world is first loaded from JSON file using loadFromJSON and then state is restored using this method.
         * \param fileName File to which write state.
         * \return True on success, false on failure.
         */
        bool saveGame(const std::string& fileName);
        /*!
         * \brief Restores game.
         * Saves don't contain as little data as possible. Its isn't possible to use this without call to loadFromJSON.
         * \param fileName File containing game state.
         * \return True on success, false on failure.
         */
        bool loadGame(const std::string& fileName);
        //! Deletes all entities.
        void clear();

        //! Returns manager of EntityDescResource objects.
        EntityDescMgr& entityDescMgr();

        //! Creates an entity from a description.
        Entity* createEntity(const std::string& name, const EntityDef* desc);
        //! Creates an entity from a description loaded from a JSON file.
        Entity* createEntity(const std::string& entityName, const std::string &descFileName);
        //! Finds an entity with specified name.
        Entity* entity(const std::string& name);

        //! Adds a monitor. Monitors aren't managed gy GameWorld.
        void addMonitor(IGameWorldMonitor* monitor);
        //! Removed a monitor.
        void removeMonitor(IGameWorldMonitor* monitor);

        /*!
         * \brief Updates whole world.
         * I think it should do everything you can think of: from game logic to rendering to UI.
         * \param deltaTime Duration (in seconds) of the last frame.
         */
        void update(double deltaTime);

        /*!
         * \brief Iterates over entities and calls func fro them.
         * \param func If func returns false, method will immediately return.
         * \return Number of visited entities.
         */
        int forEachEntity(const std::function<bool(cute::Entity*)>& func);

        //! Using Entity::markAsRedundant is more readable and does the same thing.
        void _markForDeletion(Entity* ent);

        bool editorMode() const;

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        EntityDescMgr m_entityDescMgr;
        std::vector<IComponentMgr*> m_managers;
        std::unordered_map<std::string, Entity*> m_entities;
        std::list<Entity*> m_forDeletion;
        std::vector<IGameWorldMonitor*> m_monitors;
        std::string m_currentSceneName;
        bool m_editorMode;
    };
}

#endif // GAMEWORLD_HPP
