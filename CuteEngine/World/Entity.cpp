#include "Entity.hpp"
#include "GameWorld.hpp"
#include <CuteEngine/World/ComponentDefFactory.hpp>
#include <fstream>
#include <CuteEngine/World/Component/Graphics2DComponent.hpp>
#include <CuteEngine/World/Component/Transform2DComponent.hpp>
#include <CuteEngine/World/Component/Physics2DComponent.hpp>
#include <CuteEngine/World/Component/ScriptsComponent.hpp>
#include <CuteEngine/World/Component/SoundComponent.hpp>

using namespace cute;

/*
 * IComponentDesc
 */

IComponentDef::IComponentDef(const std::string &familyName, const std::string &typeName)
    : familyName(familyName), typeName(typeName)
{
}

IComponentDef::~IComponentDef()
{
}

json::ObjectValue* IComponentDef::toJSON() const
{
    json::ObjectValue* obj = this->IEditable::toJSON();
    obj->addChild(new json::Pair("Type", new json::StringValue(typeName)));
    return obj;
}

/*
 * IComponent
 */

IComponent::~IComponent()
{
}

/*
 * EntityDesc
 */

std::string EntityDef::path = "Data/Entities/";

EntityDef::EntityDef(const std::string &fileName)
    : IResource(fileName)
{
}

EntityDef::~EntityDef()
{
    for(unsigned int i = 0; i < m_components.size(); ++i)
        delete m_components[i];
}

bool EntityDef::load()
{
    json::Document doc;

    std::ifstream file(path + this->fileName());
    json::StdTokenStream tokenStr(file);
    json::StdErrorCallback errorCb(std::clog, this->fileName());

    if(!doc.load(&tokenStr, &errorCb))
        return false;

    json::ValueHandle hRoot = doc.root();
    json::ValueHandle hComps = hRoot.childPair("Components").value();
    for(unsigned int i = 0; i < hComps.numChildren(); ++i)
    {
        json::ValueHandle hc = hComps.childValue(i);
        std::string compType = hc.childPair("Type").value().string();
        IComponentDef* comp = ComponentDefFactory::Instance().createDef(compType);
        if(!comp)
            std::clog << "Error while loading entity description " << this->fileName() << ": component type " << compType << " has not been registered!\n";
        else
        {
            comp->configure(hc);
            this->_addComponent(comp);
        }
    }

    return true;
}

bool EntityDef::write()
{
    json::Document doc;
    doc.createEmpty();

    json::ArrayValue* compArr = new json::ArrayValue();
    doc.root()->addChild(new json::Pair("Components", compArr));

    for(size_t i = 0; i < m_components.size(); ++i)
        compArr->appendChild(m_components[i]->toJSON());

    std::ofstream file(path + this->fileName());
    if(!file)
        return false;
    json::StdOutputStream stream(file);
    doc.save(&stream, json::FormattingHints::oneLineHints());
    return true;
}

void EntityDef::addComponent(IComponentDef *desc)
{
    for(auto ptr : m_components)
        if(ptr == desc)
            return;
    m_components.push_back(desc);
}

IComponentDef* EntityDef::component(int index)
{
    if(index < 0 || index >= (int)m_components.size())
        return nullptr;

    return m_components[index];
}

const IComponentDef* EntityDef::component(int index) const
{
    if(index < 0 || index >= (int)m_components.size())
        return nullptr;

    return m_components[index];
}

EntityDef* EntityDef::Create(const std::string& fileName)
{
    return new EntityDef(fileName);
}

void EntityDef::_addComponent(IComponentDef *desc)
{
    for(IComponentDef* cd : m_components)
        if(cd == desc)
            return;

    m_components.push_back(desc);
}

/*
 * Entity
 */

Entity::Entity(const std::string& name, const EntityDef* desc)
    : m_name(name), m_desc(desc), m_isInitialized(false), m_world(nullptr), m_isRedundant(false)
{
}

Entity::~Entity()
{
    for(unsigned int i = 0; i < m_components.size(); ++i)
        m_components[i]->deinitialize();
}

const std::string& Entity::name() const
{
    return m_name;
}

const EntityDef* Entity::description() const
{
    return m_desc;
}

void Entity::addComponent(IComponent *comp)
{
    // avoid adding the same component two times
    for(unsigned int i = 0; i < m_components.size(); ++i)
        if(m_components[i] == comp)
            return;

    m_components.push_back(comp);
}

void Entity::initialize(GameWorld* world)
{
    if(m_isInitialized)
        return;

    m_world = world;

    for(unsigned int i = 0; i < m_components.size(); ++i)
        m_components[i]->initialize(this);

    m_isInitialized = true;
}

json::ObjectValue* Entity::toJSON() const
{
    json::ObjectValue* obj = new json::ObjectValue();

    obj->addChild(new json::Pair("Name", new json::StringValue(m_name)));
    obj->addChild(new json::Pair("Description", new json::StringValue(m_desc->fileName())));

    ConfStorage conf;
    this->saveState(conf);
    conf.toJSON(obj);

    return obj;
}

void Entity::saveState(ConfStorage &storage) const
{
    for(IComponent* comp : m_components)
        comp->saveState(storage);
}

void Entity::loadState(const ConfStorage &storage)
{
    for(IComponent* comp : m_components)
        comp->loadState(storage);
}

GameWorld* Entity::world()
{
    return m_world;
}

IComponent* Entity::component(int index)
{
    if(index < 0 || index >= (int)m_components.size())
        return nullptr;

    return m_components[index];
}

IComponent* Entity::componentByFamily(const std::string &familyName, int index)
{
    int count = 0;

    for(unsigned int i = 0; i < m_components.size(); ++i)
    {
        if(m_components[i]->familyName() == familyName)
        {
            if(count == index)
                return m_components[i];
            ++count;
        }
    }

    return nullptr;
}

IComponent* Entity::componentByType(const std::string &typeName, int index)
{
    int count = 0;

    for(unsigned int i = 0; i < m_components.size(); ++i)
    {
        if(m_components[i]->typeName() == typeName)
        {
            if(count == index)
                return m_components[i];
            ++count;
        }
    }

    return nullptr;
}

void Entity::markAsRedundant()
{
    m_world->_markForDeletion(this);
}

bool Entity::isRedundant() const
{
    return m_isRedundant;
}

void Entity::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Entity, Sqrat::NoConstructor> _Entity;
    _Entity.SquirrelFunc("component", &Entity::Sq_component);
    _Entity.SquirrelFunc("componentByFamily", &Entity::Sq_componentByFamily);
    _Entity.SquirrelFunc("componentByType", &Entity::Sq_componentByType);
    _Entity.Func("name", &Entity::name);
    _Entity.Func("world", &Entity::world);
    _Entity.Func("markAsRedundant", &Entity::markAsRedundant);
    _Entity.Func("isRedundant", &Entity::isRedundant);
    Sqrat::RootTable(vm).Bind("Entity", _Entity);
}

SQInteger Entity::Sq_component(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // Entiy and component index
        Sqrat::Var<Entity*> _entity(vm, 1);
        Sqrat::Var<int> _index(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            IComponent* comp = _entity.value->component(_index.value);
            return Sq_pushComponent(vm, comp);
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger Entity::Sq_componentByFamily(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 3) { // one argument
        // Entiy and component index
        Sqrat::Var<Entity*> _entity(vm, 1);
        Sqrat::Var<const std::string&> _family(vm, 2);
        Sqrat::Var<int> _index(vm,3);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            IComponent* comp = _entity.value->componentByFamily(_family.value, _index.value);
            return Sq_pushComponent(vm, comp);
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger Entity::Sq_componentByType(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 3) { // one argument
        // Entiy and component index
        Sqrat::Var<Entity*> _entity(vm, 1);
        Sqrat::Var<const std::string&> _type(vm, 2);
        Sqrat::Var<int> _index(vm,3);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            IComponent* comp = _entity.value->componentByType(_type.value, _index.value);
            return Sq_pushComponent(vm, comp);
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger Entity::Sq_pushComponent(HSQUIRRELVM vm, IComponent *comp)
{
    Graphics2D* graph;
    Transform2D* loc;
    Physics2D* phy;
    Scripts* scr;
    Sound* snd;
    Camera2D* cam;

    // Some manual work
    if ((graph = dynamic_cast<Graphics2D*>(comp)) != nullptr)
        Sqrat::PushVar(vm, graph);
    else if ((loc = dynamic_cast<Transform2D*>(comp)) != nullptr)
        Sqrat::PushVar(vm, loc);
    else if ((phy = dynamic_cast<Physics2D*>(comp)) != nullptr)
        Sqrat::PushVar(vm, phy);
    else if ((scr = dynamic_cast<Scripts*>(comp)) != nullptr)
        Sqrat::PushVar(vm, scr);
    else if ((snd = dynamic_cast<Sound*>(comp)) != nullptr)
        Sqrat::PushVar(vm, snd);
    else if ((cam = dynamic_cast<Camera2D*>(comp)) != nullptr)
        Sqrat::PushVar(vm, cam);
    else
        Sqrat::PushVar(vm, 0);
    return 1;
}
