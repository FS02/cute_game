#include <CuteEngine/World/ComponentDefFactory.hpp>
#include "../Utils/JSON.hpp"
#include <fstream>
#include <iostream>

using namespace cute;

/*
 * IComponentDescCreator
 */

IComponentDefCreator::~IComponentDefCreator()
{
}

/*
 * StdComponentDescCreator
 */

StdComponentDefCreator::StdComponentDefCreator(const std::string &family, const std::string &type, const Func &func)
    : m_family(family), m_type(type), m_func(func)
{
}

const std::string& StdComponentDefCreator::familyName() const
{
    return m_family;
}

const std::string& StdComponentDefCreator::typeName() const
{
    return m_type;
}

IComponentDef* StdComponentDefCreator::create()
{
    return m_func(m_type);
}

/*
 * ComponentDescFactory
 */

ComponentDefFactory::~ComponentDefFactory()
{
    for(auto pr : m_creators)
        delete pr.second;
}

void ComponentDefFactory::addType(IComponentDefCreator* creator)
{
    if(creator->typeName().find('.') != std::string::npos)
    {
        std::clog << "Cannot register component type " << creator->typeName() << "! Component type names cannot have dots!\n";
        delete creator;
        return;
    }

    auto iter = m_creators.find(creator->typeName());
    if(iter == m_creators.end())
        m_creators[creator->typeName()] = creator;
    else
    {
        delete iter->second;
        iter->second = creator;
    }
}

IComponentDef* ComponentDefFactory::createDef(const std::string &type) const
{
    auto iter = m_creators.find(type);
    if(iter != m_creators.end())
        return iter->second->create();
    return nullptr;
}

std::list<const IComponentDefCreator*> ComponentDefFactory::listCreators() const
{
    std::list<const IComponentDefCreator*> list;
    for(auto pr : m_creators)
        list.push_back(pr.second);
    return list;
}

ComponentDefFactory& ComponentDefFactory::Instance()
{
    static ComponentDefFactory instance;
    return instance;
}

ComponentDefFactory::ComponentDefFactory()
{
}
