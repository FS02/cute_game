#include <CuteEngine/World/Component/ScriptsComponent.hpp>
#include <CuteEngine/World/ComponentDefFactory.hpp>
#include <CuteEngine/World/GameWorld.hpp>
#include <stdarg.h>
#include <fstream>
#include <algorithm>
#include <CuteEngine/World/SquirrelBindFactory.hpp>
#include <sqstdmath.h>

using namespace cute;

// See LocationComponent.cpp
namespace
{
    struct RegisterScriptsComp
    {
        RegisterScriptsComp()
        {
            std::string family = "Scripts";
            std::string type = "Scripts";
            IComponentDefCreator* creator = new StdComponentDefCreator(family, type, &CreateDef);
            ComponentDefFactory::Instance().addType(creator);
        }

        static ScriptsComponentDesc* CreateDef(const std::string&)
        {
            return new ScriptsComponentDesc();
        }
    };

    RegisterScriptsComp registerScriptsComp;
}

/*
 * Squirrel console print function
 */
static void squirrel_print_function(HSQUIRRELVM sqvm, const SQChar *format, ...)
{
    (void)sqvm;
    va_list args;
    va_start(args, format);
    vfprintf(stdout, format, args);
    va_end(args);
}

/*
 * ScriptsComponentDesc
 */

cuteBEGIN_PROPERTY_IMPL(ScriptsComponentDesc)
    cutePROPERTY_IMPL(std::vector<std::string>, Modules, true)
cuteEND_PROPERTY_IMPL

ScriptsComponentDesc::ScriptsComponentDesc()
    : IComponentDef(ScriptsComponentMgr::FamilyName, Scripts::TypeName)
{
    cutePREPARE_PROPERTIES();
}

void ScriptsComponentDesc::configure(const json::ValueHandle &data)
{
    this->setProperty("Modules", data.childPair("Modules").value());
}

/*
 * ScrpitsComponent
 */

const std::string Scripts::TypeName = "Scripts";
const std::string Scripts::Path = "Data/Scripts/";

Scripts::Scripts(ScriptsComponentMgr* mgr, const ScriptsComponentDesc* desc)
    : m_mgr(mgr), m_owner(nullptr), m_desc(desc)
{
}

Scripts::~Scripts()
{
}

const std::string& Scripts::familyName() const
{
    return ScriptsComponentMgr::FamilyName;
}

const std::string& Scripts::typeName() const
{
    return TypeName;
}

void Scripts::initialize(Entity* ent)
{
    m_owner = ent;

    if (this->m_owner->world()->editorMode()) return;

    bindSqInstance(this);
    std::vector<std::string> modules = m_desc->getModules();
    for (auto it = modules.begin(); it != modules.end(); ++it)
        loadScripts((*it));

    for (auto it = m_SqObj.begin(); it !=m_SqObj.end(); ++it)
    {
        Sqrat::Object obj = *it;
        if(!obj.IsNull())
        {
            Sqrat::Function _start(obj, "start");
            if(!_start.IsNull())
            {
                _start.Execute();
            }
        }
    }
}

void Scripts::deinitialize()
{
    if (this->m_owner->world()->editorMode()) return;

    for (auto it = m_SqObj.begin(); it !=m_SqObj.end(); ++it)
    {
        Sqrat::Object obj = *it;
        if(!obj.IsNull())
        {
            Sqrat::Function _end(obj, "end");
            if(!_end.IsNull())
            {
                _end.Execute();
            }
        }
    }
    m_SqObj.clear();
    m_mgr->markForDeletion(this);
}

void Scripts::loadScripts(const std::string &file)
{
    Sqrat::Script script;
    try {
        std::string slot("_" + file);
        std::string scriptstr("::import(\""+Path+file+"\"); "
                              + slot + " <- " + file + "(); ");
        script.CompileString(_SC(scriptstr.c_str()));
        script.Run();
        m_SqObj.push_back(Sqrat::RootTable().GetSlot(slot.c_str()));
    } catch(Sqrat::Exception ex) {
        std::cout << _SC("Compile Failed: ") << ex.Message();
    }
}

void Scripts::update(double delta)
{
    if (this->m_owner->world()->editorMode()) return;

    for (auto it = m_SqObj.begin(); it !=m_SqObj.end(); ++it)
    {
        Sqrat::Object obj = *it;
        if(!obj.IsNull())
        {
            Sqrat::Function _update(obj, "update");
            if(!_update.IsNull())
            {
                _update.Execute(delta);
            }
        }
    }
}

void Scripts::saveState(ConfStorage& conf) const
{
    ((void)conf);
}

void Scripts::loadState(const ConfStorage& conf)
{
    ((void)conf);
}

void Scripts::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Scripts> _ScriptsComponent;
    _ScriptsComponent.Func("familyName", &Scripts::familyName);
    _ScriptsComponent.Func("loadScripts", &Scripts::loadScripts);
    _ScriptsComponent.Func("typeName", &Scripts::typeName);
    Sqrat::RootTable(vm).Bind("Scripts", _ScriptsComponent);
}

void Scripts::bindSqInstance(Scripts *scr)
{
    Sqrat::RootTable().SetInstance("_entity", scr->m_owner);
}

/*
 * ScriptsComponentMgr
 */

const std::string ScriptsComponentMgr::FamilyName = "Scripts";

ScriptsComponentMgr::ScriptsComponentMgr(int stack)
{
    // Initialise virtual machine
    m_vm = sq_open(stack);

    Sqrat::DefaultVM::Set(m_vm);

    // set print callback function
    sq_setprintfunc(m_vm, squirrel_print_function, NULL);

    sqstd_register_mathlib(m_vm);
    sqstd_register_iolib(m_vm);
    sqrat_register_importlib(m_vm);
}

ScriptsComponentMgr::~ScriptsComponentMgr()
{
    sq_close(m_vm);
}

const std::string& ScriptsComponentMgr::familyName() const
{
    return FamilyName;
}

void ScriptsComponentMgr::initialize(GameWorld* world)
{
    ((void*)world);
    SquirrelBindFactory bindFac;
    bindFac.processQueue(m_vm);
}

void ScriptsComponentMgr::deinitialize()
{
}

IComponent* ScriptsComponentMgr::createComponent(const IComponentDef* desc)
{
    const ScriptsComponentDesc* sDesc = (const ScriptsComponentDesc*)desc;
    m_comps.push_back(new Scripts(this, sDesc));
    return m_comps.back();
}

void ScriptsComponentMgr::markForDeletion(Scripts* comp)
{
    m_comps.erase(std::find(m_comps.begin(), m_comps.end(), comp));
}

void ScriptsComponentMgr::update(double deltaTime)
{
    for (auto it = m_comps.begin(); it != m_comps.end(); ++it)
        (*it)->update(deltaTime);
}

const HSQUIRRELVM& ScriptsComponentMgr::vm() const
{
    return m_vm;
}
