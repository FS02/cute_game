#ifndef PHYSICSCOMPONENT_HPP
#define PHYSICSCOMPONENT_HPP

#include <CuteEngine/World/GameWorld.hpp>
#include <CuteEngine/Utils/Vector2.hpp>
#include <CuteEngine/Utils/Rect.hpp>
#include <CuteEngine/Core/PhysicsDebugDraw.hpp>
#include <Box2D/Box2D.h>
#include <unordered_set>
#include <CuteEngine/Utils/JSON.hpp>
#include <functional>

namespace cute
{
    class CERayCastCallback : public b2RayCastCallback
    {
    public:
        CERayCastCallback(float fraction = 0);

        float32 ReportFixture(b2Fixture* fix, const b2Vec2& pos, const b2Vec2& norm, float32 frac);

        bool hit() const;

        Entity* entity();

        b2Fixture* hitFixture();

        cute::Vector2 hitPosition() const;

        cute::Vector2 hitNormal() const;

        void clear();

        float m_fraction;
    private:
        Entity* m_ent;
        b2Fixture* m_fixture;
        b2Vec2 m_position;
        b2Vec2 m_normal;
    };

    class CEQueryCallback : public b2QueryCallback
    {
    public:
        bool ReportFixture(b2Fixture* fixture);
        const std::unordered_set<Entity*>& entities() const;
        void clear();
    private:
        std::unordered_set<Entity*> m_entities;
    };

    class Physics2D;
    class Physics2DCompMgr;
    class Transform2D;

    class Skeleton
    {
    public:
        static std::string path;

        Skeleton(b2World* world, const std::string& cesfile);
        ~Skeleton();

        bool create(const std::string& cesfile);
        void clear();

        void setUserData(void* data);
        void* getUserData() const;
        void setTransform(const Vector2& pos, float rot);
        const Vector2& getPosition() const;
        float getRotation() const;

        b2Body* mainBody();
        b2Body* body(const std::string& id);
        b2Joint* joint(const std::string& id);
        inline b2RevoluteJoint* revoluteJoint(const std::string& id);
        inline b2DistanceJoint* distanceJoint(const std::string& id);
        inline b2PrismaticJoint* prismaticJoint(const std::string& id);
        inline b2PulleyJoint* pulleyJoint(const std::string& id);
        inline b2GearJoint* gearJoint(const std::string& id);
        inline b2MouseJoint* mouseJoint(const std::string& id);
        inline b2WheelJoint* wheelJoint(const std::string& id);
        inline b2WeldJoint* weldJoint(const std::string& id);
        inline b2RopeJoint* ropeJoint(const std::string& id);
        inline b2FrictionJoint* frictionJoint(const std::string& id);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static void createShapes(Skeleton* sk, std::map<std::string, b2Shape*>& shapes, const json::ValueHandle& value);
        static void createFixtures(Skeleton* sk, std::map<std::string, b2Shape*>& shapes, std::map<std::string, b2FixtureDef*>& fixtures, const json::ValueHandle& value);
        static void createBodies(Skeleton* sk, std::map<std::string, b2FixtureDef*>& fixtures, const json::ValueHandle& value);
        static void createJoints(Skeleton* sk, const json::ValueHandle& value);

        b2World* m_world;
        b2Body* m_mainBody;
        std::map<std::string, b2Body*> m_bodies;
        std::map<std::string, b2Joint*> m_joints;
    };

    //! Decription for PhysicsComponent
    class Physics2DCompDesc : public IComponentDef
    {
    public:
        Physics2DCompDesc();
        Physics2DCompDesc(const Physics2DCompDesc& cp);

        Physics2DCompDesc& operator =(const Physics2DCompDesc& rv);

        virtual void configure(const json::ValueHandle& data);

        const std::string& getCesFile() const;
    private:
        std::string m_cesFile;
    };

    //! Physical component.
    class Physics2D : public IComponent
    {
    public:
        static const std::string TypeName;

        Physics2D(Physics2DCompMgr* mgr, const Physics2DCompDesc* desc);

        virtual const std::string& familyName() const;
        virtual const std::string& typeName() const;

        virtual void initialize(Entity* ent);
        virtual void deinitialize();

        virtual void saveState(ConfStorage &conf) const;
        virtual void loadState(const ConfStorage &conf);

        Entity* rayCast(Vector2 dir, float range, float fraction = 1);
        const std::unordered_set<Entity*>& query(const cute::Rect& area) const;

        //! Returns manager of this component.
        Physics2DCompMgr* manager();
        //! Returns manager of this component.
        const Physics2DCompMgr* manager() const;

        Skeleton* skeleton();

        const Skeleton* skeleton() const;

        //! Returns entity to which this component belongs.
        Entity* owner();
        //! Returns entity to which this component belongs.
        const Entity* owner() const;

        //! Updates values in LocationComponent.
        void update();

        static void bindSquirrel(HSQUIRRELVM vm);

    private:        
        static SQInteger Sq_query(HSQUIRRELVM vm);

        Physics2DCompMgr* m_mgr;
        const Physics2DCompDesc* m_desc;
        Entity* m_owner;
        Skeleton* m_skeleton;
        Transform2D* m_transform;
        unsigned int m_locChangeId;

        static CERayCastCallback* rayCallback;
        static CEQueryCallback* queryCallback;
    };

    //! Manager for physics components.
    class Physics2DCompMgr : public IComponentMgr
    {
    public:
        static const std::string FamilyName;

        Physics2DCompMgr();
        virtual ~Physics2DCompMgr();

        virtual const std::string& familyName() const;

        virtual void initialize(GameWorld* world);
        virtual void deinitialize();

        virtual IComponent* createComponent(const IComponentDef* desc);
        virtual void update(double deltaTime);

        //! Enabled drawing of debug information using PhysicsDebugDraw
        void enableDebugDraw(bool e);

        //! Marks component for deletion. Don't call directly.
        void markForDeletion(Physics2D* comp);

        //! Returns Box2D's world object.
        b2World* world();
        //! Returns Box2D's world object.
        const b2World* world() const;

    private:
        b2World* m_world;
        PhysicsDebugDraw* m_draw;

        std::list<Physics2D*> m_PhysicsCompList;
        std::list<Physics2D*> m_forDeletion;
    };
}

#endif // PHYSICSCOMPONENT_HPP
