#ifndef SPRITECOMPONENT_HPP
#define SPRITECOMPONENT_HPP

#include <CuteEngine/World/Component/Graphics2DComponent.hpp>
#include <CuteEngine/Utils/Vertex.hpp>

namespace cute
{
    class SpriteComponentDesc : public IComponentDef
    {
        cuteEDITABLE
        cutePROPERTY(std::string, FileName, m_fileName)
        cutePROPERTY(cute::Vector2, Size, m_size)
        cutePROPERTY(cute::Vector2, Origin, m_origin)
        cutePROPERTY(cute::Rect, TextureRect, m_textureRect)
        cutePROPERTY(cute::Color, Color, m_color)

    public:
        SpriteComponentDesc();
        SpriteComponentDesc(const std::string& fileName, const cute::Vector2& size, const cute::Vector2& origin);

        virtual void configure(const json::ValueHandle &data);

    private:
        std::string m_fileName;
        cute::Vector2 m_size, m_origin;
        cute::Rect m_textureRect;
        cute::Color m_color;
    };

    class Sprite : public IGraphics2D
    {
        cuteEDITABLE
        cutePROPERTY_WITH_SETTER_AND_GETTER(std::string, FileName, _setTexture, _getTexture)
        cutePROPERTY(cute::Vector2, Size, m_size)
        cutePROPERTY(cute::Vector2, Origin, m_origin)
        cutePROPERTY(cute::Rect, TextureRect, m_textureRect)
        cutePROPERTY(cute::Color, Color, m_color)

    public:
        static const std::string TypeName;

        Sprite(Graphics2DComponentMgr* mgr, const SpriteComponentDesc* desc);
        virtual ~Sprite();

        const std::string& familyName() const;
        virtual const std::string& typeName() const;

        virtual void initialize(Entity* ent);
        virtual void deinitialize();

        virtual void saveState(ConfStorage &conf) const;
        virtual void loadState(const ConfStorage &conf);

        int layer() const;

        virtual void draw(int);

        static void genVertex(Vertex* verts, int startIndex, const Matrix3x3& transform, const Vector2& origin,
                                const Vector2& size, const Vector2& textureSize, const Rect& textureRect,
                                const Color& color);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        Graphics2DComponentMgr* m_mgr;
        const SpriteComponentDesc* m_desc;
        SDL2Texture* m_texture;
        Transform2D* m_transform;
        cute::Vector2 m_size, m_origin;
        cute::Rect m_textureRect;
        cute::Color m_color;

        std::string _setTexture(const std::string& fn);
        std::string _getTexture() const;
    };
}


#endif // SPRITECOMPONENT_HPP
