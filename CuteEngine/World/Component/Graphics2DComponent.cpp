#include <CuteEngine/Core/SDL2/SDL2Texture.hpp>
#include <CuteEngine/World/Component/Graphics2DComponent.hpp>
#include <CuteEngine/World/Component/SpriteComponent.hpp>
#include <CuteEngine/World/ComponentDefFactory.hpp>
#include <SDL_image.h>

using namespace cute;

// See LocationComponent.cpp
namespace
{
    struct RegisterGraphics2DComponentType
    {
        RegisterGraphics2DComponentType()
        {
            std::string family = "Graphics2D";
            std::string type = "Graphics2D";
            IComponentDefCreator* creator = new StdComponentDefCreator(family, type, &CreateDef);
            ComponentDefFactory::Instance().addType(creator);
        }

        static IComponentDef* CreateDef(const std::string& type)
        {
            ((void)type);
            return new Graphics2DComponentDesc();
        }
    };

    RegisterGraphics2DComponentType registerGraphics2DComponent;

    // internal sorting function to sort graphics2d components based on its layer
    void sort_drawable(std::vector<IGraphics2D*>& drawable)
    {
        std::sort(drawable.begin(), drawable.end(), [](IGraphics2D* a, IGraphics2D* b) {
            return b->layer() < a->layer();
            });
    }
}


/*
 * Graphics2DComponentDesc
 */

cuteBEGIN_PROPERTY_IMPL(Graphics2DComponentDesc)
    cutePROPERTY_IMPL(Vector2, Size, true)
    cutePROPERTY_IMPL(Vector2, Origin, true)
    cutePROPERTY_IMPL(float, PointSize, true)
    cutePROPERTY_IMPL(float, LineWidth, true)
    cutePROPERTY_IMPL(float, Capacity, true)
cuteEND_PROPERTY_IMPL

Graphics2DComponentDesc::Graphics2DComponentDesc()
    : IComponentDef(Graphics2DComponentMgr::FamilyName, Graphics2D::TypeName)
{
    cutePREPARE_PROPERTIES();
}

Graphics2DComponentDesc::Graphics2DComponentDesc(const Vector2& size, const Vector2& origin)
    : IComponentDef(Graphics2DComponentMgr::FamilyName, Graphics2D::TypeName)
{
    cutePREPARE_PROPERTIES();

    this->setSize(size);
    this->setOrigin(origin);
    this->setCapacity(600);
}

void Graphics2DComponentDesc::configure(const json::ValueHandle &data)
{
    json::ValueHandle hSize = data.childPair("Size").value();
    if (!hSize.isNull())
    {
        Vector2 size;
        size.x = hSize.childValue(0).number();
        size.y = hSize.childValue(1).number();
        this->setSize(size);
    }

    json::ValueHandle hOrigin = data.childPair("Origin").value();
    if (!hOrigin.isNull())
    {
        Vector2 origin;
        origin.x = hOrigin.childValue(0).number();
        origin.y = hOrigin.childValue(1).number();
        this->setOrigin(origin);
    }

    json::ValueHandle hValue = data.childPair("PointSize").value();
    this->setPointSize(hValue.number(1.f));

    hValue = data.childPair("LineWidth").value();
    this->setLineWidth(hValue.number(1.f));

    hValue = data.childPair("Capacity").value();
    this->setCapacity(hValue.number(600));
}

/*
 * Graphics2DComponent
 */

cuteBEGIN_PROPERTY_IMPL(Graphics2D)
    cutePROPERTY_IMPL(float, PointSize, true);
    cutePROPERTY_IMPL(float, LineWidth, true)
    cutePROPERTY_IMPL(Vector2, Size, true)
    cutePROPERTY_IMPL(Vector2, Origin, true)
cuteEND_PROPERTY_IMPL

const std::string Graphics2D::TypeName = "Graphics2D";

Graphics2D::Graphics2D(Graphics2DComponentMgr* mgr, const Graphics2DComponentDesc* desc, unsigned int capacity)
    : m_mgr(mgr), m_desc(desc), m_transform(nullptr), m_capacity(capacity), m_triangleCount(0), m_lineCount(0), m_pointCount(0),
      m_pointSize(1.f), m_lineWidth(1.f)
{
    cutePREPARE_PROPERTIES();

    this->setSize(desc->getSize());
    this->setOrigin(desc->getOrigin());
    this->setPointSize(desc->getPointSize());
    this->setLineWidth(desc->getLineWidth());

    m_vertex = new Vertex[m_capacity];
}

Graphics2D::~Graphics2D()
{
    delete [] m_vertex;
}

const std::string& Graphics2D::familyName() const
{
    return Graphics2DComponentMgr::FamilyName;
}

const std::string& Graphics2D::typeName() const
{
    return Graphics2D::TypeName;
}

void Graphics2D::initialize(Entity* ent)
{
    IComponent* _tr = ent->componentByType(Transform2D::TypeName);
    if(_tr)
        m_transform = (Transform2D*)_tr;
}

void Graphics2D::deinitialize()
{
    /*
     * Entity doesn't delete entities, it just deinitializes them.
     * Redundant entities are added to m_forDeletion in Graphics2DComponentMgr, and then deleted in Graphics2DComponentMgr::update
     */
    m_mgr->markForDeletion(this);
}

void Graphics2D::saveState(ConfStorage &conf) const
{
    cuteSAVE_STATE(conf, Size, m_desc, Size);
    cuteSAVE_STATE(conf, Origin, m_desc, Origin);
    cuteSAVE_STATE(conf, PointSize, m_desc, PointSize);
    cuteSAVE_STATE(conf, LineWidth, m_desc, LineWidth);
}

void Graphics2D::loadState(const ConfStorage &conf)
{
    this->IEditable::loadState(conf, TypeName+".");
}

int Graphics2D::layer() const
{
    if (m_transform)
        return m_transform->getLayer();

    return 0;
}

void Graphics2D::draw(int camRemaining)
{
    /*
     * we use 3/6 array space for triangle drawing, 2/6 for line and the rest is for point
     */
    const int triangleBegin = 0;
    const int lineBegin = m_capacity * 3/6;
    const int pointBegin = lineBegin + m_capacity * 2/6;

    glPointSize(m_pointSize);
    glLineWidth(m_lineWidth);
    glVertexPointer(2, GL_FLOAT, sizeof(Vertex), m_vertex);
    glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Vertex), &m_vertex[0].r);

    // draw triangles
    glDrawArrays(GL_TRIANGLES, triangleBegin, m_triangleCount);
    // draw lines
    glDrawArrays(GL_LINES, lineBegin, m_lineCount);
    // draw points
    glDrawArrays(GL_POINTS, pointBegin, m_pointCount);

    if (camRemaining <= 0)
        m_triangleCount = m_lineCount = m_pointCount = 0;
}

void Graphics2D::drawPoint(const Vector2 &point, const Color &color)
{
    const unsigned int maxPointIndex = m_capacity;
    const unsigned int minPointIndex = m_capacity - (m_capacity * 1 / 6);
    if (m_pointCount + 1 > maxPointIndex - minPointIndex)
        this->draw(0);


    Vector2 pos = m_transform->matrix() * point;

    Vertex vert;
    vert.x = pos.x;
    vert.y = pos.y;
    vert.r = color.r;
    vert.g = color.g;
    vert.b = color.b;
    vert.a = color.a;
    m_vertex[minPointIndex + (m_pointCount++)] = vert;
}

void Graphics2D::drawLine(const Vector2 &p1, const Vector2 &p2, const Color &color)
{
    const unsigned int maxLineIndex = m_capacity - (m_capacity * 1 / 6);
    const unsigned int minLineindex = m_capacity - (m_capacity * 3 / 6);

    if (m_lineCount + 2 > maxLineIndex - minLineindex)
        this->draw(0);

    Vector2 pos[]=
    {
        m_transform->matrix() * p1,
        m_transform->matrix() * p2
    };

    Vertex vert;
    vert.r = color.r;
    vert.g = color.g;
    vert.b = color.b;
    vert.a = color.a;

    vert.x = pos[0].x;
    vert.y = pos[0].y;
    m_vertex[minLineindex + (m_lineCount++)] = vert;

    vert.x = pos[1].x;
    vert.y = pos[1].y;
    m_vertex[minLineindex + (m_lineCount++)] = vert;
}

void Graphics2D::drawPolygon(const Vector2 *vertices, int32 vertexCount, const Color &color)
{
    for(int i = 0; i < vertexCount; ++i)
    {
        int next = i + 1;
        if(next == vertexCount)
            next = 0;
        this->drawLine(vertices[i], vertices[next], color);
    }
}

void Graphics2D::drawCircle(const Vector2 &center, float32 radius, int32 segments, const Color &color)
{
    float step = 2.0f*M_PI / segments;
    float angle = 0.0f;
    b2Vec2 a, b;
    for(int i = 0; i < segments; ++i)
    {
        a.x = std::cos(angle)*radius + center.x;
        a.y = std::sin(angle)*radius + center.y;
        b.x = std::cos(angle + step)*radius + center.x;
        b.y = std::sin(angle + step)*radius + center.y;
        this->drawLine(a, b, color);

        angle += step;
    }
}

void Graphics2D::drawSolidPolygon(const Vector2 *vertices, int32 vertexCount, const Color &color)
{

}


void Graphics2D::drawSolidCircle(const Vector2 &center, float32 radius, int32 segments, const Color &color)
{
    const unsigned int maxTriangleIndex = m_capacity * 3 / 6;

    float step = 2.0f*M_PI / segments;
    float angle = 0.0f;

    auto mt = m_transform->matrix();
    Vector2 ctr = mt * ctr;

    Vertex c, a, b;
    c.x = ctr.x;
    c.y = ctr.y;
    c.r = color.r;
    c.g = color.g;
    c.b = color.b;
    c.a = color.a;
    a = b = c;

    if(m_triangleCount + segments*3 > maxTriangleIndex)
        this->draw(0);

    for(int i = 0; i < segments; ++i)
    {
        Vector2 p[] =
        {
            mt * Vector2(std::cos(angle)*radius + center.x, std::sin(angle)*radius + center.y),
            mt * Vector2(std::cos(angle + step)*radius + center.x, std::sin(angle + step)*radius + center.y)
        };
        a.x = p[0].x; a.y = p[0].y;
        b.x = p[1].x; b.y = p[1].y;

        m_vertex[m_triangleCount++] = c;
        m_vertex[m_triangleCount++] = a;
        m_vertex[m_triangleCount++] = b;

        angle += step;
    }
}

void Graphics2D::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Graphics2D> _Graphics2DComponent;
    _Graphics2DComponent.Func("familyName", &Graphics2D::familyName);
    _Graphics2DComponent.Func("getOrigin", &Graphics2D::getOrigin);
    _Graphics2DComponent.Func("getSize", &Graphics2D::getSize);
    _Graphics2DComponent.Func("setOrigin", &Graphics2D::setOrigin);
    _Graphics2DComponent.Func("setSize", &Graphics2D::setSize);
    _Graphics2DComponent.Func("typeName", &Graphics2D::typeName);
    _Graphics2DComponent.Func("drawCircle", &Graphics2D::drawCircle);
    _Graphics2DComponent.Func("drawLine", &Graphics2D::drawLine);
    _Graphics2DComponent.Func("drawPoint", &Graphics2D::drawPoint);
    _Graphics2DComponent.Func("drawPolygon", &Graphics2D::drawPolygon);
    _Graphics2DComponent.Func("drawSolidCircle", &Graphics2D::drawSolidCircle);
    _Graphics2DComponent.Func("drawSolidPolygon", &Graphics2D::drawSolidPolygon);
    Sqrat::RootTable(vm).Bind("Graphics2D", _Graphics2DComponent);
}

/*
 * Graphics2DComponentMgr
 */

const std::string Graphics2DComponentMgr::FamilyName = "Graphics2D";

Graphics2DComponentMgr::Graphics2DComponentMgr()
    : m_textures(&SDL2Texture::Create), m_isWireframeMode(false)
{
    /*
     * We need this for running the engine in the Qt window.
     * Qt makes OpenGL context current only when you request window update.
     * To load OpenGL textures you need OpenGL context, therefore you can load textures only in window's update method.
     * When loading policy is LP_Deferred, textures will be loaded in TextureMgr::deferredLoad(), which is called in Graphics2DComponentMgr::update(),
     * which is called from Qt window's update method.
     */
    m_textures.setLoadPolicy(LP_Deferred);
}

Graphics2DComponentMgr::~Graphics2DComponentMgr()
{
    this->deinitialize();
}

const std::string& Graphics2DComponentMgr::familyName() const
{
    return FamilyName;
}

void Graphics2DComponentMgr::initialize(GameWorld* world)
{
    (void(world));

    Vector2 wndSize(800,600);

    // Initialise default camera
    m_defCamera.setCenter(0.0f, 0.0f);
    m_defCamera.setRotation(0.0f);
    m_defCamera.setSize(wndSize.x, wndSize.y);
    m_defCamera.setViewport(Rect(0.f, 0.f, wndSize.x, wndSize.y));

    int flags = IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF | IMG_INIT_WEBP;
    int res = IMG_Init(flags);
    if((res & flags) != flags)
        std::clog <<"[SDL_image] " << SDL_GetError() << "\n";
}

void Graphics2DComponentMgr::deinitialize()
{
    for(IGraphics2D* comp : m_drawableForDeletion)
        delete comp;
    m_drawableForDeletion.clear();

    for(Camera2D* comp : m_camForDeletion)
        delete comp;
    m_camForDeletion.clear();

    m_textures.unloadAll();
}

IComponent* Graphics2DComponentMgr::createComponent(const IComponentDef* desc)
{
    if (desc->typeName == Graphics2D::TypeName)
    {
        const Graphics2DComponentDesc* gDesc = (const Graphics2DComponentDesc*)desc;
        Graphics2D* gComp = new Graphics2D(this, gDesc, gDesc->getCapacity());
        m_drawable.push_back(gComp);

        // Sort the component based on it's layer
        sort_drawable(m_drawable);

        return gComp;
    }
    else if (desc->typeName == Sprite::TypeName)
    {
        const SpriteComponentDesc* sDesc = (const SpriteComponentDesc*)desc;
        Sprite* sComp = new Sprite(this, sDesc);
        m_drawable.push_back(sComp);

        // Sort the component based on it's layer
        sort_drawable(m_drawable);

        return sComp;
    }
    else if (desc->typeName == Camera2D::TypeName)
    {
        const Camera2DComponentDesc* cDesc = (const Camera2DComponentDesc*)desc;
        m_cameras.push_back(new Camera2D(this, cDesc));
        return m_cameras.back();
    }
    return nullptr;
}

void Graphics2DComponentMgr::update(double deltaTime)
{
    (void(deltaTime));

    m_textures.deferredLoad();

    if (m_isWireframeMode)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    // iterate camera in reverse direction
    int camRemaining = m_activeCamera.size();
    for (auto rit = m_activeCamera.rbegin(); rit != m_activeCamera.rend(); ++rit)
    {
        --camRemaining;

        auto cam = *rit;
        // set the viewport
        Rect viewport = cam->getViewport();
        glViewport(viewport.x, viewport.y, viewport.width, viewport.height);

        // Set the camera
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glScalef(cam->getSize().x/viewport.width, cam->getSize().y/viewport.height, 0);
        glRotatef(cam->getRotation(), 0, 0, 1);
        glTranslatef(-cam->getCenter().x, -cam->getCenter().y, 0.0f);

        // Draw in reverse, from higher layer to lower layer
        for (auto it = m_drawable.begin(); it != m_drawable.end(); ++it)
            (*it)->draw(camRemaining);
    }

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    for(IGraphics2D* comp : m_drawableForDeletion)
        delete comp;
    m_drawableForDeletion.clear();

    for(Camera2D* comp : m_camForDeletion)
        delete comp;
    m_camForDeletion.clear();
}

void Graphics2DComponentMgr::setActiveCamera(Camera2D *cam)
{
    m_activeCamera.insert(&cam->getCameraDef());
}

void Graphics2DComponentMgr::setInactiveCamera(Camera2D *cam)
{
    m_activeCamera.erase(m_activeCamera.find(&cam->getCameraDef()));
}

bool Graphics2DComponentMgr::isActiveCamera(Camera2D *cam)
{
    return m_activeCamera.find(&cam->getCameraDef()) != m_activeCamera.end();
}

const Camera2* Graphics2DComponentMgr::getActiveCameraDef(unsigned int index) const
{
    if (index > m_activeCamera.size())
        return nullptr;
    else if (index == m_activeCamera.size())
        return &m_defCamera;

    auto it = m_activeCamera.begin();
    for (unsigned int i = 0; i < index; ++i) ++it;

    return *it;
}

TextureMgr& Graphics2DComponentMgr::resourceMgr()
{
    return m_textures;
}

void Graphics2DComponentMgr::markForDeletion(IGraphics2D* comp)
{
    m_drawable.erase(std::find(m_drawable.begin(), m_drawable.end(), comp));
    m_drawableForDeletion.push_back(comp);
}

void Graphics2DComponentMgr::markForDeletion(Camera2D* comp)
{
    m_cameras.erase(std::find(m_cameras.begin(), m_cameras.end(), comp));
    m_camForDeletion.push_back(comp);
}
