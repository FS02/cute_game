#ifndef CAMERACOMPONENT_HPP
#define CAMERACOMPONENT_HPP

#include <CuteEngine/Core/Camera.hpp>
#include <CuteEngine/World/GameWorld.hpp>
#include <CuteEngine/Utils/Rect.hpp>

namespace cute
{
    class Camera2DComponentDesc : public IComponentDef
    {
        cuteEDITABLE
        cutePROPERTY(Vector2, Center, m_center)
        cutePROPERTY(Vector2, Size, m_size)
        cutePROPERTY(float, Rotation, m_rotation)
        cutePROPERTY(Rect, Viewport, m_viewport)
        cutePROPERTY(bool, Active, m_active)

    public:
        Camera2DComponentDesc();
        Camera2DComponentDesc(const Camera2DComponentDesc&) = delete;

        virtual void configure(const json::ValueHandle &data);

    private:
        cute::Vector2 m_center;
        cute::Vector2 m_size;
        float m_rotation;
        cute::Rect m_viewport;
        bool m_active;
    };

    class Graphics2DComponentMgr;

    class Camera2D : public IComponent
    {
    public:
        static const std::string TypeName;

        Camera2D(Graphics2DComponentMgr* mgr, const Camera2DComponentDesc* desc);

        virtual const std::string& familyName() const;
        virtual const std::string& typeName() const;

        virtual void initialize(Entity* ent);
        virtual void deinitialize();

        virtual void saveState(ConfStorage& conf) const;
        virtual void loadState(const ConfStorage& conf);

        void update(double deltaTime);

        void move(const cute::Vector2& offset);

        void rotate(float angle);

        void zoom(float factor);

        void activate();

        bool isActive();

        void setCenter(const Vector2& center);

        const Vector2& getCenter() const;

        void setSize(const Vector2& size);

        const Vector2& getSize() const;

        void setRotation(float angle);

        float getRotation() const;

        void setViewport(const Rect& rect);

        const Rect& getViewport() const;

        void setCameraDef(const Camera2& cam);

        const Camera2& getCameraDef() const;

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        Graphics2DComponentMgr* m_mgr;
        const Camera2DComponentDesc* m_desc;
        Camera2 m_camera;
    };
}

#endif // CAMERACOMPONENT_HPP
