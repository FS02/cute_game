#include "SoundComponent.hpp"
#include <CuteEngine/World/ComponentDefFactory.hpp>
#include <SDL.h>
#include <iostream>

using namespace cute;

// See LocationComponent.cpp
namespace
{
    struct RegisterSoundComponentType
    {
        RegisterSoundComponentType()
        {
            std::string family = "Sound";
            std::string type = "Sound";
            IComponentDefCreator* creator = new StdComponentDefCreator(family, type, &CreateDef);
            ComponentDefFactory::Instance().addType(creator);
        }

        static IComponentDef* CreateDef(const std::string& type)
        {
            ((void)type);
            return new SoundComponentDesc();
        }
    };

    RegisterSoundComponentType g_registerSoundComponent;
}

/*
 * SoundResource
 */

SoundResource::SoundResource(const std::string &fileName)
    : IResource(fileName), m_chunk(nullptr)
{
}

SoundResource::~SoundResource()
{
    if(m_chunk)
        Mix_FreeChunk(m_chunk);
}

bool SoundResource::load()
{
    if(m_chunk)
        Mix_FreeChunk(m_chunk);

    m_chunk = Mix_LoadWAV((ResourcePath + "Sounds/" + this->fileName()).c_str());

    if(!m_chunk)
    {
        std::clog << "[SDL_mixer] "<< Mix_GetError() << "\n";
        return false;
    }

    return true;
}

int SoundResource::startLooped(int channel, int times)
{
    if(!m_chunk)
        return -1;

    return Mix_PlayChannelTimed(channel, m_chunk, times, -1);
}

int SoundResource::startTimed(int channel, double time)
{
    if(!m_chunk)
        return -1;

    if(time <= 0)
        return -1;

    return Mix_PlayChannelTimed(channel, m_chunk, -1, (int)(time*1000.0));
}

SoundResource* SoundResource::Create(const std::string& fileName)
{
    return new SoundResource(fileName);
}

/*
 * SoundComponentDesc
 */

cuteBEGIN_PROPERTY_IMPL(SoundComponentDesc)
    cutePROPERTY_IMPL(std::string, FileName, true)
cuteEND_PROPERTY_IMPL

SoundComponentDesc::SoundComponentDesc()
    : IComponentDef(SoundComponentMgr::FamilyName, Sound::TypeName)
{
    cutePREPARE_PROPERTIES();
}

void SoundComponentDesc::configure(const json::ValueHandle &data)
{
    json::ValueHandle hFileName = data.childPair("FileName").value();
    this->setFileName(hFileName.string());
}

/*
 * SoundComponent
 */

cuteBEGIN_PROPERTY_IMPL(Sound)
    cutePROPERTY_IMPL(std::string, FileName, true)
cuteEND_PROPERTY_IMPL

const std::string Sound::TypeName = "Sound";

Sound::Sound(SoundComponentMgr *mgr, const SoundComponentDesc* desc)
    : m_mgr(mgr), m_desc(desc), m_sound(nullptr), m_channel(-1), m_transform(nullptr)
{
    cutePREPARE_PROPERTIES();

    this->setFileName(m_desc->getFileName());
}

void Sound::startLooped(int times)
{
    this->stop();
    m_channel = -1; // choose any
    if(m_sound->startLooped(m_channel, times) == -1)
    {
        m_channel = -1;
        std::clog << "[Sound subsystem] Couldn't play a sample! (not enough channels?)\n";
    }
}

void Sound::startTimed(double time)
{
    this->stop();
    if(time <= 0)
        return;
    m_channel = -1;  // choose any
    if(m_sound->startTimed(m_channel, time) == -1)
    {
        m_channel = -1;
        std::clog << "[Sound subsystem] Couldn't play a sample! (not enough channels?)\n";
    }
}

void Sound::stop()
{
    if(m_channel == -1)
        return;

    Mix_HaltChannel(m_channel);
    m_channel = -1;
}

const std::string& Sound::familyName() const
{
    return SoundComponentMgr::FamilyName;
}

const std::string& Sound::typeName() const
{
    return TypeName;
}

void Sound::initialize(Entity *ent)
{
    IComponent* _loc = ent->componentByType(Transform2D::TypeName);
    if(_loc)
        m_transform = (Transform2D*)_loc;
    this->startLooped(3);
}

void Sound::deinitialize()
{
    this->stop();
    m_mgr->markForDeletion(this);
}

void Sound::saveState(ConfStorage &conf) const
{
    cuteSAVE_STATE(conf, FileName, m_desc, FileName);
}

void Sound::loadState(const ConfStorage &conf)
{
    this->IEditable::loadState(conf, TypeName+".");
}

std::string Sound::_setSndFileName(const std::string &fn)
{
    m_sound = m_mgr->resourceMgr().resource(fn);
    return m_sound->fileName();
}

std::string Sound::_getSndFileName() const
{
    if(m_sound)
        return m_sound->fileName();
    return std::string();
}

void Sound::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Sound> _SoundComponent;
    _SoundComponent.Func("familyName", &Sound::familyName);
    _SoundComponent.Func("getFileName", &Sound::getFileName);
    _SoundComponent.Func("setFileName", &Sound::setFileName);
    _SoundComponent.Func("startLooped", &Sound::startLooped);
    _SoundComponent.Func("startTimed", &Sound::startTimed);
    _SoundComponent.Func("stop", &Sound::stop);
    _SoundComponent.Func("typeName", &Sound::typeName);
    Sqrat::RootTable(vm).Bind("Sound", _SoundComponent);
}

/*
 * SoundComponentMgr
 */

const std::string SoundComponentMgr::FamilyName = "Sound";

SoundComponentMgr::SoundComponentMgr()
    : m_sounds(&SoundResource::Create)
{
}

SoundComponentMgr::~SoundComponentMgr()
{
    this->deinitialize();
}

const std::string& SoundComponentMgr::familyName() const
{
    return FamilyName;
}

void SoundComponentMgr::initialize(GameWorld *world)
{
    constexpr int numChannels = 32;

    ((void)world);

    SDL_InitSubSystem(SDL_INIT_AUDIO);
    Mix_OpenAudio(22050, AUDIO_S16, 2, 4096);

    Mix_AllocateChannels(numChannels);

    int flags = MIX_INIT_OGG|MIX_INIT_FLAC|MIX_INIT_MP3;
    Mix_Init(flags);
}

void SoundComponentMgr::deinitialize()
{
    for(auto ptr : m_forDeletion)
        delete ptr;
    m_forDeletion.clear();

    if(!SDL_WasInit(SDL_INIT_AUDIO))
        return;

    Mix_CloseAudio();
    Mix_Quit();
    SDL_QuitSubSystem(SDL_INIT_AUDIO);
}

IComponent* SoundComponentMgr::createComponent(const IComponentDef *desc)
{
    const SoundComponentDesc* sndDesc = (const SoundComponentDesc*)desc;
    return new Sound(this, sndDesc);
}

void SoundComponentMgr::update(double deltaTime)
{
    ((void)deltaTime);

    for(auto ptr : m_forDeletion)
        delete ptr;
    m_forDeletion.clear();
}

SoundMgr& SoundComponentMgr::resourceMgr()
{
    return m_sounds;
}

void SoundComponentMgr::markForDeletion(Sound *comp)
{
    m_forDeletion.push_back(comp);
}
