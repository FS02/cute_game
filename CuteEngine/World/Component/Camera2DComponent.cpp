#include <CuteEngine/World/Component/Graphics2DComponent.hpp>
#include <CuteEngine/World/Component/Camera2DComponent.hpp>
#include <CuteEngine/World/ComponentDefFactory.hpp>

using namespace cute;

// see LocationComponent.cpp
namespace
{
    struct RegisterCameraComponent
    {
        RegisterCameraComponent()
        {
            std::string family = "Graphics2D";
            std::string type = "Camera2D";
            IComponentDefCreator* creator = new StdComponentDefCreator(family, type, &CreateDef);
            ComponentDefFactory::Instance().addType(creator);
        }

        static IComponentDef* CreateDef(const std::string& type)
        {
            ((void)type); // unused argument
            return new Camera2DComponentDesc();
        }
    };

    RegisterCameraComponent registerCameraComponent;
}

/*
 * Camera2DComponentDesc
 */

cuteBEGIN_PROPERTY_IMPL(Camera2DComponentDesc)
    cutePROPERTY_IMPL(Vector2, Center, true)
    cutePROPERTY_IMPL(Vector2, Size, true)
    cutePROPERTY_IMPL(float, Rotation, true)
    cutePROPERTY_IMPL(Rect, Viewport, true)
    cutePROPERTY_IMPL(bool, Active, true)
cuteEND_PROPERTY_IMPL

Camera2DComponentDesc::Camera2DComponentDesc()
    : IComponentDef(Graphics2DComponentMgr::FamilyName, Camera2D::TypeName)
{
       cutePREPARE_PROPERTIES();

       this->setSize(Vector2(800,600));
       this->setViewport(Rect(0, 0, 800, 600));
       this->setActive(false);
}

void Camera2DComponentDesc::configure(const json::ValueHandle &data)
{
    json::ValueHandle hCenter = data.childPair("Center").value();
    if (!hCenter.isNull())
    {
        Vector2 center;
        center.x = hCenter.childValue(0).number();
        center.y = hCenter.childValue(1).number();
        this->setCenter(center);
    }

    json::ValueHandle hSize = data.childPair("Size").value();
    if (!hSize.isNull())
    {
        Vector2 size;
        size.x = hSize.childValue(0).number();
        size.y = hSize.childValue(1).number();
        this->setSize(size);
    }

    json::ValueHandle hRotation = data.childPair("Rotation").value();
    this->setRotation(hRotation.number());

    json::ValueHandle hViewport = data.childPair("Viewport").value();
    if (!hViewport.isNull())
    {
        Rect viewport;
        viewport.x = hViewport.childValue(0).number();
        viewport.y = hViewport.childValue(1).number();
        viewport.width = hViewport.childValue(2).number();
        viewport.height = hViewport.childValue(3).number();
        this->setViewport(viewport);
    }

    json::ValueHandle hActive = data.childPair("Active").value();
    this->setActive(hActive.boolean(false));
}

/*
 * Camera2DComponent
 */

const std::string Camera2D::TypeName = "Camera2D";

Camera2D::Camera2D(Graphics2DComponentMgr *mgr, const Camera2DComponentDesc *desc)
    : m_mgr(mgr), m_desc(desc)
{
   this->setCenter(m_desc->getCenter());
   this->setSize(m_desc->getSize());
   this->setRotation(m_desc->getRotation());
   this->setViewport(m_desc->getViewport());
}

const std::string& Camera2D::familyName() const
{
    return Graphics2DComponentMgr::FamilyName;
}

const std::string& Camera2D::typeName() const
{
    return Camera2D::TypeName;
}

void Camera2D::initialize(Entity *ent)
{
    (void)ent;
    if (m_desc->getActive())
        m_mgr->setActiveCamera(this);
}

void Camera2D::deinitialize()
{
    m_mgr->setInactiveCamera(this);
}

void Camera2D::saveState(ConfStorage &conf) const
{
    (void)conf;
}

void Camera2D::loadState(const ConfStorage &conf)
{
    (void)conf;
}

void Camera2D::update(double deltaTime)
{
    (void)deltaTime;
}

void Camera2D::move(const Vector2 &offset)
{
    m_camera.move(offset);
}

void Camera2D::rotate(float angle)
{
    m_camera.rotate(angle);
}

void Camera2D::zoom(float factor)
{
    m_camera.zoom(factor);
}

void Camera2D::activate()
{
    m_mgr->setActiveCamera(this);
}

bool Camera2D::isActive()
{
    return m_mgr->isActiveCamera(this);
}

void Camera2D::setCenter(const Vector2 &center)
{
    m_camera.setCenter(center);
}

const Vector2& Camera2D::getCenter() const
{
    return m_camera.getCenter();
}

void Camera2D::setSize(const Vector2 &size)
{
    m_camera.setSize(size);
}

const Vector2& Camera2D::getSize() const
{
    return m_camera.getSize();
}

void Camera2D::setRotation(float angle)
{
    m_camera.setRotation(angle);
}

float Camera2D::getRotation() const
{
    return m_camera.getRotation();
}

void Camera2D::setViewport(const Rect &rect)
{
    m_camera.setViewport(rect);
}

const Rect& Camera2D::getViewport() const
{
    return m_camera.getViewport();
}

void Camera2D::setCameraDef(const Camera2 &cam)
{
    m_camera = cam;
    if (isActive())
        this->activate();
}

const Camera2& Camera2D::getCameraDef() const
{
    return m_camera;
}

void Camera2D::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Camera2D> _CamComp;
    _CamComp.Func("activate", &Camera2D::activate);
    _CamComp.Func("isActivate", &Camera2D::isActive);
    _CamComp.Func("familyName", &Camera2D::familyName);
    _CamComp.Func("getCenter", &Camera2D::getCenter);
    _CamComp.Func("getRotation", &Camera2D::getRotation);
    _CamComp.Func("getSize", &Camera2D::getSize);
    _CamComp.Func("getViewport", &Camera2D::getViewport);
    _CamComp.Func("move", &Camera2D::move);
    _CamComp.Func("rotate", &Camera2D::rotate);
    _CamComp.Func("setCenter", &Camera2D::setCenter);
    _CamComp.Func("setRotation", &Camera2D::setRotation);
    _CamComp.Func("setSize", &Camera2D::setSize);
    _CamComp.Func("setViewport", &Camera2D::setViewport);
    _CamComp.Func("typeName", &Camera2D::typeName);
    _CamComp.Func("zoom", &Camera2D::zoom);
    _CamComp.Func("setCameraDef", &Camera2D::setCameraDef);
    _CamComp.Func("getCameraDef", &Camera2D::getCameraDef);
    Sqrat::RootTable(vm).Bind("Camera2D", _CamComp);
}
