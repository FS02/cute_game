#ifndef GRAPHICSCOMPONENT_HPP
#define GRAPHICSCOMPONENT_HPP

#include <CuteEngine/Core/SDL2/SDL2Texture.hpp>
#include <CuteEngine/World/Component/Transform2DComponent.hpp>
#include <CuteEngine/World/Component/Camera2DComponent.hpp>
#include <CuteEngine/Core/OpenGL.hpp>
#include <CuteEngine/World/GameWorld.hpp>
#include <CuteEngine/Utils/Color.h>
#include <CuteEngine/Utils/Vertex.hpp>
#include <set>

namespace cute
{
    class Graphics2DComponentMgr;

    class IGraphics2D : public IComponent
    {
    public:
        virtual void draw(int) = 0;
        virtual int layer() const = 0;
    };

    class Graphics2DComponentDesc : public IComponentDef
    {
        cuteEDITABLE
        cutePROPERTY(cute::Vector2, Size, m_size)
        cutePROPERTY(cute::Vector2, Origin, m_origin)
        cutePROPERTY(float, PointSize, m_pointSize)
        cutePROPERTY(float, LineWidth, m_lineWidth)
        cutePROPERTY(float, Capacity, m_capacity)

    public:
        Graphics2DComponentDesc();
        Graphics2DComponentDesc(const cute::Vector2& size, const cute::Vector2& origin);

        virtual void configure(const json::ValueHandle &data);

    private:
        cute::Vector2 m_size, m_origin;
        float m_pointSize, m_lineWidth, m_capacity;
    };

    class Graphics2D : public IGraphics2D
    {
        cuteEDITABLE
        cutePROPERTY(float, PointSize, m_pointSize)
        cutePROPERTY(float, LineWidth, m_lineWidth)
        cutePROPERTY(cute::Vector2, Size, m_size)
        cutePROPERTY(cute::Vector2, Origin, m_origin)

    public:
        static const std::string TypeName;

        Graphics2D(Graphics2DComponentMgr* mgr, const Graphics2DComponentDesc* desc, unsigned int capacity = 600);
        ~Graphics2D();

        const std::string& familyName() const;
        const std::string& typeName() const;

        void initialize(Entity* ent);
        void deinitialize();

        void saveState(ConfStorage &conf) const;
        void loadState(const ConfStorage &conf);

        int layer() const;

        void draw(int camRemaining);

        void drawPoint(const Vector2& point, const Color& color);
        void drawLine(const Vector2 &p1, const Vector2 &p2, const Color &color);
        void drawPolygon(const Vector2 *vertices, int32 vertexCount, const Color &color);
        void drawCircle(const Vector2 &center, float32 radius, int32 segments, const Color &color);
        void drawSolidPolygon(const Vector2* vertices, int32 vertexCount, const Color& color);
        void drawSolidCircle(const Vector2& center, float32 radius, int32 segments, const Color& color);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        Graphics2DComponentMgr* m_mgr;
        const Graphics2DComponentDesc* m_desc;
        Transform2D* m_transform;
        Vertex* m_vertex;
        unsigned int m_capacity;
        unsigned int m_triangleCount;
        unsigned int m_lineCount;
        unsigned int m_pointCount;
        float m_pointSize, m_lineWidth;
        cute::Vector2 m_size, m_origin;
    };

    class Graphics2DComponentMgr : public IComponentMgr
    {
        friend class GameWorld;
    public:
        static const std::string FamilyName;

        Graphics2DComponentMgr();
        ~Graphics2DComponentMgr();

        const std::string& familyName() const;

        virtual void initialize(GameWorld* world);
        virtual void deinitialize();

        IComponent* createComponent(const IComponentDef* desc);
        void update(double deltaTime);

        void setActiveCamera(Camera2D* cam);
        void setInactiveCamera(Camera2D* cam);
        bool isActiveCamera(Camera2D* cam);
        const Camera2* getActiveCameraDef(unsigned int index = 0) const;

        TextureMgr& resourceMgr();
        void markForDeletion(IGraphics2D* comp);
        void markForDeletion(Camera2D* comp);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        Camera2 m_defCamera;

        std::vector<IGraphics2D*> m_drawable;
        std::vector<Camera2D*> m_cameras;
        std::set<const Camera2*> m_activeCamera;

        std::list<IGraphics2D*> m_drawableForDeletion;
        std::list<Camera2D*> m_camForDeletion;

        TextureMgr m_textures;

        bool m_isWireframeMode;
    };
}

#endif // GRAPHICSCOMPONENT_HPP

