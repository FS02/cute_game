#ifndef SCRIPTSCOMPONENT_HPP
#define SCRIPTSCOMPONENT_HPP

#define NO_COMPILER

#include <CuteEngine/World/GameWorld.hpp>
#include <squirrel.h>
#include <sqrat.h>
#include <sqratimport.h>
#include <cstring>
#include <unordered_map>
#include <CuteEngine/World/Component/Graphics2DComponent.hpp>
#include <CuteEngine/World/Component/Transform2DComponent.hpp>
#include <CuteEngine/World/Component/Physics2DComponent.hpp>
#include <CuteEngine/World/Component/SoundComponent.hpp>
#include <CuteEngine/World/Component/Camera2DComponent.hpp>

namespace cute
{
    class ScriptsComponentMgr;

    class ScriptsComponentDesc : public IComponentDef
    {
        cuteEDITABLE
        cutePROPERTY(std::vector<std::string>, Modules, m_modules)

    public:
        ScriptsComponentDesc();

        virtual void configure(const json::ValueHandle &data);

    private:
        std::vector<std::string> m_modules;
    };

    class Scripts : public IComponent
    {
    public:
        static const std::string TypeName;
        static const std::string Path;

        Scripts(ScriptsComponentMgr* mgr, const ScriptsComponentDesc* desc);
        ~Scripts();

        const std::string& familyName() const;
        const std::string& typeName() const;

        void initialize(Entity* ent);
        void deinitialize();

        void loadScripts(const std::string& file);

        void update(double delta);

        void saveState(ConfStorage& conf) const;
        void loadState(const ConfStorage& conf);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static void bindSqInstance(Scripts* scr);

        ScriptsComponentMgr* m_mgr;
        Entity* m_owner;
        const ScriptsComponentDesc* m_desc;
        std::list<Sqrat::Object> m_SqObj;
    };

    class ScriptsComponentMgr : public IComponentMgr
    {
    public:
        static const std::string FamilyName;

        ScriptsComponentMgr(int stack = 1024);
        ~ScriptsComponentMgr();

        const std::string& familyName() const;

        virtual void initialize(GameWorld* world);
        virtual void deinitialize();

        IComponent* createComponent(const IComponentDef* desc);

        void markForDeletion(Scripts* comp);

        void update(double deltaTime);

        const HSQUIRRELVM& vm() const;

    private:
        HSQUIRRELVM m_vm;
        std::vector<Scripts*> m_comps;
    };
}

#endif // SCRIPTSCOMPONENT_HPP
