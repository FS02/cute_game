#include <CuteEngine/World/Component/SpriteComponent.hpp>
#include <CuteEngine/World/ComponentDefFactory.hpp>

using namespace cute;

namespace
{
    struct RegisterGraphics2DComponentType
    {
        RegisterGraphics2DComponentType()
        {
            std::string family = "Graphics2D";
            std::string type = "Sprite";
            IComponentDefCreator* creator = new StdComponentDefCreator(family, type, &CreateDef);
            ComponentDefFactory::Instance().addType(creator);
        }

        static IComponentDef* CreateDef(const std::string& type)
        {
            ((void)type);
            return new SpriteComponentDesc();
        }
    };

    RegisterGraphics2DComponentType registerGraphics2DComponent;
}

/*
 * SpriteComponentDesc
 */

cuteBEGIN_PROPERTY_IMPL(SpriteComponentDesc)
    cutePROPERTY_IMPL(std::string, FileName, true)
    cutePROPERTY_IMPL(Vector2, Size, true)
    cutePROPERTY_IMPL(Vector2, Origin, true)
    cutePROPERTY_IMPL(Rect, TextureRect, true)
    cutePROPERTY_IMPL(Color, Color, true)
cuteEND_PROPERTY_IMPL

SpriteComponentDesc::SpriteComponentDesc()
    : IComponentDef(Graphics2DComponentMgr::FamilyName, Sprite::TypeName)
{
    cutePREPARE_PROPERTIES();
}

SpriteComponentDesc::SpriteComponentDesc(const std::string& fileName, const Vector2& size, const Vector2& origin)
    : IComponentDef(Graphics2DComponentMgr::FamilyName, Sprite::TypeName)
{
    cutePREPARE_PROPERTIES();

    this->setFileName(fileName);
    this->setSize(size);
    this->setOrigin(origin);
}

void SpriteComponentDesc::configure(const json::ValueHandle &data)
{
    json::ValueHandle hSrc = data.childPair("FileName").value();
    this->setFileName(hSrc.string());

    json::ValueHandle hSize = data.childPair("Size").value();
    Vector2 size;
    size.x = hSize.childValue(0).number();
    size.y = hSize.childValue(1).number();
    this->setSize(size);

    json::ValueHandle hOrigin = data.childPair("Origin").value();
    Vector2 origin;
    origin.x = hOrigin.childValue(0).number();
    origin.y = hOrigin.childValue(1).number();
    this->setOrigin(origin);

    json::ValueHandle hRect = data.childPair("TextureRect").value();
    if (!hRect.isNull())
    {
        Rect rect;
        rect.x = hRect.childValue(0).number();
        rect.y = hRect.childValue(1).number();
        rect.width = hRect.childValue(2).number();
        rect.height = hRect.childValue(3).number();
        this->setTextureRect(rect);
    }
    else
    {
        this->setTextureRect(Rect(-1,-1,-1,-1));
    }

    json::ValueHandle hColor = data.childPair("Color").value();
    if (!hColor.isNull())
    {
        Color color;
        color.r = hColor.childValue(0).number();
        color.g = hColor.childValue(1).number();
        color.b = hColor.childValue(2).number();
        color.a = hColor.childValue(3).number();
        this->setColor(color);
    }
}

/*
 * SpriteComponent
 */

cuteBEGIN_PROPERTY_IMPL(Sprite)
    cutePROPERTY_IMPL(std::string, FileName, true)
    cutePROPERTY_IMPL(Vector2, Size, true)
    cutePROPERTY_IMPL(Vector2, Origin, true)
    cutePROPERTY_IMPL(Rect, TextureRect, true)
    cutePROPERTY_IMPL(Color, Color, true)
cuteEND_PROPERTY_IMPL

const std::string Sprite::TypeName = "Sprite";

Sprite::Sprite(Graphics2DComponentMgr* mgr, const SpriteComponentDesc* desc)
    : m_mgr(mgr), m_desc(desc), m_transform(nullptr)
{
    cutePREPARE_PROPERTIES();

    this->setTextureRect(desc->getTextureRect());
    this->setFileName(desc->getFileName());
    this->setSize(desc->getSize());
    this->setOrigin(desc->getOrigin());
    this->setColor(desc->getColor());
}

Sprite::~Sprite()
{
}

const std::string& Sprite::familyName() const
{
    return Graphics2DComponentMgr::FamilyName;
}

const std::string& Sprite::typeName() const
{
    return Sprite::TypeName;
}

void Sprite::initialize(Entity* ent)
{
    IComponent* _loc = ent->componentByType(Transform2D::TypeName);
    if(_loc)
        m_transform = (Transform2D*)_loc;
}

void Sprite::deinitialize()
{
    /*
     * Entity doesn't delete entities, it just deinitializes them.
     * Redundant entities are added to m_forDeletion in Graphics2DComponentMgr, and then deleted in Graphics2DComponentMgr::update
     */
    m_mgr->markForDeletion(this);
}

void Sprite::saveState(ConfStorage &conf) const
{
    cuteSAVE_STATE(conf, FileName, m_desc, FileName);
    cuteSAVE_STATE(conf, Size, m_desc, Size);
    cuteSAVE_STATE(conf, Origin, m_desc, Origin);
    cuteSAVE_STATE(conf, TextureRect, m_desc, TextureRect);
    cuteSAVE_STATE(conf, Color, m_desc, Color);
}

void Sprite::loadState(const ConfStorage &conf)
{
    this->IEditable::loadState(conf, TypeName+".");
}

int Sprite::layer() const
{
    if (m_transform)
        return m_transform->getLayer();

    return 0;
}

void Sprite::draw(int)
{
    if ((m_textureRect.x < 0.0f && m_textureRect.y < 0.0f) && (m_textureRect.width < 0.0f && m_textureRect.height < 0.0f))
    {
        Rect textureRect(0, 0, this->m_texture->width(), this->m_texture->height());
        this->setTextureRect(textureRect);
    }
    Vertex verts[6];
    Sprite::genVertex(verts, 0, m_transform->matrix(), m_origin, m_size,
                      Vector2(m_texture->width(), m_texture->height()), m_textureRect, m_color);

    SDL2Texture::bind(m_texture);

    glVertexPointer(2, GL_FLOAT, sizeof(Vertex), verts);
    glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Vertex), &verts[0].r);
    glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &verts[0].u);

    glDrawArrays(GL_TRIANGLES, 0, 6);

    SDL2Texture::bind(nullptr);
}

void Sprite::genVertex(Vertex* verts, int startIndex, const Matrix3x3& transform, const Vector2& origin,
                         const Vector2& size, const Vector2& textureSize, const Rect& textureRect,
                         const Color& color)
{
    Vector2 lowerR(textureRect.x / textureSize.x, textureRect.y / textureSize.y);
    Vector2 upperR((textureRect.x + textureRect.width) / textureSize.x, (textureRect.y + textureRect.height) / textureSize.y);

    Vector2 s = 0.5f * size;
    Vector2 pos[] =
    {
        transform * Vector2(origin.x - s.x, origin.y - s.y), // lower left
        transform * Vector2(origin.x + s.x, origin.y - s.y), // lower right
        transform * Vector2(origin.x - s.x, origin.y + s.y), // upper left
        transform * Vector2(origin.x + s.x, origin.y + s.y)  // upper right
    };

    assert(sizeof(verts) - startIndex < 6);

    int i = startIndex;

    // first triangle
    verts[i].x = pos[0].x;
    verts[i].y = pos[0].y;
    verts[i].r = color.r; verts[i].g = color.g; verts[i].b = color.b; verts[i].a = color.a;
    verts[i].u = lowerR.x;
    verts[i].v = upperR.y;
    ++i;

    verts[i].x = pos[1].x;
    verts[i].y = pos[1].y;
    verts[i].r = color.r; verts[i].g = color.g; verts[i].b = color.b; verts[i].a = color.a;
    verts[i].u = upperR.x;
    verts[i].v = upperR.y;
    ++i;

    verts[i].x = pos[2].x;
    verts[i].y = pos[2].y;
    verts[i].r = color.r; verts[i].g = color.g; verts[i].b = color.b; verts[i].a = color.a;
    verts[i].u = lowerR.x;
    verts[i].v = lowerR.y;
    ++i;

    // second triangle
    verts[i].x = pos[2].x;
    verts[i].y = pos[2].y;
    verts[i].r = color.r; verts[i].g = color.g; verts[i].b = color.b; verts[i].a = color.a;
    verts[i].u = lowerR.x;
    verts[i].v = lowerR.y;
    ++i;

    verts[i].x = pos[1].x;
    verts[i].y = pos[1].y;
    verts[i].r = color.r; verts[i].g = color.g; verts[i].b = color.b; verts[i].a = color.a;
    verts[i].u = upperR.x;
    verts[i].v = upperR.y;
    ++i;

    verts[i].x = pos[3].x;
    verts[i].y = pos[3].y;
    verts[i].r = color.r; verts[i].g = color.g; verts[i].b = color.b; verts[i].a = color.a;
    verts[i].u = upperR.x;
    verts[i].v = lowerR.y;
    ++i;
}

std::string Sprite::_setTexture(const std::string &fn)
{
    m_texture = m_mgr->resourceMgr().resource(fn);
    return m_texture->fileName();
}

std::string Sprite::_getTexture() const
{
    if(m_texture)
        return m_texture->fileName();
    return std::string();
}

void Sprite::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Sprite> _SpriteComponent;
    _SpriteComponent.Func("familyName", &Sprite::familyName);
    _SpriteComponent.Func("getFileName", &Sprite::getFileName);
    _SpriteComponent.Func("getOrigin", &Sprite::getOrigin);
    _SpriteComponent.Func("getSize", &Sprite::getSize);
    _SpriteComponent.Func("setFileName", &Sprite::setFileName);
    _SpriteComponent.Func("setOrigin", &Sprite::setOrigin);
    _SpriteComponent.Func("setSize", &Sprite::setSize);
    _SpriteComponent.Func("getTextureRect", &Sprite::getTextureRect);
    _SpriteComponent.Func("setTextureRect", &Sprite::setTextureRect);
    _SpriteComponent.Func("getColor", &Sprite::getColor);
    _SpriteComponent.Func("setColor", &Sprite::setColor);
    _SpriteComponent.Func("typeName", &Sprite::typeName);
    Sqrat::RootTable(vm).Bind("Sprite", _SpriteComponent);
}
