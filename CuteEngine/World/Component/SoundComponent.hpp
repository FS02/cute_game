#ifndef SOUNDCOMPONENT_HPP
#define SOUNDCOMPONENT_HPP

#include "../Entity.hpp"
#include "../GameWorld.hpp"
#include "../../Core/ResourceMgr.hpp"
#include <CuteEngine/World/Component/Transform2DComponent.hpp>
#include <SDL_mixer.h>
#include <vector>

namespace cute
{
    class SoundComponentMgr;

    /*!
     * \brief Sound resource.
     * Audio track that can be loaded from OGG, WAV, MP3, etc. files.
     */
    class SoundResource : public IResource
    {
        friend class SoundComponentMgr;

    public:
        //! Infinity. Can be passed to startLooped to play sound in an infinite loop.
        static constexpr int Infinity = -1;

        //! Constructor.
        SoundResource(const std::string& fileName);
        virtual ~SoundResource();

        //! Loads sound from a file.
        virtual bool load();

        /*!
         * \brief Starts playing the sound in a loop.
         * \param channel Channel in which the sound should be played. Pass -1 to play on first free channel.
         * \param times Number of sound iteration. Pass Infinity to play fo ever.
         * \return Index of channel in which the sound will be played, or -1 on error (e.g. there are no free channels).
         */
        int startLooped(int channel, int times);
        /*!
         * \brief Starts playing the sound for specified time.
         * \param channel Channel in which the sound should be played. Pass -1 to play on first free channel.
         * \param time Time for which the sound will be played. Must be greater than 0, otherwise function will return -1.
         * \return Index of channel in which the sound will be played, or -1 on error (e.g. there are no free channels).
         */
        int startTimed(int channel, double time);

        //! Allocates SoundResource. Used as callback function passed to SoundMgr.
        static SoundResource* Create(const std::string& fileName);

    private:
        Mix_Chunk* m_chunk;
    };

    typedef ResourceMgr<SoundResource> SoundMgr;

    /*!
     * \brief Description for SoundComponent.
     * familyType = "Sound"
     * typeName = "Sound"
     */
    class SoundComponentDesc : public IComponentDef
    {
        cuteEDITABLE
        cutePROPERTY(std::string, FileName, m_sndFileName)

    public:
        SoundComponentDesc();

        virtual void configure(const json::ValueHandle &data);

    private:
        std::string m_sndFileName;
    };

    /*!
     * \brief Sound component.
     * Add it to an entity to be able to play sounds.
     */
    class Sound : public IComponent
    {
        cuteEDITABLE
        cutePROPERTY_WITH_SETTER_AND_GETTER(std::string, FileName, _setSndFileName, _getSndFileName)

    public:
        static const std::string TypeName;

        Sound(SoundComponentMgr* mgr, const SoundComponentDesc* desc);

        //! Starts playing sound in a loop. See SoundResource::startLooped. One component can play sound once at a time.
        void startLooped(int times);
        //! Starts playing sound for given time. See SoundResource::startTimed. One component can play sound once at a time.
        void startTimed(double time);
        //! Stops playing sound.
        void stop();

        virtual const std::string& familyName() const;
        virtual const std::string& typeName() const;

        virtual void initialize(Entity* ent);
        virtual void deinitialize();

        virtual void saveState(ConfStorage &conf) const;
        virtual void loadState(const ConfStorage &conf);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        SoundComponentMgr* m_mgr;
        const SoundComponentDesc* m_desc;
        SoundResource* m_sound;
        int m_channel;
        Transform2D* m_transform;

        std::string _setSndFileName(const std::string& fn);
        std::string _getSndFileName() const;
    };

    /*!
     * \brief Manager for sound components.
     * Manages SoundComponent instances and SoundResource instances.
     */
    class SoundComponentMgr : public IComponentMgr
    {
    public:
        static const std::string FamilyName;

        SoundComponentMgr();
        virtual ~SoundComponentMgr();

        virtual const std::string& familyName() const;

        virtual void initialize(GameWorld* world);
        virtual void deinitialize();

        virtual IComponent* createComponent(const IComponentDef* desc);
        virtual void update(double deltaTime);

        //! Returns manager of SoundResource objects.
        SoundMgr& resourceMgr();
        //! Marks component for deletion. Don't call it yourseld, SoundComponents will call this method when necessary.
        void markForDeletion(Sound* comp);

    private:
        SoundMgr m_sounds;
        std::list<Sound*> m_forDeletion;
    };
}

#endif // SOUNDCOMPONENT_HPP
