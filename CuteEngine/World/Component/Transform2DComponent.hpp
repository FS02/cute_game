#ifndef LOCATIONCOMPONENT_HPP
#define LOCATIONCOMPONENT_HPP

#include <CuteEngine/World/GameWorld.hpp>
#include <CuteEngine/Utils/Vector2.hpp>
#include <CuteEngine/Utils/Matrix3x3.hpp>
#include <list>
#include <Box2D.h>

namespace cute
{
    /*!
     * \brief Description for LocationComponent, stores data loaded from JSON files.
     * Rotation is specified in radians.
     */
    class Transform2DCompDesc : public IComponentDef
    {
        cuteEDITABLE
        cutePROPERTY(cute::Vector2, Position, m_position)
        cutePROPERTY(float, Rotation, m_rotation)
        cutePROPERTY(float, Layer, m_layer)

    public:
        Transform2DCompDesc(const cute::Vector2& pos = cute::Vector2(), float rot = 0.0f);

        virtual void configure(const json::ValueHandle &data);

    private:
        cute::Vector2 m_position;
        float m_rotation;
        int m_layer;
    };

    class TransformCompMgr;

    /*!
     * \brief Position and rotation of an entity.
     * You can use it to set position and rotation of PhysicsComponent, if it is attached to the owner.
     */
    class Transform2D : public IComponent
    {
        cuteEDITABLE
        cutePROPERTY_WITH_SETTER_AND_GETTER(cute::Vector2, Position, _setPosition, _getPosition)
        cutePROPERTY_WITH_SETTER_AND_GETTER(float, Rotation, _setRotation, _getRotation)
        cutePROPERTY(float, Layer, m_layer)
        friend class PhysicsComponent;

    public:
        static const std::string TypeName;

        Transform2D(TransformCompMgr* mgr, const Transform2DCompDesc* desc);

        virtual const std::string& familyName() const;
        virtual const std::string& typeName() const;

        //! Does nothing.
        virtual void initialize(Entity* ent);
        //! Does nothing.
        virtual void deinitialize();

        virtual void saveState(ConfStorage &conf) const;
        virtual void loadState(const ConfStorage &conf);

        unsigned int lastChanageId() const;

        Matrix3x3 matrix();

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        TransformCompMgr* m_mgr;
        const Transform2DCompDesc* m_desc;
        cute::Vector2 m_position;
        float m_rotation;
        unsigned int m_lastChangeId;
        int m_layer;

        cute::Vector2 _setPosition(const cute::Vector2& pos);
        cute::Vector2 _getPosition() const;
        float _setRotation(float rot);
        float _getRotation() const;
    };

    /*!
     * \brief Manager for LocationComponent objects.
     */
    class TransformCompMgr : public IComponentMgr
    {
    public:
        static const std::string FamilyName;

        virtual const std::string& familyName() const;

        virtual void initialize(GameWorld* world);
        virtual void deinitialize();

        virtual IComponent* createComponent(const IComponentDef* desc);
        virtual void update(double deltaTime);

        //! Marks a component for deletion when it will be safe. Do not call directly.
        void markForDeletion(Transform2D* comp);

    private:
        std::vector<Transform2D*> m_comps;
        std::list<Transform2D*> m_forDeletion;
    };
}

#endif // LOCATIONCOMPONENT_HPP
