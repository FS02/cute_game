#include "Transform2DComponent.hpp"
#include "../../Utils/Time.hpp"
#include <CuteEngine/World/ComponentDefFactory.hpp>

using namespace cute;

/*
 * RegisterLocationComponent is a trick add factory function for LocationCompDesc before entering "main" function.
 * Construcor registers the function.
 * Ctor is called for global varibale called registerLocationComponent before application enters main.
 * Use anonymouse namespaces for such "local" (used only inside one cpp file) classes and variables.
 */
namespace
{
    struct RegisterLocationComponent
    {
        RegisterLocationComponent()
        {
            std::string family = "Transform";
            std::string type = "Transform2D";
            IComponentDefCreator* creator = new StdComponentDefCreator(family, type, &CreateDef);
            ComponentDefFactory::Instance().addType(creator);
        }

        static IComponentDef* CreateDef(const std::string& type)
        {
            ((void)type); // unused argument
            return new Transform2DCompDesc();
        }
    };

    RegisterLocationComponent registerLocationComponent;
}

using namespace cute;

/*
 * Transform2DCompDesc
 */

cuteBEGIN_PROPERTY_IMPL(Transform2DCompDesc)
    cutePROPERTY_IMPL(Vector2, Position, true)
    cutePROPERTY_IMPL(float, Rotation, true)
    cutePROPERTY_IMPL(float, Layer, true)
cuteEND_PROPERTY_IMPL

Transform2DCompDesc::Transform2DCompDesc(const Vector2& pos, float rot)
    : IComponentDef(TransformCompMgr::FamilyName, Transform2D::TypeName)
{
    cutePREPARE_PROPERTIES();

    this->setPosition(pos);
    this->setRotation(rot);
}

void Transform2DCompDesc::configure(const json::ValueHandle &data)
{
    json::ValueHandle hPos = data.childPair("Position").value();
    if (!hPos.isNull())
    {
        Vector2 position;
        position.x = hPos.childValue(0).number();
        position.y = hPos.childValue(1).number();
        this->setPosition(position);
    }

    json::ValueHandle hRot = data.childPair("Rotation").value();
    this->setRotation(hRot.number());

    json::ValueHandle hLayer = data.childPair("Layer").value();
    this->setLayer(hLayer.number());
}

/*
 * Transform2DComponent
 */

cuteBEGIN_PROPERTY_IMPL(Transform2D)
    cutePROPERTY_IMPL(Vector2, Position, true)
    cutePROPERTY_IMPL(float, Rotation, true)
    cutePROPERTY_IMPL(float, Layer, true)
cuteEND_PROPERTY_IMPL

const std::string Transform2D::TypeName = "Transform2D";

Transform2D::Transform2D(TransformCompMgr *mgr, const Transform2DCompDesc* desc)
    : m_mgr(mgr), m_desc(desc), m_lastChangeId(0)
{
    cutePREPARE_PROPERTIES();

    this->setPosition(m_desc->getPosition());
    this->setRotation(m_desc->getRotation());
    this->setLayer(m_desc->getLayer());
}

const std::string& Transform2D::familyName() const
{
    return TransformCompMgr::FamilyName;
}

const std::string& Transform2D::typeName() const
{
    return TypeName;
}

void Transform2D::initialize(Entity *ent)
{
    ((void)ent);
}

void Transform2D::deinitialize()
{
    m_mgr->markForDeletion(this);
}

void Transform2D::saveState(ConfStorage &conf) const
{
    cuteSAVE_STATE(conf, Position, m_desc, Position);
    cuteSAVE_STATE(conf, Rotation, m_desc, Rotation);
    cuteSAVE_STATE(conf, Layer, m_desc, Layer);
}

void Transform2D::loadState(const ConfStorage &conf)
{
    this->IEditable::loadState(conf, TypeName+".");
}

unsigned int Transform2D::lastChanageId() const
{
    return m_lastChangeId;
}

Matrix3x3 Transform2D::matrix()
{
    Matrix3x3 m;
    m.setRotation(m_rotation);
    m.translate(m_position);
    return m;
}

Vector2 Transform2D::_setPosition(const Vector2 &pos)
{
    m_position = pos;
    ++m_lastChangeId;
    return m_position;
}

Vector2 Transform2D::_getPosition() const
{
    return m_position;
}

float Transform2D::_setRotation(float rot)
{
    m_rotation = rot;
    ++m_lastChangeId;
    return m_rotation;
}

float Transform2D::_getRotation() const
{
    return m_rotation;
}

void Transform2D::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Transform2D> _Transform2D;
    _Transform2D.Func("familyName", &Transform2D::familyName);
    _Transform2D.Func("getPosition", &Transform2D::getPosition);
    _Transform2D.Func("getRotation", &Transform2D::getRotation);
    _Transform2D.Func("setPosition", &Transform2D::setPosition);
    _Transform2D.Func("setRotation", &Transform2D::setRotation);
    _Transform2D.Func("setLayer", &Transform2D::setLayer);
    _Transform2D.Func("getLayer", &Transform2D::getLayer);
    _Transform2D.Func("typeName", &Transform2D::typeName);
    Sqrat::RootTable(vm).Bind("Transform", _Transform2D);
}

/*
 * Transform2DCompMgr
 */

const std::string TransformCompMgr::FamilyName = "Transform";

const std::string& TransformCompMgr::familyName() const
{
    return FamilyName;
}

void TransformCompMgr::initialize(GameWorld *world)
{
    ((void)world);
}

void TransformCompMgr::deinitialize()
{
    for(auto iter = m_forDeletion.begin(); iter != m_forDeletion.end(); ++iter)
        delete (*iter);
    m_forDeletion.clear();
}

IComponent* TransformCompMgr::createComponent(const IComponentDef *desc)
{
    Transform2DCompDesc* cd = (Transform2DCompDesc*)desc;
    return new Transform2D(this, cd);
}

void TransformCompMgr::update(double deltaTime)
{
    ((void)deltaTime);

    for(auto iter = m_forDeletion.begin(); iter != m_forDeletion.end(); ++iter)
        delete (*iter);
    m_forDeletion.clear();
}

void TransformCompMgr::markForDeletion(Transform2D *comp)
{
    m_forDeletion.push_back(comp);
}
