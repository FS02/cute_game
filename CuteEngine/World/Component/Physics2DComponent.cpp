#include <CuteEngine/World/Component/Physics2DComponent.hpp>
#include <CuteEngine/World/Component/Transform2DComponent.hpp>
#include <CuteEngine/World/ComponentDefFactory.hpp>
#include <fstream>
#include <cassert>

using namespace cute;

// see LocationComponent.cpp
namespace
{
    struct RegisterPhysicsComp
    {
        RegisterPhysicsComp()
        {
            std::string family = "Physics2D";
            std::string type = "Physics2D";
            IComponentDefCreator* creator = new StdComponentDefCreator(family, type, &CreateDef);
            ComponentDefFactory::Instance().addType(creator);
        }

        static Physics2DCompDesc* CreateDef(const std::string&)
        {
            return new Physics2DCompDesc();
        }
    };

    RegisterPhysicsComp registerPhysicsComp;
}

/*
 * RayCastCallback
 */
CERayCastCallback::CERayCastCallback(float fraction)
    : m_fraction(fraction), m_ent(nullptr), m_fixture(nullptr)
{ }

float32 CERayCastCallback::ReportFixture(b2Fixture *fix, const b2Vec2 &pos, const b2Vec2 &norm, float32 frac)
{
    (void)frac;
    m_fixture = fix;
    m_position = pos;
    m_normal = norm;
    --m_fraction;
    if (m_fixture && m_fraction == 0)
        m_ent = ((Physics2D*)m_fixture->GetBody()->GetUserData())->owner();

    return m_fraction;
}

bool CERayCastCallback::hit() const
{
    return (m_fixture != nullptr);
}

Entity* CERayCastCallback::entity()
{
    return m_ent;
}

b2Fixture* CERayCastCallback::hitFixture()
{
    return m_fixture;
}

Vector2 CERayCastCallback::hitPosition() const
{
    return Vector2(m_position.x, m_position.y);
}

Vector2 CERayCastCallback::hitNormal() const
{
    return Vector2(m_normal.x, m_normal.y);
}

void CERayCastCallback::clear()
{
    m_fraction = 0;
    m_ent = nullptr;
    m_fixture = nullptr;
    m_position.SetZero();
    m_normal.SetZero();
}

/*
 * CEQueryCallback
 */
bool CEQueryCallback::ReportFixture(b2Fixture *fixture)
{
    m_entities.insert(((Physics2D*)fixture->GetBody()->GetUserData())->owner());
    return true;
}

const std::unordered_set<Entity*>& CEQueryCallback::entities() const
{
    return m_entities;
}

void CEQueryCallback::clear()
{
    m_entities.clear();
}

/*
 * Skeleton
 */

std::string Skeleton::path = "Data/Skeletons/";

Skeleton::Skeleton(b2World *world, const std::string &cesfile)
    : m_world(world), m_mainBody(nullptr)
{
    assert(this->create(cesfile));
}

Skeleton::~Skeleton()
{
    clear();
}

bool Skeleton::create(const std::string &cesfile)
{
    this->clear();

    json::Document doc;

    std::ifstream file((path + cesfile).c_str());
    json::StdTokenStream tokenStr(file);
    json::StdErrorCallback errorCb(std::clog, cesfile);
    if(!doc.load(&tokenStr, &errorCb))
        return false;

    std::map<std::string, b2Shape*> shapes;
    std::map<std::string, b2FixtureDef*> fixtures;

    json::ValueHandle hShapes = doc.root()->child("Shapes")->value;
    if (!hShapes.isNull())  createShapes(const_cast<Skeleton*>(this), shapes, hShapes);

    json::ValueHandle hFixtures = doc.root()->child("Fixtures")->value;
    if (!hFixtures.isNull()) createFixtures(const_cast<Skeleton*>(this), shapes, fixtures, hFixtures);

    json::ValueHandle hBodies = doc.root()->child("Bodies")->value;
    if (!hBodies.isNull()) createBodies(const_cast<Skeleton*>(this), fixtures, hBodies);

    json::ValueHandle hJoints = doc.root()->child("Joints")->value;
    if (!hJoints.isNull()) createJoints(const_cast<Skeleton*>(this), hJoints);

    for(auto iter = shapes.begin(); iter != shapes.end(); ++iter)
        delete iter->second;
    shapes.clear();

    for(auto iter = fixtures.begin(); iter != fixtures.end(); ++iter)
        delete iter->second;
    fixtures.clear();

    return true;
}

void Skeleton::clear()
{
    m_mainBody = nullptr;

    for(auto iter = m_joints.begin(); iter != m_joints.end(); ++iter)
        m_world->DestroyJoint(iter->second);
    m_joints.clear();

    for(auto iter = m_bodies.begin(); iter != m_bodies.end(); ++iter)
        m_world->DestroyBody(iter->second);
    m_bodies.clear();
}

void Skeleton::setUserData(void *data)
{
    for(auto iter = m_joints.begin(); iter != m_joints.end(); ++iter)
        iter->second->SetUserData(data);

    for(auto iter = m_bodies.begin(); iter != m_bodies.end(); ++iter)
        iter->second->SetUserData(data);
}

void* Skeleton::getUserData() const
{
    return m_mainBody->GetUserData();
}

void Skeleton::setTransform(const Vector2 &pos, float rot)
{
    for(auto iter = m_bodies.begin(); iter != m_bodies.end(); ++iter)
        iter->second->SetTransform(pos, rot);
}

const Vector2& Skeleton::getPosition() const
{
    return m_mainBody->GetPosition();
}

float Skeleton::getRotation() const
{
    return m_mainBody->GetAngle();
}

b2Body* Skeleton::mainBody()
{
    return m_mainBody;
}

b2Body* Skeleton::body(const std::string &id)
{
    if (m_bodies.find(id) == m_bodies.end())
        return nullptr;

    return m_bodies[id];
}

b2Joint* Skeleton::joint(const std::string &id)
{
    if (m_joints.find(id) == m_joints.end())
        return nullptr;

    return m_joints[id];
}

b2RevoluteJoint* Skeleton::revoluteJoint(const std::string &id)
{
    return static_cast<b2RevoluteJoint*>(joint(id));
}

b2DistanceJoint* Skeleton::distanceJoint(const std::string &id)
{
    return static_cast<b2DistanceJoint*>(joint(id));
}

b2PrismaticJoint* Skeleton::prismaticJoint(const std::string &id)
{
    return static_cast<b2PrismaticJoint*>(joint(id));
}

b2PulleyJoint* Skeleton::pulleyJoint(const std::string &id)
{
    return static_cast<b2PulleyJoint*>(joint(id));
}

b2GearJoint* Skeleton::gearJoint(const std::string &id)
{
    return static_cast<b2GearJoint*>(joint(id));
}

b2MouseJoint* Skeleton::mouseJoint(const std::string &id)
{
    return static_cast<b2MouseJoint*>(joint(id));
}

b2WheelJoint* Skeleton::wheelJoint(const std::string &id)
{
    return static_cast<b2WheelJoint*>(joint(id));
}

b2WeldJoint* Skeleton::weldJoint(const std::string &id)
{
    return static_cast<b2WeldJoint*>(joint(id));
}

b2RopeJoint* Skeleton::ropeJoint(const std::string &id)
{
    return static_cast<b2RopeJoint*>(joint(id));
}

b2FrictionJoint* Skeleton::frictionJoint(const std::string &id)
{
    return static_cast<b2FrictionJoint*>(joint(id));
}

void Skeleton::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<b2Body, Sqrat::NoConstructor> _body;
    _body.Func("applyAngularImpulse", &b2Body::ApplyAngularImpulse);
    _body.Func("applyForce", &b2Body::ApplyForce);
    _body.Func("applyForceToCenter", &b2Body::ApplyForceToCenter);
    _body.Func("applyLinearImpulse", &b2Body::ApplyLinearImpulse);
    _body.Func("applyTorque", &b2Body::ApplyTorque);
    _body.Func("getAngle", &b2Body::GetAngle);
    _body.Func("getAngularDamping", &b2Body::GetAngularDamping);
    _body.Func("getAngularVelocity", &b2Body::GetAngularVelocity);
    _body.Func("getGravityScale", &b2Body::GetGravityScale);
    _body.Func("getInertia", &b2Body::GetInertia);
    _body.Func("getLinearDamping", &b2Body::GetLinearDamping);
    _body.Func("getLinearVelocity", &b2Body::GetLinearVelocity);
    _body.Func("getLinearVelocityFromLocalPoint", &b2Body::GetLinearVelocityFromLocalPoint);
    _body.Func("getLinearVelocityFromWorldPoint", &b2Body::GetLinearVelocityFromWorldPoint);
    _body.Func("getLocalCenter", &b2Body::GetLocalCenter);
    _body.Func("getLocalPoint", &b2Body::GetLocalPoint);
    _body.Func("getLocalVector", &b2Body::GetLocalVector);
    _body.Func("getMass", &b2Body::GetMass);
    _body.Func("getPosition", &b2Body::GetPosition);
    _body.Func("getWorldCenter", &b2Body::GetWorldCenter);
    _body.Func("getWorldPoint", &b2Body::GetWorldPoint);
    _body.Func("getWorldVector", &b2Body::GetWorldVector);
    _body.Func("isActive", &b2Body::IsActive);
    _body.Func("isAwake", &b2Body::IsAwake);
    _body.Func("isBullet", &b2Body::IsBullet);
    _body.Func("isFixedRotation", &b2Body::IsFixedRotation);
    _body.Func("isSleepingAllowed", &b2Body::IsSleepingAllowed);
    _body.Func("setActive", &b2Body::SetActive);
    _body.Func("setAngularDamping", &b2Body::SetAngularDamping);
    _body.Func("setAngularVelocity", &b2Body::SetAngularVelocity);
    _body.Func("setAwake", &b2Body::SetAwake);
    _body.Func("setBullet", &b2Body::SetBullet);
    _body.Func("setFixedRotation", &b2Body::SetFixedRotation);
    _body.Func("setGravityScale", &b2Body::SetGravityScale);
    _body.Func("setLinearDamping", &b2Body::SetLinearDamping);
    _body.Func("setLinearVelocity", &b2Body::SetLinearVelocity);
    Sqrat::RootTable(vm).Bind("b2Body", _body);

    Sqrat::Class<b2Joint, Sqrat::NoConstructor> _joint;
    _joint.Func("getAnchorA", &b2Joint::GetAnchorA);
    _joint.Func("getAnchorB", &b2Joint::GetAnchorB);
    _joint.Func("getBodyA", &b2Joint::GetBodyA);
    _joint.Func("getBodyB", &b2Joint::GetBodyB);
    _joint.Func("getCollideConnected", &b2Joint::GetCollideConnected);
    _joint.Func("getReactionForce", &b2Joint::GetReactionForce);
    _joint.Func("getReactionTorque", &b2Joint::GetReactionTorque);
    _joint.Func("isActive", &b2Joint::IsActive);
    Sqrat::RootTable(vm).Bind("b2Joint", _joint);

    Sqrat::Class<b2RevoluteJoint, Sqrat::NoConstructor> _revoluteJoint;
    _revoluteJoint.Func("enableLimit", &b2RevoluteJoint::EnableLimit);
    _revoluteJoint.Func("enableMotor", &b2RevoluteJoint::EnableMotor);
    _revoluteJoint.Func("getAnchorA", &b2RevoluteJoint::GetAnchorA);
    _revoluteJoint.Func("getAnchorB", &b2RevoluteJoint::GetAnchorB);
    _revoluteJoint.Func("getBodyA", &b2RevoluteJoint::GetBodyA);
    _revoluteJoint.Func("getBodyB", &b2RevoluteJoint::GetBodyB);
    _revoluteJoint.Func("getCollideConnected", &b2RevoluteJoint::GetCollideConnected);
    _revoluteJoint.Func("getJointAngle", &b2RevoluteJoint::GetJointAngle);
    _revoluteJoint.Func("getJointSpeed", &b2RevoluteJoint::GetJointSpeed);
    _revoluteJoint.Func("getLocalAnchorA", &b2RevoluteJoint::GetLocalAnchorA);
    _revoluteJoint.Func("getLocalAnchorB", &b2RevoluteJoint::GetLocalAnchorB);
    _revoluteJoint.Func("getLowerLimit", &b2RevoluteJoint::GetLowerLimit);
    _revoluteJoint.Func("getMaxMotorTorque", &b2RevoluteJoint::GetMaxMotorTorque);
    _revoluteJoint.Func("getMotorSpeed", &b2RevoluteJoint::GetMotorSpeed);
    _revoluteJoint.Func("getMotorTorque", &b2RevoluteJoint::GetMotorTorque);
    _revoluteJoint.Func("getReactionForce", &b2RevoluteJoint::GetReactionForce);
    _revoluteJoint.Func("getReactionTorque", &b2RevoluteJoint::GetReactionTorque);
    _revoluteJoint.Func("getReferenceAngle", &b2RevoluteJoint::GetReferenceAngle);
    _revoluteJoint.Func("getUpperLimit", &b2RevoluteJoint::GetUpperLimit);
    _revoluteJoint.Func("isActive", &b2RevoluteJoint::IsActive);
    _revoluteJoint.Func("isLimitEnabled", &b2RevoluteJoint::IsLimitEnabled);
    _revoluteJoint.Func("isMotorEnabled", &b2RevoluteJoint::IsMotorEnabled);
    _revoluteJoint.Func("setLimits", &b2RevoluteJoint::SetLimits);
    _revoluteJoint.Func("setMaxMotorTorque", &b2RevoluteJoint::SetMaxMotorTorque);
    _revoluteJoint.Func("setMotorSpeed", &b2RevoluteJoint::SetMotorSpeed);
    Sqrat::RootTable(vm).Bind("b2RevoluteJoint", _revoluteJoint);

    Sqrat::Class<b2DistanceJoint, Sqrat::NoConstructor> _distanceJoint;
    _distanceJoint.Func("getAnchorA", &b2DistanceJoint::GetAnchorA);
    _distanceJoint.Func("getAnchorB", &b2DistanceJoint::GetAnchorB);
    _distanceJoint.Func("getBodyA", &b2DistanceJoint::GetBodyA);
    _distanceJoint.Func("getBodyB", &b2DistanceJoint::GetBodyB);
    _distanceJoint.Func("getCollideConnected", &b2DistanceJoint::GetCollideConnected);
    _distanceJoint.Func("getDampingRatio", &b2DistanceJoint::GetDampingRatio);
    _distanceJoint.Func("getFrequency", &b2DistanceJoint::GetFrequency);
    _distanceJoint.Func("getLength", &b2DistanceJoint::GetLength);
    _distanceJoint.Func("getLocalAnchorA", &b2DistanceJoint::GetLocalAnchorA);
    _distanceJoint.Func("getLocalAnchorB", &b2DistanceJoint::GetLocalAnchorB);
    _distanceJoint.Func("getReactionForce", &b2DistanceJoint::GetReactionForce);
    _distanceJoint.Func("getReactionTorque", &b2DistanceJoint::GetReactionTorque);
    _distanceJoint.Func("isActive", &b2DistanceJoint::IsActive);
    _distanceJoint.Func("setDampingRatio", &b2DistanceJoint::SetDampingRatio);
    _distanceJoint.Func("setFrequency", &b2DistanceJoint::SetFrequency);
    _distanceJoint.Func("setLength", &b2DistanceJoint::SetLength);
    Sqrat::RootTable(vm).Bind("b2DistanceJoint", _distanceJoint);

    Sqrat::Class<b2PrismaticJoint, Sqrat::NoConstructor> _prismaticJoint;
    _prismaticJoint.Func("nableLimit", &b2PrismaticJoint::EnableLimit);
    _prismaticJoint.Func("enableMotor", &b2PrismaticJoint::EnableMotor);
    _prismaticJoint.Func("isActive", &b2PrismaticJoint::IsActive);
    _prismaticJoint.Func("isLimitEnabled", &b2PrismaticJoint::IsLimitEnabled);
    _prismaticJoint.Func("isMotorEnabled", &b2PrismaticJoint::IsMotorEnabled);
    _prismaticJoint.Func("getAnchorA", &b2PrismaticJoint::GetAnchorA);
    _prismaticJoint.Func("getAnchorB", &b2PrismaticJoint::GetAnchorB);
    _prismaticJoint.Func("getBodyA", &b2PrismaticJoint::GetBodyA);
    _prismaticJoint.Func("getBodyB", &b2PrismaticJoint::GetBodyB);
    _prismaticJoint.Func("getCollideConnected", &b2PrismaticJoint::GetCollideConnected);
    _prismaticJoint.Func("getJointSpeed", &b2PrismaticJoint::GetJointSpeed);
    _prismaticJoint.Func("getJointTranslation", &b2PrismaticJoint::GetJointTranslation);
    _prismaticJoint.Func("getLocalAnchorA", &b2PrismaticJoint::GetLocalAnchorA);
    _prismaticJoint.Func("getLocalAnchorB", &b2PrismaticJoint::GetLocalAnchorB);
    _prismaticJoint.Func("getLocalAxisA", &b2PrismaticJoint::GetLocalAxisA);
    _prismaticJoint.Func("getLowerLimit", &b2PrismaticJoint::GetLowerLimit);
    _prismaticJoint.Func("getMaxMotorForce", &b2PrismaticJoint::GetMaxMotorForce);
    _prismaticJoint.Func("getMotorForce", &b2PrismaticJoint::GetMotorForce);
    _prismaticJoint.Func("getMotorSpeed", &b2PrismaticJoint::GetMotorSpeed);
    _prismaticJoint.Func("getReactionForce", &b2PrismaticJoint::GetReactionForce);
    _prismaticJoint.Func("getReactionTorque", &b2PrismaticJoint::GetReactionTorque);
    _prismaticJoint.Func("getReference", &b2PrismaticJoint::GetReferenceAngle);
    _prismaticJoint.Func("getUpperLimit", &b2PrismaticJoint::GetUpperLimit);
    Sqrat::RootTable(vm).Bind("b2PrismaticJoint", _prismaticJoint);

    Sqrat::Class<b2PulleyJoint, Sqrat::NoConstructor> _pulleyJoint;
    _pulleyJoint.Func("getAnchorA", &b2PulleyJoint::GetAnchorA);
    _pulleyJoint.Func("getAnchorB", &b2PulleyJoint::GetAnchorB);
    _pulleyJoint.Func("getBodyA", &b2PulleyJoint::GetBodyA);
    _pulleyJoint.Func("getBodyB", &b2PulleyJoint::GetBodyB);
    _pulleyJoint.Func("getCollideConnected", &b2PulleyJoint::GetCollideConnected);
    _pulleyJoint.Func("getGroundAnchorA", &b2PulleyJoint::GetGroundAnchorA);
    _pulleyJoint.Func("getGroundAnchorB", &b2PulleyJoint::GetGroundAnchorB);
    _pulleyJoint.Func("getLengthB", &b2PulleyJoint::GetLengthA);
    _pulleyJoint.Func("getLengthA", &b2PulleyJoint::GetLengthB);
    _pulleyJoint.Func("getRatio", &b2PulleyJoint::GetRatio);
    _pulleyJoint.Func("getReactionForce", &b2PulleyJoint::GetReactionForce);
    _pulleyJoint.Func("getReactionTorque", &b2PulleyJoint::GetReactionTorque);
    _pulleyJoint.Func("isActive", &b2PulleyJoint::IsActive);
    Sqrat::RootTable(vm).Bind("b2PulleyJoint", _pulleyJoint);

    Sqrat::Class<b2GearJoint, Sqrat::NoConstructor> _gearJoint;
    _gearJoint.Func("getAnchorA", &b2GearJoint::GetAnchorA);
    _gearJoint.Func("getAnchorB", &b2GearJoint::GetAnchorB);
    _gearJoint.Func("getBodyA", &b2GearJoint::GetBodyA);
    _gearJoint.Func("getBodyB", &b2GearJoint::GetBodyB);
    _gearJoint.Func("getCollideConnected", &b2GearJoint::GetCollideConnected);
    _gearJoint.Func("getJoint1", &b2GearJoint::GetJoint1);
    _gearJoint.Func("getJoint2", &b2GearJoint::GetJoint2);
    _gearJoint.Func("getRatio", &b2GearJoint::GetRatio);
    _gearJoint.Func("getReactionForce", &b2GearJoint::GetReactionForce);
    _gearJoint.Func("getReactionTorque", &b2GearJoint::GetReactionTorque);
    _gearJoint.Func("isActive", &b2GearJoint::IsActive);
    _gearJoint.Func("setRatio", &b2GearJoint::SetRatio);
    Sqrat::RootTable(vm).Bind("b2GearJoint", _gearJoint);

    Sqrat::Class<b2MouseJoint, Sqrat::NoConstructor> _mouseJoint;
    _mouseJoint.Func("getAnchorA", &b2MouseJoint::GetAnchorA);
    _mouseJoint.Func("getAnchorB", &b2MouseJoint::GetAnchorB);
    _mouseJoint.Func("getBodyA", &b2MouseJoint::GetBodyA);
    _mouseJoint.Func("getBodyB", &b2MouseJoint::GetBodyB);
    _mouseJoint.Func("getCollideConnected", &b2MouseJoint::GetCollideConnected);
    _mouseJoint.Func("getDampingRatio", &b2MouseJoint::GetDampingRatio);
    _mouseJoint.Func("getFrequency", &b2MouseJoint::GetFrequency);
    _mouseJoint.Func("getMaxForce", &b2MouseJoint::GetMaxForce);
    _mouseJoint.Func("getReactionForce", &b2MouseJoint::GetReactionForce);
    _mouseJoint.Func("getReactionTorque", &b2MouseJoint::GetReactionTorque);
    _mouseJoint.Func("getTarget", &b2MouseJoint::GetTarget);
    _mouseJoint.Func("isActive", &b2MouseJoint::IsActive);
    _mouseJoint.Func("setDampingRatio", &b2MouseJoint::SetDampingRatio);
    _mouseJoint.Func("setFrequency", &b2MouseJoint::SetFrequency);
    _mouseJoint.Func("setMaxForce", &b2MouseJoint::SetMaxForce);
    _mouseJoint.Func("setTarget", &b2MouseJoint::SetTarget);
    Sqrat::RootTable(vm).Bind("b2MouseJoint", _mouseJoint);

    Sqrat::Class<b2WheelJoint, Sqrat::NoConstructor> _wheelJoint;
    _wheelJoint.Func("enableMotor", &b2WheelJoint::EnableMotor);
    _wheelJoint.Func("getAnchorA", &b2WheelJoint::GetAnchorA);
    _wheelJoint.Func("getAnchorB", &b2WheelJoint::GetAnchorB);
    _wheelJoint.Func("getBodyA", &b2WheelJoint::GetBodyA);
    _wheelJoint.Func("getBodyB", &b2WheelJoint::GetBodyB);
    _wheelJoint.Func("getCollideConnected", &b2WheelJoint::GetCollideConnected);
    _wheelJoint.Func("getJointSpeed", &b2WheelJoint::GetJointSpeed);
    _wheelJoint.Func("getJointTranslation", &b2WheelJoint::GetJointTranslation);
    _wheelJoint.Func("getLocalAnchorA", &b2WheelJoint::GetLocalAnchorA);
    _wheelJoint.Func("getLocalAnchorB", &b2WheelJoint::GetLocalAnchorB);
    _wheelJoint.Func("getLocalAxisA", &b2WheelJoint::GetLocalAxisA);
    _wheelJoint.Func("getMaxMotorTorque", &b2WheelJoint::GetMaxMotorTorque);
    _wheelJoint.Func("getMotorSpeed", &b2WheelJoint::GetMotorSpeed);
    _wheelJoint.Func("getMotorTorque", &b2WheelJoint::GetMotorTorque);
    _wheelJoint.Func("getReactionForce", &b2WheelJoint::GetReactionForce);
    _wheelJoint.Func("getReactionTorque", &b2WheelJoint::GetReactionTorque);
    _wheelJoint.Func("getSpringDampingRatio", &b2WheelJoint::GetSpringDampingRatio);
    _wheelJoint.Func("getSpringFrequencyHz", &b2WheelJoint::GetSpringFrequencyHz);
    _wheelJoint.Func("isActive", &b2WheelJoint::IsActive);
    _wheelJoint.Func("isMotorEnabled", &b2WheelJoint::IsMotorEnabled);
    _wheelJoint.Func("setMaxMotorTorque", &b2WheelJoint::SetMaxMotorTorque);
    _wheelJoint.Func("setMotorSpeed", &b2WheelJoint::SetMotorSpeed);
    _wheelJoint.Func("setSpringDampingRatio", &b2WheelJoint::SetSpringDampingRatio);
    _wheelJoint.Func("setSpringFrequencyHz", &b2WheelJoint::SetSpringFrequencyHz);
    Sqrat::RootTable(vm).Bind("b2WheelJoint", _wheelJoint);

    Sqrat::Class<b2WeldJoint, Sqrat::NoConstructor> _weldJoint;
    _weldJoint.Func("getAnchorA", &b2WeldJoint::GetAnchorA);
    _weldJoint.Func("getAnchorB", &b2WeldJoint::GetAnchorB);
    _weldJoint.Func("getBodyA", &b2WeldJoint::GetBodyA);
    _weldJoint.Func("getBodyB", &b2WeldJoint::GetBodyB);
    _weldJoint.Func("getCollideConnected", &b2WeldJoint::GetCollideConnected);
    _weldJoint.Func("getDampingRatio", &b2WeldJoint::GetDampingRatio);
    _weldJoint.Func("getFrequency", &b2WeldJoint::GetFrequency);
    _weldJoint.Func("getLocalAnchorA", &b2WeldJoint::GetLocalAnchorA);
    _weldJoint.Func("getLocalAnchorB", &b2WeldJoint::GetLocalAnchorB);
    _weldJoint.Func("getReactionForce", &b2WeldJoint::GetReactionForce);
    _weldJoint.Func("getReactionTorque", &b2WeldJoint::GetReactionTorque);
    _weldJoint.Func("getReferenceAngle", &b2WeldJoint::GetReferenceAngle);
    _weldJoint.Func("isActive", &b2WeldJoint::IsActive);
    _weldJoint.Func("setDampingRatio", &b2WeldJoint::SetDampingRatio);
    Sqrat::RootTable(vm).Bind("b2WeldJoint", _weldJoint);

    Sqrat::Class<b2RopeJoint, Sqrat::NoConstructor> _ropeJoint;
    _ropeJoint.Func("getAnchorA", &b2RopeJoint::GetAnchorA);
    _ropeJoint.Func("getAnchorB", &b2RopeJoint::GetAnchorB);
    _ropeJoint.Func("getBodyA", &b2RopeJoint::GetBodyA);
    _ropeJoint.Func("getBodyB", &b2RopeJoint::GetBodyB);
    _ropeJoint.Func("getCollideConnected", &b2RopeJoint::GetCollideConnected);
    _ropeJoint.Func("getLocalAnchorA", &b2RopeJoint::GetLocalAnchorA);
    _ropeJoint.Func("getLocalAnchorB", &b2RopeJoint::GetLocalAnchorB);
    _ropeJoint.Func("getMaxLength", &b2RopeJoint::GetMaxLength);
    _ropeJoint.Func("getReactionForce", &b2RopeJoint::GetReactionForce);
    _ropeJoint.Func("getReactionTorque", &b2RopeJoint::GetReactionTorque);
    _ropeJoint.Func("isActive", &b2RopeJoint::IsActive);
    _ropeJoint.Func("setMaxLength", &b2RopeJoint::SetMaxLength);
    Sqrat::RootTable(vm).Bind("b2RopeJoint", _ropeJoint);

    Sqrat::Class<b2FrictionJoint, Sqrat::NoConstructor> _frictionJoint;
    _frictionJoint.Func("getAnchorA", &b2FrictionJoint::GetAnchorA);
    _frictionJoint.Func("getAnchorB", &b2FrictionJoint::GetAnchorB);
    _frictionJoint.Func("getBodyA", &b2FrictionJoint::GetBodyA);
    _frictionJoint.Func("getBodyB", &b2FrictionJoint::GetBodyB);
    _frictionJoint.Func("getCollideConnected", &b2FrictionJoint::GetCollideConnected);
    _frictionJoint.Func("getLocalAnchorA", &b2FrictionJoint::GetLocalAnchorA);
    _frictionJoint.Func("getLocalAnchorB", &b2FrictionJoint::GetLocalAnchorB);
    _frictionJoint.Func("getMaxForce", &b2FrictionJoint::GetMaxForce);
    _frictionJoint.Func("getMaxTorque", &b2FrictionJoint::GetMaxTorque);
    _frictionJoint.Func("getReactionForce", &b2FrictionJoint::GetReactionForce);
    _frictionJoint.Func("getReactionTorque", &b2FrictionJoint::GetReactionTorque);
    _frictionJoint.Func("isActive", &b2FrictionJoint::IsActive);
    _frictionJoint.Func("setMaxForce", &b2FrictionJoint::SetMaxForce);
    _frictionJoint.Func("setMaxTorque", &b2FrictionJoint::SetMaxTorque);
    Sqrat::RootTable(vm).Bind("b2FrictionJoint", _frictionJoint);

    Sqrat::Class<Skeleton, Sqrat::NoConstructor> _skeleton;
    _skeleton.Func("body", &Skeleton::body);
    _skeleton.Func("clear", &Skeleton::clear);
    _skeleton.Func("create", &Skeleton::create);
    _skeleton.Func("joint", &Skeleton::joint);
    _skeleton.Func("distanceJoint", &Skeleton::distanceJoint);
    _skeleton.Func("frictionJoint", &Skeleton::frictionJoint);
    _skeleton.Func("gearJoint", &Skeleton::gearJoint);
    _skeleton.Func("mouseJoint", &Skeleton::mouseJoint);
    _skeleton.Func("prismaticJoint", &Skeleton::prismaticJoint);
    _skeleton.Func("pulleyJoint", &Skeleton::pulleyJoint);
    _skeleton.Func("revoluteJoint", &Skeleton::revoluteJoint);
    _skeleton.Func("ropeJoint", &Skeleton::ropeJoint);
    _skeleton.Func("weldJoint", &Skeleton::weldJoint);
    _skeleton.Func("wheelJoint", &Skeleton::wheelJoint);
    _skeleton.Func("mainBody", &Skeleton::mainBody);
    Sqrat::RootTable(vm).Bind("Skeleton", _skeleton);
}

void Skeleton::createShapes(Skeleton *sk, std::map<std::string, b2Shape *> &shapes, const json::ValueHandle &value)
{
    (void)sk;
    for (unsigned int i = 0; i < value.numChildren(); ++i)
    {
        json::ValueHandle hShape = value.childValue(i);
        b2Shape* shape;
        std::string sName = hShape.childPair("Name").value().string();
        std::string sType = hShape.childPair("Type").value().string();

        if (shapes.find(sName) != shapes.end())
        {
            std::clog<<"[Error] Duplicate shape name : "<<sName<<".";
            continue;
        }

        if (sType == "Box")
        {
            json::ValueHandle handle = hShape.childPair("Size").value();
            b2Vec2 size;
            size.x = handle.childValue(0).number();
            size.y = handle.childValue(1).number();

            handle = hShape.childPair("Center").value();
            b2Vec2 center;
            center.x = handle.childValue(0).number();
            center.y = handle.childValue(1).number();

            handle = hShape.childPair("Angle").value();
            float angle = handle.number() * b2_pi / 360.f;

            b2PolygonShape* pShape = new b2PolygonShape();
            pShape->SetAsBox(size.x/2, size.y/2, center, angle);

            shape = (b2Shape*)pShape;

            std::clog<<"\n\n\t==="<<sName<<"===";
            std::clog<<"\nSize : "<<size.x<<" , "<<size.y;
            std::clog<<"\nCenter : "<<center.x<<" , "<<center.y;
            std::clog<<"\nAngle : "<<angle;
        }
        else if (sType == "Circle")
        {
            json::ValueHandle handle = hShape.childPair("Center").value();
            b2Vec2 center;
            center.x = handle.childValue(0).number();
            center.y = handle.childValue(1).number();

            handle = hShape.childPair("Radius").value();
            float radius = handle.number();

            b2CircleShape* pShape = new b2CircleShape();
            pShape->m_p = center;
            pShape->m_radius = radius;

            shape = (b2Shape*)pShape;

            std::clog<<"\n\n\t==="<<sName<<"===";
            std::clog<<"\ncenter : "<<center.x<<" , "<<center.y;
            std::clog<<"\nradius : "<<radius;
        }
        else if (sType == "Polygon")
        {
            json::ValueHandle handle = hShape.childPair("Vertex").value();
            const int numVert = handle.numChildren();
            b2Vec2 *vert = new b2Vec2[numVert];

            for (int i = 0; i < numVert/2; ++i)
            {
                vert[i].x = handle.childValue(i*2).number();
                vert[i].y = handle.childValue(i*2+1).number();
            }

            b2PolygonShape* pShape = new b2PolygonShape();
            pShape->Set(vert, numVert/2);

            shape = (b2Shape*)pShape;

            delete [] vert;
        }
        else if (sType == "Edge")
        {
            json::ValueHandle handle = hShape.childPair("Vertex-Start").value();
            b2Vec2 v1;
            v1.x = handle.childValue(0).number();
            v1.y = handle.childValue(1).number();

            handle = hShape.childPair("Vertex-End").value();
            b2Vec2 v2;
            v2.x = handle.childValue(0).number();
            v2.y = handle.childValue(1).number();

            b2EdgeShape* pShape = new b2EdgeShape();
            pShape->Set(v1, v2);

            shape = (b2Shape*)pShape;
        }
        else if (sType == "Chain")
        {
            json::ValueHandle handle = hShape.childPair("Loop").value();
            bool loop = handle.boolean();

            handle = hShape.childPair("Vertex").value();
            const int numVert = handle.numChildren();
            b2Vec2 *vert = new b2Vec2[numVert];

            for (int i = 0; i < numVert/2; ++i)
            {
                vert[i].x = handle.childValue(i*2).number();
                vert[i].y = handle.childValue(i*2+1).number();
            }

            b2ChainShape* pShape = new b2ChainShape();
            if (loop)   pShape->CreateLoop(vert, numVert/2);
            else pShape->CreateChain(vert, numVert/2);

            shape = (b2Shape*)pShape;

            delete [] vert;
        }
        else
        {
            std::clog<<"[Error] Unrecognized shape type : "<<sType<<".";
            continue;
        }

        shapes[sName] = shape;
    }
}

void Skeleton::createFixtures(Skeleton *sk, std::map<std::string, b2Shape *> &shapes, std::map<std::string, b2FixtureDef *> &fixtures, const json::ValueHandle &value)
{
    (void)sk;
    for (unsigned int i = 0; i < value.numChildren(); ++i)
    {
        json::ValueHandle hFixDef = value.childValue(i);
        std::string fName = hFixDef.childPair("Name").value().string();
        std::string fShape = hFixDef.childPair("Shape").value().string();
        if (shapes.find(fShape) == shapes.end())
        {
            std::clog<<"[Error] Shape "<<fShape<<" is not declared.";
            continue;
        }
        if (fixtures.find(fName) != fixtures.end())
        {
            std::clog<<"[Error] Duplicate fixture name : "<<fName<<".";
            continue;
        }
        b2FixtureDef* fixDef = new b2FixtureDef();
        fixDef->shape = shapes[fShape];
        fixDef->density = hFixDef.childPair("Density").value().number();
        fixDef->friction = hFixDef.childPair("Friction").value().number();
        fixDef->restitution = hFixDef.childPair("Restitution").value().number();
        fixDef->isSensor = hFixDef.childPair("Sensor").value().boolean();
        fixDef->filter.categoryBits = hFixDef.childPair("Filter.CategoryBits").value().number();
        fixDef->filter.maskBits = hFixDef.childPair("Filter.MaskBits").value().number();
        fixDef->filter.groupIndex = hFixDef.childPair("Filter.GroupIndex").value().number();

        fixtures[fName] = fixDef;

        std::clog<<"\n\t==="<<fShape<<"===";
        std::clog<<"\nDensity : "<<fixDef->density;
        std::clog<<"\nFriction : "<<fixDef->friction;
        std::clog<<"\nRestitution : "<<fixDef->restitution;
        std::clog<<"\nSensor : "<<fixDef->isSensor;
        std::clog<<"\nCatbits : "<<fixDef->filter.categoryBits;
        std::clog<<"\nMaskbits : "<<fixDef->filter.maskBits;
        std::clog<<"\nGroup Index : "<<fixDef->filter.groupIndex;
    }
}

void Skeleton::createBodies(Skeleton *sk, std::map<std::string, b2FixtureDef *> &fixtures, const json::ValueHandle &value)
{
    for (unsigned int i = 0; i < value.numChildren(); ++i)
    {
        json::ValueHandle hBodyDef = value.childValue(i);
        std::string bName = hBodyDef.childPair("Name").value().string();

        if (sk->m_bodies.find(bName) != sk->m_bodies.end())
        {
            std::clog<<"[Error] Duplicate body name : "<<bName<<".";
            continue;
        }

        std::string bType = hBodyDef.childPair("Type").value().string();

        b2BodyDef def;
        if (bType == "Static") def.type = b2_staticBody;
        else if (bType == "Kinematic") def.type = b2_kinematicBody;
        else def.type = b2_dynamicBody;

        json::ValueHandle handle = hBodyDef.childPair("Position").value();
        def.position.x = handle.childValue(0).number();
        def.position.y = handle.childValue(1).number();

        def.angle = hBodyDef.childPair("Angle").value().number() * b2_pi / 360.f;
        def.linearDamping = hBodyDef.childPair("LinearDamping").value().number();
        def.angularDamping = hBodyDef.childPair("AngularDamping").value().number();
        def.gravityScale = hBodyDef.childPair("GravityScale").value().number();
        def.fixedRotation = hBodyDef.childPair("FixedRotation").value().boolean();
        def.bullet = hBodyDef.childPair("Bullet").value().boolean();

        std::clog<<"\n\n\t==="<<bName<<"===";
        std::clog<<"\nAngle : "<<def.angle;
        std::clog<<"\nLinear Damping : "<<def.linearDamping;
        std::clog<<"\nAngular Damping : "<<def.angularDamping;
        std::clog<<"\nGravity Scale : "<<def.gravityScale;
        std::clog<<"\nFixed Rotation : "<<def.fixedRotation;
        std::clog<<"\nBullet : "<<def.bullet;

        b2Body* body = sk->m_bodies[bName] = sk->m_world->CreateBody(&def);
        if (!sk->m_mainBody) sk->m_mainBody = body;

        handle = hBodyDef.childPair("Fixtures").value();
        for (unsigned int i = 0; i < handle.numChildren(); ++i)
        {
            std::string fName = handle.childValue(i).string();
            if (fixtures.find(fName) == fixtures.end())
            {
                std::clog<<"[Error] fixture "<<fName<<" is not defined.";
                continue;
            }
            body->CreateFixture(fixtures[fName]);
        }
    }

}

void Skeleton::createJoints(Skeleton *sk, const json::ValueHandle &value)
{
    for (unsigned int i = 0; i < value.numChildren(); ++i)
    {
        json::ValueHandle hJoint = value.childValue(i);
        b2Joint* joint = nullptr;
        std::string jName = hJoint.childPair("Name").value().string();
        std::string jType = hJoint.childPair("Type").value().string();
        std::string jBodyA = hJoint.childPair("BodyA").value().string();
        std::string jBodyB = hJoint.childPair("BodyB").value().string();

        if (sk->m_bodies.find(jBodyA) == sk->m_bodies.end() || sk->m_bodies.find(jBodyB) == sk->m_bodies.end())
        {
            std::clog<<"[Error] "<<jBodyA<<" or "<<jBodyB<<" is not defined.";
            continue;
        }
        b2Body* bodyA = sk->m_bodies[jBodyA];
        b2Body* bodyB = sk->m_bodies[jBodyB];

        if (jType == "Revolute")
        {
            json::ValueHandle handle = hJoint.childPair("LocalAnchorA").value();
            b2Vec2 localAnchorA;
            localAnchorA.x = handle.childValue(0).number();
            localAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAnchorB").value();
            b2Vec2 localAnchorB;
            localAnchorB.x = handle.childValue(0).number();
            localAnchorB.y = handle.childValue(1).number();

            b2RevoluteJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.localAnchorA = localAnchorA;
            jDef.localAnchorB = localAnchorB;
            jDef.lowerAngle = hJoint.childPair("LowerAngle").value().number();
            jDef.upperAngle = hJoint.childPair("UpperAngle").value().number();
            jDef.enableLimit = hJoint.childPair("EnableLimit").value().boolean();
            jDef.enableMotor = hJoint.childPair("EnableMotor").value().boolean();
            jDef.maxMotorTorque = hJoint.childPair("MaxMotorTorque").value().number();
            jDef.motorSpeed = hJoint.childPair("MotorSpeed").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();
            jDef.referenceAngle = hJoint.childPair("ReferenceAngle").value().number();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Distance")
        {
            json::ValueHandle handle = hJoint.childPair("LocalAnchorA").value();
            b2Vec2 localAnchorA;
            localAnchorA.x = handle.childValue(0).number();
            localAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAnchorB").value();
            b2Vec2 localAnchorB;
            localAnchorB.x = handle.childValue(0).number();
            localAnchorB.y = handle.childValue(1).number();

            b2DistanceJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.localAnchorA = localAnchorA;
            jDef.localAnchorB = localAnchorB;
            jDef.dampingRatio = hJoint.childPair("DampingRatio").value().number();
            jDef.frequencyHz = hJoint.childPair("FrequencyHz").value().number();
            jDef.length = hJoint.childPair("Length").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Prismatic")
        {
            json::ValueHandle handle = hJoint.childPair("LocalAnchorA").value();
            b2Vec2 localAnchorA;
            localAnchorA.x = handle.childValue(0).number();
            localAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAnchorB").value();
            b2Vec2 localAnchorB;
            localAnchorB.x = handle.childValue(0).number();
            localAnchorB.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAxisA").value();
            b2Vec2 localAxisA;
            localAxisA.x = handle.childValue(0).number();
            localAxisA.y = handle.childValue(1).number();

            b2PrismaticJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.localAnchorA = localAnchorA;
            jDef.localAnchorB = localAnchorB;
            jDef.enableLimit = hJoint.childPair("EnableLimit").value().boolean();
            jDef.enableMotor = hJoint.childPair("EnableMotor").value().boolean();
            jDef.localAxisA = localAxisA;
            jDef.lowerTranslation = hJoint.childPair("LowerTranslation").value().number();
            jDef.upperTranslation = hJoint.childPair("UpperTranslation").value().number();
            jDef.maxMotorForce = hJoint.childPair("MaxMotorForce").value().number();
            jDef.motorSpeed = hJoint.childPair("MotorSpeed").value().number();
            jDef.referenceAngle = hJoint.childPair("ReferenceAngle").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Pulley")
        {
            json::ValueHandle handle = hJoint.childPair("LocalAnchorA").value();
            b2Vec2 localAnchorA;
            localAnchorA.x = handle.childValue(0).number();
            localAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAnchorB").value();
            b2Vec2 localAnchorB;
            localAnchorB.x = handle.childValue(0).number();
            localAnchorB.y = handle.childValue(1).number();

            handle = hJoint.childPair("GroundAnchorA").value();
            b2Vec2 groundAnchorA;
            groundAnchorA.x = handle.childValue(0).number();
            groundAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("GroundAnchorB").value();
            b2Vec2 groundAnchorB;
            groundAnchorB.x = handle.childValue(0).number();
            groundAnchorB.y = handle.childValue(1).number();

            b2PulleyJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.localAnchorA = localAnchorA;
            jDef.localAnchorB = localAnchorB;
            jDef.groundAnchorA = groundAnchorA;
            jDef.groundAnchorB = groundAnchorB;
            jDef.lengthA = hJoint.childPair("LengthA").value().number();
            jDef.lengthB = hJoint.childPair("LengthB").value().number();
            jDef.ratio = hJoint.childPair("Ratio").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Gear")
        {
            std::string jJoint1 = hJoint.childPair("Joint1").value().string();
            std::string jJoint2 = hJoint.childPair("Joint2").value().string();
            if (sk->m_joints.find(jJoint1) == sk->m_joints.end() || sk->m_joints.find(jJoint2) == sk->m_joints.end())
            {
                std::clog<<"[Error] "<<jJoint1<<" or "<<jJoint2<<" is not defined.";
                continue;
            }
            b2GearJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.joint1 = sk->m_joints[jJoint1];
            jDef.joint2 = sk->m_joints[jJoint2];
            jDef.ratio = hJoint.childPair("Ratio").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Mouse")
        {
            json::ValueHandle handle = hJoint.childPair("Target").value();
            b2Vec2 target;
            target.x = handle.childValue(0).number();
            target.y = handle.childValue(1).number();

            b2MouseJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.target = target;
            jDef.dampingRatio = hJoint.childPair("DampingRatio").value().number();
            jDef.frequencyHz = hJoint.childPair("FrequencyHz").value().number();
            jDef.maxForce = hJoint.childPair("MaxForce").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Wheel")
        {
            json::ValueHandle handle = hJoint.childPair("LocalAnchorA").value();
            b2Vec2 localAnchorA;
            localAnchorA.x = handle.childValue(0).number();
            localAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAnchorB").value();
            b2Vec2 localAnchorB;
            localAnchorB.x = handle.childValue(0).number();
            localAnchorB.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAxisA").value();
            b2Vec2 localAxisA;
            localAxisA.x = handle.childValue(0).number();
            localAxisA.y = handle.childValue(1).number();

            b2WheelJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.localAnchorA = localAnchorA;
            jDef.localAnchorB = localAnchorB;
            jDef.localAxisA = localAxisA;
            jDef.enableMotor = hJoint.childPair("EnableMotor").value().boolean();
            jDef.maxMotorTorque = hJoint.childPair("MaxMotorTorque").value().number();
            jDef.motorSpeed = hJoint.childPair("MotorSpeed").value().boolean();
            jDef.dampingRatio = hJoint.childPair("DampingRatio").value().number();
            jDef.frequencyHz = hJoint.childPair("FrequencyHz").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Weld")
        {
            json::ValueHandle handle = hJoint.childPair("LocalAnchorA").value();
            b2Vec2 localAnchorA;
            localAnchorA.x = handle.childValue(0).number();
            localAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAnchorB").value();
            b2Vec2 localAnchorB;
            localAnchorB.x = handle.childValue(0).number();
            localAnchorB.y = handle.childValue(1).number();

            b2WeldJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.localAnchorA = localAnchorA;
            jDef.localAnchorB = localAnchorB;
            jDef.dampingRatio = hJoint.childPair("DampingRatio").value().number();
            jDef.frequencyHz = hJoint.childPair("FrequencyHz").value().number();
            jDef.referenceAngle = hJoint.childPair("ReferenceAngle").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Rope")
        {
            json::ValueHandle handle = hJoint.childPair("LocalAnchorA").value();
            b2Vec2 localAnchorA;
            localAnchorA.x = handle.childValue(0).number();
            localAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAnchorB").value();
            b2Vec2 localAnchorB;
            localAnchorB.x = handle.childValue(0).number();
            localAnchorB.y = handle.childValue(1).number();

            b2RopeJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.localAnchorA = localAnchorA;
            jDef.localAnchorB = localAnchorB;
            jDef.maxLength = hJoint.childPair("MaxLength").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();

            joint = sk->m_world->CreateJoint(&jDef);
        }
        else if (jType == "Friction")
        {
            json::ValueHandle handle = hJoint.childPair("LocalAnchorA").value();
            b2Vec2 localAnchorA;
            localAnchorA.x = handle.childValue(0).number();
            localAnchorA.y = handle.childValue(1).number();

            handle = hJoint.childPair("LocalAnchorB").value();
            b2Vec2 localAnchorB;
            localAnchorB.x = handle.childValue(0).number();
            localAnchorB.y = handle.childValue(1).number();

            b2FrictionJointDef jDef;
            jDef.bodyA = bodyA;
            jDef.bodyB = bodyB;
            jDef.localAnchorA = localAnchorA;
            jDef.localAnchorB = localAnchorB;
            jDef.maxForce = hJoint.childPair("MaxForce").value().number();
            jDef.maxTorque = hJoint.childPair("MaxTorque").value().number();
            jDef.collideConnected = hJoint.childPair("CollideConnected").value().boolean();
        }
        else
        {
            std::clog<<"[Error] Unrecognized joint type : "<<jType<<".";
            continue;
        }

        sk->m_joints[jName] = joint;
    }

}


/*
 * PhysicsCompDesc
 */

Physics2DCompDesc::Physics2DCompDesc()
    : IComponentDef(Physics2DCompMgr::FamilyName, Physics2D::TypeName)
{
}

Physics2DCompDesc::Physics2DCompDesc(const Physics2DCompDesc &cp)
    : IComponentDef(Physics2DCompMgr::FamilyName, Physics2D::TypeName)
{
    this->m_cesFile = cp.m_cesFile;
}

Physics2DCompDesc& Physics2DCompDesc::operator =(const Physics2DCompDesc& rv)
{
    this->m_cesFile = rv.m_cesFile;
    return (*this);
}

void Physics2DCompDesc::configure(const json::ValueHandle &data)
{
    json::ValueHandle sfile = data.childPair("FileName").value();
    m_cesFile = sfile.string();
}

const std::string& Physics2DCompDesc::getCesFile() const
{
    return m_cesFile;
}

/*
 * PhysicsComponent
 */

const std::string Physics2D::TypeName = "Physics2D";
CERayCastCallback *Physics2D::rayCallback = nullptr;
CEQueryCallback *Physics2D::queryCallback = nullptr;

Physics2D::Physics2D(Physics2DCompMgr *mgr, const Physics2DCompDesc *desc)
    : m_mgr(mgr), m_desc(desc), m_owner(nullptr), m_skeleton(new Skeleton(mgr->world(), desc->getCesFile())),
      m_transform(nullptr), m_locChangeId(0)
{
    std::clog<<m_skeleton->mainBody()->GetPosition().x;
}

const std::string& Physics2D::familyName() const
{
    return Physics2DCompMgr::FamilyName;
}

const std::string& Physics2D::typeName() const
{
    return TypeName;
}

void Physics2D::initialize(Entity *ent)
{
    m_transform = (Transform2D*)ent->componentByType(Transform2D::TypeName);
    m_owner = ent;

    if(m_transform)
    {
        m_skeleton->setTransform(m_transform->getPosition(), m_transform->getRotation());
        m_skeleton->setUserData((void*)ent);
        m_locChangeId = m_transform->lastChanageId();
    }
}

void Physics2D::deinitialize()
{
    m_transform = nullptr;
    delete m_skeleton;
    m_skeleton = nullptr;
    m_mgr->markForDeletion(this);
}

void Physics2D::saveState(ConfStorage &conf) const
{
    (void)conf;
}

void Physics2D::loadState(const ConfStorage &conf)
{
    this->IEditable::loadState(conf, TypeName+".");
}

Entity* Physics2D::rayCast(Vector2 dir, float range, float fraction)
{
    dir.Normalize();
    b2Vec2 rayDir(dir.x * range, dir.y * range);

    if(!rayCallback) rayCallback = new CERayCastCallback(fraction);
    rayCallback->clear();
    rayCallback->m_fraction = fraction;

    m_mgr->world()->RayCast(rayCallback, m_skeleton->mainBody()->GetPosition(), m_skeleton->mainBody()->GetPosition() + rayDir);
    return rayCallback->entity();
}

const std::unordered_set<Entity*>& Physics2D::query(const Rect &area) const
{
    if (!queryCallback) queryCallback = new CEQueryCallback();
    queryCallback->clear();
    b2AABB aabb;
    aabb.lowerBound = b2Vec2(std::min(area.x, area.x + area.width), std::min(area.y, area.y + area.height));
    aabb.upperBound = b2Vec2(std::max(area.x, area.x + area.width), std::max(area.y, area.y + area.height));
    m_mgr->world()->QueryAABB(queryCallback, aabb);
    return queryCallback->entities();
}

Physics2DCompMgr* Physics2D::manager()
{
    return m_mgr;
}

const Physics2DCompMgr* Physics2D::manager() const
{
    return m_mgr;
}

Skeleton* Physics2D::skeleton()
{
    return m_skeleton;
}

const Skeleton* Physics2D::skeleton() const
{
    return m_skeleton;
}

Entity* Physics2D::owner()
{
    return m_owner;
}

const Entity* Physics2D::owner() const
{
    return m_owner;
}

void Physics2D::update()
{
    auto body = m_skeleton->mainBody();
    if(m_transform && m_skeleton->mainBody())
    {
        unsigned int changeId = m_transform->lastChanageId();
        if(changeId != m_locChangeId)
        {
            m_skeleton->setTransform(m_transform->getPosition(), m_transform->getRotation());
            body->SetLinearVelocity(b2Vec2(0, 0));
            body->SetAngularVelocity(0);
            body->SetAwake(true);
            body->SetActive(true);
            m_locChangeId = m_transform->lastChanageId();
        }
        else
        {
            m_transform->setPosition(m_skeleton->getPosition());
            m_transform->setRotation(m_skeleton->getRotation());
            m_locChangeId = m_transform->lastChanageId();
        }
    }
}

void Physics2D::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Physics2D> _PhysicsComponent;
    _PhysicsComponent.SquirrelFunc("query", &Physics2D::Sq_query);
    _PhysicsComponent.Func("familyName", &Physics2D::familyName);
    _PhysicsComponent.Func("rayCast", &Physics2D::rayCast);
    _PhysicsComponent.Func<const Entity*(Physics2D::*)()const>("owner", &Physics2D::owner);
    _PhysicsComponent.Func("typeName", &Physics2D::typeName);
    _PhysicsComponent.Func<const Skeleton*(Physics2D::*)()const>("skeleton", &Physics2D::skeleton);
    Sqrat::RootTable(vm).Bind("Physics2D", _PhysicsComponent);
}

SQInteger Physics2D::Sq_query(HSQUIRRELVM vm)
{
    // Checks that we have the right number of parameters
    if (sq_gettop(vm) == 2) {
        Sqrat::Var<Physics2D&> phys(vm, 1);
        Sqrat::Var<Rect&> area(vm,2);
        // Make sure that sf::Image we got was a sf::Image
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            // Get the image's pixels (note that each pixel consists of 4 numbers)
            auto entities = phys.value.query(area.value);

            // Create a new array (we'll optimize it by passing it the size it will be)
            sq_newarray(vm, entities.size());

            std::size_t i = 0;
            // Load that baby up with them datas
            for (auto it = entities.begin(); it != entities.end(); ++it, ++i) {
                Sqrat::PushVar(vm, i);
                Sqrat::PushVar(vm, (*it));
                sq_rawset(vm, -3);
            }

            // Inform Squirrel that we want the 1 thing (array) on the top of the stack to be returned
            return 1;
        }

        // Return the error generated by Sqrat::Var
        return sq_throwerror(vm, Sqrat::Error::Instance().Message(vm).c_str());
    }

    return sq_throwerror(vm, _SC("wrong number of parameters"));
}

/*
 * PhysicsCompMgr
 */

const std::string Physics2DCompMgr::FamilyName = "Physics2D";

Physics2DCompMgr::Physics2DCompMgr()
    : m_draw(nullptr)
{
    m_world = new b2World(b2Vec2(0, -10));
}

Physics2DCompMgr::~Physics2DCompMgr()
{
    delete m_world;
    delete m_draw;
}

const std::string& Physics2DCompMgr::familyName() const
{
    return FamilyName;
}

void Physics2DCompMgr::initialize(GameWorld *world)
{
    ((void)world);
}

void Physics2DCompMgr::deinitialize()
{
    m_PhysicsCompList.clear();

    for(Physics2D* comp : m_forDeletion)
        delete comp;
    m_forDeletion.clear();
}

IComponent* Physics2DCompMgr::createComponent(const IComponentDef *desc)
{
    if(desc->familyName != FamilyName)
        return nullptr;

    if(desc->typeName == Physics2D::TypeName)
    {
        const Physics2DCompDesc* pd = (const Physics2DCompDesc*)desc;
        m_PhysicsCompList.push_back(new Physics2D(this, pd));
        return m_PhysicsCompList.back();
    }

    return nullptr;
}

void Physics2DCompMgr::update(double deltaTime)
{
    m_world->Step(deltaTime, 10, 10);
    for(Physics2D* comp : m_PhysicsCompList)
        comp->update();

    if(m_draw)
    {
        m_world->DrawDebugData();
        m_draw->flush();
    }

    for(Physics2D* comp : m_forDeletion)
        delete comp;
    m_forDeletion.clear();
}

void Physics2DCompMgr::enableDebugDraw(bool e)
{
    if(e)
    {
        if(m_draw)
            return;

        m_draw = new PhysicsDebugDraw();
        m_draw->SetFlags(b2Draw::e_shapeBit|b2Draw::e_jointBit|b2Draw::e_aabbBit|b2Draw::e_pairBit|b2Draw::e_centerOfMassBit);
        m_world->SetDebugDraw(m_draw);
    }
    else
    {
        if(!m_draw)
            return;

        m_world->SetDebugDraw(nullptr);
        delete m_draw;
        m_draw = nullptr;
    }
}

void Physics2DCompMgr::markForDeletion(Physics2D *comp)
{
    m_forDeletion.push_back(comp);
}

b2World* Physics2DCompMgr::world()
{
    return m_world;
}

const b2World* Physics2DCompMgr::world() const
{
    return m_world;
}
