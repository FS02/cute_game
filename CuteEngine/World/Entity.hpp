#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <CuteEngine/Utils/JSON.hpp>
#include <CuteEngine/Core/ResourceMgr.hpp>
#include <CuteEngine/Core/IEditable.hpp>
#include <CuteEngine/Core/ConfStorage.hpp>
#include <string>
#include <vector>
#include <list>

#define cuteKEY(name) (TypeName + std::string(".") + std::string(#name))
#define cuteSAVE_STATE(storage, propName, descPtr, descPropName) if(this->get##propName() != (descPtr)->get##descPropName()) storage.addValue(cuteKEY(propName), this->get##propName())
#define cuteSAVE_STATE2(storage, thisPtr, propName, descPtr, descPropName) if((thisPtr)->get##propName() != (descPtr)->get##descPropName()) storage.addValue(cuteKEY(propName), (thisPtr)->get##propName())

namespace cute
{
    class Entity;
    class GameWorld;

    /*!
     * \brief Interface for component definition.
     * Classes derived from this interface hold parameters used to create specific components.
     * This should use only "generic" parameters (like strings, numbers, etc.).
     * Especially, it should not hold pointers to external objects.
     * Component descriptions will be loaded from JSON/XML entity description files.
     */
    class IComponentDef : public IEditable
    {
    public:
        const std::string familyName;
        const std::string typeName;

        virtual ~IComponentDef();

        /*!
         * \brief Configures component description from JSON data.
         * \param data JSON object describing this component description.
         */
        virtual void configure(const json::ValueHandle& data) = 0;
        /*!
         * \brief Creates JSON object containing all data needed by configure method.
         * Default implementation writes all properties and type name.
         * \return Pointer to JSON object.
         */
        virtual json::ObjectValue* toJSON() const;

    protected:
        /*!
         * \brief Contructor. Derived classes have to call it and pass family and type names (note: these members are constant).
         * \param familyName
         * \param typeName
         */
        IComponentDef(const std::string& familyName, const std::string& typeName);
    };

    /*!
     * \brief Interface for entity components.
     * Yes, it doesn't have an update method. Components are updated by their managers. This way we avoid one virtual call, yay!
     */
    class IComponent : public IEditable
    {
    public:
        virtual ~IComponent();

        virtual const std::string& familyName() const = 0;
        virtual const std::string& typeName() const = 0;

        /*!
         * \brief Initializes a component.
         * Example: we may have an InventoryComponent storing all items owned by a character and InventoryUIComponent that draws these items in the inventory screen.
         * InventoryUIComponent needs a pointer to InventoryComponent to obtain items. This is the place to get a pointer to InventoryComponent for InventoryUIComponent.
         * \param ent Owner of this component.
         */
        virtual void initialize(Entity* ent) = 0;
        /*!
         * \brief Deinitializes a component.
         * Its called in Entity's destrcutor before deleting any other component.
         * You can safly call methods of other components from here.
         */
        virtual void deinitialize() = 0;

        /*!
         * \brief Saves current state. Should save only values that have changed since component's creation to reduce data size.
         * Keys should have the following format: <ComponentTypeName>.<Key>
         * \param conf Container for data, shared between all components in one entity.
         */
        virtual void saveState(ConfStorage& conf) const = 0;
        virtual void loadState(const ConfStorage& conf) = 0;

    };

    /*!
     * \brief Container for IComponentDef. It can be loaded from a JSON file.
     */
    class EntityDef : public IResource
    {
    public:
        static std::string path;

        EntityDef(const std::string& fileName);
        virtual ~EntityDef();

        virtual bool load();
        bool write();

        void addComponent(IComponentDef* desc);
        IComponentDef* component(int index);
        const IComponentDef* component(int index) const;

        //! Factory function used by EntityDescMgr.
        static EntityDef* Create(const std::string& fileName);

    private:
        std::vector<IComponentDef*> m_components;

        void _addComponent(IComponentDef* desc);
    };

    typedef ResourceMgr<EntityDef> EntityDescMgr;

    /*!
     * \brief The Entity class
     */
    class Entity
    {
        friend class GameWorld;

    public:
        /*!
         * \brief Constructor.
         * Do NOT call this method directly. Use GameWorld::createEntity. Thank you.
         * \param name Unique name of the entity.
         */
        Entity(const std::string& name, const EntityDef* desc);
        Entity(const Entity&) = delete;
        ~Entity();

        //! Returns name of the entity.
        const std::string& name() const;
        //! Returns description from which this object was created.
        const EntityDef* description() const;

        void addComponent(IComponent* comp);
        void initialize(GameWorld* world);

        json::ObjectValue* toJSON() const;

        void saveState(ConfStorage& storage) const;
        void loadState(const ConfStorage& storage);

        GameWorld* world();

        IComponent* component(int index);
        /*! If an entity has several components of the same family, use index to specify which one you want.
         * Returns nullptr if there's no component with such family, or if index is out-of-range.
         */
        IComponent* componentByFamily(const std::string& familyName, int index = 0);
        /*! If an entity has several components of the same type, use index to specify which one you want.
         * Returns nullptr if there's no component with such type, or if index is out-of-range.
         */
        IComponent* componentByType(const std::string& typeName, int index = 0);

        //! Entity will be deleted as soon as it's safe.
        void markAsRedundant();
        //! Is marked for deletion?
        bool isRedundant() const;

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static SQInteger Sq_component(HSQUIRRELVM vm);
        static SQInteger Sq_componentByFamily(HSQUIRRELVM vm);
        static SQInteger Sq_componentByType(HSQUIRRELVM vm);
        static SQInteger Sq_pushComponent(HSQUIRRELVM vm, IComponent* comp);

        const std::string m_name;
        const EntityDef* m_desc;
        bool m_isInitialized;
        GameWorld* m_world;
        std::vector<IComponent*> m_components;
        bool m_isRedundant;
    };
}

#endif // ENTITY_HPP
