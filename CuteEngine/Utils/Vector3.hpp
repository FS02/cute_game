/*
 * "MathLib"
 * Date: 17-08-2011
 * Author: P. Podsiadly
 */
#ifndef _CUVECTOR3_HPP_
#define _CUVECTOR3_HPP_

#include <Box2D/Box2D.h>
#include <CuteEngine/Squirrel/include/sqrat.h>

#include <cmath>
#include <ostream>

#ifndef M_PI
#	define M_PI 3.14159265358979323846f
#endif

namespace cute
{
    typedef b2Vec3 Vector3;

    class _StaticBindVector3
    {
    public:
        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static SQInteger SqConstructor(HSQUIRRELVM vm);
        static SQInteger Sq_add(HSQUIRRELVM vm);
        static SQInteger Sq_sub(HSQUIRRELVM vm);
        static SQInteger Sq_mul(HSQUIRRELVM vm);
        static SQInteger Sq_div(HSQUIRRELVM vm);
        static SQInteger Sq_cmp(HSQUIRRELVM vm);
    };
}

inline bool operator==(const b2Vec3& lhs, const b2Vec3& rhs){return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z); }
inline bool operator!=(const b2Vec3& lhs, const b2Vec3& rhs){return !(lhs == rhs); }

#endif /* _CUVECTOR3_HPP_ */
