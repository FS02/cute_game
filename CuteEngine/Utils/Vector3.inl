template<class T>
inline std::ostream& operator <<(std::ostream& stream, const Vector3<T>& v)
{
    stream<<v.x<<", "<<v.y<<", "<<v.z;
    return stream;
}

template<class T>
inline bool operator <(const Vector3<T>& a, const Vector3<T>& b);

/*
 * Implementation
 */

template<class T>
Vector3<T>::Vector3(T x, T y, T z)
{
    this->set(x, y, z);
}

template<class T>
Vector3<T>::Vector3(const Vector3& copy)
{
    this->set(copy.x, copy.y, copy.z);
}

template<class T>
void Vector3<T>::set(T vx, T vy, T vz)
{
    x=vx;
    y=vy;
    z=vz;
}

template<class T>
T Vector3<T>::length() const
{
    return std::sqrt(x*x + y*y + z*z);
}

template<class T>
T Vector3<T>::lengthSq() const
{
    return x*x + y*y + z*z;
}

template<class T>
void Vector3<T>::normalize()
{
    T m=this->length();
    if(m!=0)
        (*this)/=m;
}

template<class T>
Vector3<T> Vector3<T>::normalizedCopy() const
{
    T m=this->length();
    if(m==0)
        return Vector3(0, 0, 0);
    return (*this)/m;
}

template<class T>
T Vector3<T>::dot(const Vector3& a, const Vector3& b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

template<class T>
Vector3<T> Vector3<T>::cross(const Vector3& a, const Vector3& b)
{
    return Vector3(a.y*b.z - a.z*b.y,
        a.z*b.x - a.x*b.z,
        a.x*b.y - a.y*b.x);
}

template<class T>
T Vector3<T>::distance(const Vector3& a, const Vector3& b)
{
    return (a-b).length();
}

template<class T>
T Vector3<T>::distanceSq(const Vector3& a, const Vector3& b)
{
    return (a-b).lengthSq();
}

template<class T>
Vector3<T> Vector3<T>::lerp(const Vector3& a, const Vector3& b, T delta)
{
    return a*(1.0f-delta) + b*delta;
}

template<class T>
Vector3<T> Vector3<T>::right(const Vector3& forward, const Vector3& up)
{
    return cross(forward.normalizedCopy(), up.normalizedCopy());
}

template<class T>
Vector3<T> Vector3<T>::left(const Vector3& forward, const Vector3& up)
{
    Vector3<T> r = right(forward, up);
    return Vector3<T>(-r.x, -r.y, -r.z);
}

template<class T>
void Vector3<T>::bindSquirrel(HSQUIRRELVM vm)
{
    // Binding Vector3
    Sqrat::Class<cute::Vector3f> vector3f;
    vector3f.SquirrelFunc("constructor", &cute::Vector3f::SqConstructor);
    vector3f.SquirrelFunc("_add", &cute::Vector3f::Sq_add);
    vector3f.SquirrelFunc("_sub", &cute::Vector3f::Sq_sub);
    vector3f.SquirrelFunc("_mul", &cute::Vector3f::Sq_mul);
    vector3f.SquirrelFunc("_div", &cute::Vector3f::Sq_div);
    vector3f.SquirrelFunc("_cmp", &cute::Vector3f::Sq_cmp);
    vector3f.StaticFunc("cross", &cross);
    vector3f.StaticFunc("dot", &dot);
    vector3f.StaticFunc("distance", &distance);
    vector3f.StaticFunc("distanceSq", &distanceSq);
    vector3f.StaticFunc("lerp", &lerp);
    vector3f.StaticFunc("right", &right);
    vector3f.StaticFunc("left", &left);
    vector3f.Func("set", &cute::Vector3f::set);
    vector3f.Func("length", &cute::Vector3f::length);
    vector3f.Func("lengthSq", &cute::Vector3f::lengthSq);
    vector3f.Func("normalize", &cute::Vector3f::normalize);
    vector3f.Func("normalizedCopy", &cute::Vector3f::normalizedCopy);
    vector3f.Var("x", &cute::Vector3f::x);
    vector3f.Var("y", &cute::Vector3f::y);
    vector3f.Var("z", &cute::Vector3f::z);
    Sqrat::RootTable(vm).Bind("Vector3", vector3f);
}

template<class T>
SQInteger Vector3<T>::SqConstructor(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 1) { // no manually put arguments? default constructor. note that the first argument is always a class or a environment in Squirrel functions
        Vector3f* instance = new Vector3f();
        sq_setinstanceup(vm, 1, instance);
        sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Vector3f>::Delete);
        return 0;
    } else if (sq_gettop(vm) == 2) {
        Sqrat::Var<const Vector3f&> copy(vm, 2); // copy constructor if 2nd argument (1st manually defined argument)
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Vector3f* instance = new Vector3f(copy.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Vector3f>::Delete);
            return 0;
        }
        Sqrat::Error::Instance().Clear(vm);
        return sq_throwerror(vm, Sqrat::Error::FormatTypeError(vm, 2, Sqrat::ClassType<Vector3f>::ClassName(vm) + _SC("|float")).c_str()); // wasnt a float either... user is dumb
    } else if (sq_gettop(vm) == 4) { // 4 arguments?
        Sqrat::Var<float> _vx(vm, 2);
        Sqrat::Var<float> _vy(vm, 3);
        Sqrat::Var<float> _vz(vm, 4);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Vector3f* instance = new Vector3f(_vx.value, _vy.value, _vz.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Vector3f>::Delete);
            return 0;
        }
        return sq_throwerror(vm, Sqrat::Error::Instance().Message(vm).c_str());
    }
    return sq_throwerror(vm, _SC("wrong number of parameters"));
}

template<class T>
SQInteger Vector3<T>::Sq_add(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector + vector
        Sqrat::Var<const Vector3f&> left(vm, 1);
        Sqrat::Var<const Vector3f&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Vector3f result = left.value + right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

template<class T>
SQInteger Vector3<T>::Sq_sub(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector - vector
        Sqrat::Var<const Vector3f&> left(vm, 1);
        Sqrat::Var<const Vector3f&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Vector3f result = left.value - right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

template<class T>
SQInteger Vector3<T>::Sq_mul(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector * float
        Sqrat::Var<const Vector3f&> left(vm, 1);
        Sqrat::Var<float> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Vector3f result = left.value * right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

template<class T>
SQInteger Vector3<T>::Sq_div(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector / float
        Sqrat::Var<const Vector3f&> left(vm, 1);
        Sqrat::Var<float> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Vector3f result = left.value / right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

template<class T>
SQInteger Vector3<T>::Sq_cmp(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector / float
        Sqrat::Var<const Vector3f&> left(vm, 1);
        Sqrat::Var<const Vector3f&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            int result;
            if (left.value == right.value) result = 0;
            else if (left.value.lengthSq() > right.value.lengthSq()) result = 1;
            else result = -1;
            return result;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

// operators

template<class T>
bool operator ==(const Vector3<T>& a, const Vector3<T>& b)
{
    return (a.x==b.x && a.y==b.y && a.z==b.z);
}

template<class T>
bool operator !=(const Vector3<T>& a, const Vector3<T>& b)
{
    return (a.x!=b.x || a.y!=b.y || a.z!=b.z);
}

template<class T>
Vector3<T> operator +(const Vector3<T>& a, const Vector3<T>& b)
{
    return Vector3<T>(a.x+b.x, a.y+b.y, a.z+b.z);
}

template<class T>
Vector3<T> operator -(const Vector3<T>& a, const Vector3<T>& b)
{
    return Vector3<T>(a.x-b.x, a.y-b.y, a.z-b.z);
}

template<class T>
Vector3<T> operator *(const Vector3<T>& v, T s)
{
    return Vector3<T>(v.x*s, v.y*s, v.z*s);
}

template<class T>
Vector3<T> operator /(const Vector3<T>& v, T s)
{
    return Vector3<T>(v.x/s, v.y/s, v.z/s);
}

template<class T>
Vector3<T>& operator +=(Vector3<T>& a, const Vector3<T>& b)
{
    a.x+=b.x;
    a.y+=b.y;
    a.z+=b.z;
    return a;
}

template<class T>
Vector3<T>& operator -=(Vector3<T>& a, const Vector3<T>& b)
{
    a.x-=b.x;
    a.y-=b.y;
    a.z-=b.z;
    return a;
}

template<class T>
Vector3<T>& operator *=(Vector3<T>& v, T s)
{
    v.x*=s;
    v.y*=s;
    v.z*=s;
    return v;
}

template<class T>
Vector3<T>& operator /=(Vector3<T>& v, T s)
{
    v.x/=s;
    v.y/=s;
    v.z/=s;
    return v;
}

template<class T>
inline bool operator <(const Vector3<T>& a, const Vector3<T>& b)
{
    if(a.x < b.x)
        return true;
    else if(a.x > b.x)
        return false;

    if(a.y < b.y)
        return true;
    else if(a.y > b.y)
        return false;

    if(a.z < b.z)
        return true;
    else if(a.z > b.z)
        return false;

    return false;
}

