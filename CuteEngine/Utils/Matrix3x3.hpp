/*
 * "MathLib"
 * Date: 7-12-2011
 * Author: P. Podsiadly
 */

#ifndef _CUMATRIX3X3_HPP_
#define _CUMATRIX3X3_HPP_

#include "Vector2.hpp"
#include "Vector3.hpp"
#include <cassert>

namespace cute
{
    class Matrix3x3
    {
    public:
        enum Init
        {
            Identity
        };

        Matrix3x3(Init i=Identity);
        Matrix3x3(const Matrix3x3& copy);

        float at(unsigned int x, unsigned int y) const;
        float& at(unsigned int x, unsigned int y);
        float operator ()(unsigned int x, unsigned int y) const;
        float& operator ()(unsigned int x, unsigned int y);

        const float* data() const;

        void transpose();
        float determinant() const;
        float cofactor(unsigned int x, unsigned int y) const;
        void invert(Matrix3x3& m) const;

        void setTranslation(const cute::Vector2& v);
        void translate(const cute::Vector2& v);
        void setRotation(float rad);
        void rotate(float rad);
        void setScale(const cute::Vector2& v);
        void scale(const cute::Vector2& v);

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static SQInteger SqConstructor(HSQUIRRELVM vm);
        static SQInteger SqGetArrayData(HSQUIRRELVM vm);
        static SQInteger Sq_mul(HSQUIRRELVM vm);

        float m[9];
    };

    inline Matrix3x3 operator *(const Matrix3x3& a, const Matrix3x3& b)
    {
        Matrix3x3 res;
        for(int i=0; i<3; ++i)
        {
            for(int j=0; j<3; ++j)
            {
                res(i, j)=0;
                for(int k=0; k<3; ++k)
                    res(i, j)+=a(k, j)*b(i, k);
            }
        }

        return res;
    }

    inline cute::Vector2 operator *(const Matrix3x3& m, const cute::Vector2& v)
    {
        return cute::Vector2(m(0, 0)*v.x + m(1, 0)*v.y + m(2, 0),
                m(0, 1)*v.x + m(1, 1)*v.y + m(2, 1));
    }

    inline cute::Vector3 operator *(const Matrix3x3& m, const cute::Vector3& v)
    {
        return cute::Vector3(m(0, 0)*v.x + m(1, 0)*v.y + m(2, 0)*v.z,
                m(0, 1)*v.x + m(1, 1)*v.y + m(2, 1)*v.z,
                m(0, 2)*v.x + m(1, 2)*v.y + m(2, 2)*v.z);
    }
}

#endif /* _CUMATRIX3X3_HPP_ */
