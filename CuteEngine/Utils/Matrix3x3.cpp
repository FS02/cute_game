#include <CuteEngine/Utils/Matrix3x3.hpp>

using namespace cute;

Matrix3x3::Matrix3x3(Matrix3x3::Init i)
{
    (void)i;
    m[0]=1; m[3]=0; m[6]=0;
    m[1]=0; m[4]=1; m[7]=0;
    m[2]=0; m[5]=0; m[8]=1;
}

Matrix3x3::Matrix3x3(const Matrix3x3& copy)
{
    for(int i=0; i<9; ++i)
        m[i]=copy.m[i];
}

float Matrix3x3::at(unsigned int x, unsigned int y) const
{
    return m[x*3+y];
}

float& Matrix3x3::at(unsigned int x, unsigned int y)
{
    return m[x*3+y];
}

float Matrix3x3::operator ()(unsigned int x, unsigned int y) const
{
    return m[x*3+y];
}

float& Matrix3x3::operator ()(unsigned int x, unsigned int y)
{
    return m[x*3+y];
}


const float* Matrix3x3::data() const
{
    return m;
}


void Matrix3x3::transpose()
{
    float tmp;
    for(int i=0; i<3; ++i)
    {
        for(int j=i; j<3; ++j)
        {
            tmp=at(i, j);
            at(i, j)=at(j, i);
            at(j, i)=tmp;
        }
    }
}


float Matrix3x3::determinant() const
{
    return (m[0]*m[4]*m[8] + m[3]*m[7]*m[2] + m[6]*m[1]*m[5]) - (m[3]*m[1]*m[8] + m[0]*m[7]*m[5] + m[6]*m[4]*m[2]);
}


float Matrix3x3::cofactor(unsigned int x, unsigned int y) const
{
    float mi[2][2];
    int nextX=0, nextY=0;

    for(unsigned int i=0; i<3; ++i)
    {
        if(i==x)
            continue;

        for(unsigned int j=0; j<3; ++j)
        {
            if(j==y)
                continue;

            mi[nextX][nextY]=at(i, j);
            ++nextY;
        }

        ++nextX;
        nextY=0;
    }

    float f=(x+y)%2 == 0 ? 1 : -1;
    return (mi[0][0]*mi[1][1] - mi[1][0]*mi[0][1])*f;
}


void Matrix3x3::invert(Matrix3x3& m) const
{
    for(int i=0; i<3; ++i)
        for(int j=0; j<3; ++j)
            m(j, i)=this->cofactor(i, j);
}


void Matrix3x3::setTranslation(const cute::Vector2& v)
{
    m[0]=1; m[3]=0; m[6]=v.x;
    m[1]=0; m[4]=1; m[7]=v.y;
    m[2]=0; m[5]=0; m[8]=1;
}


void Matrix3x3::translate(const cute::Vector2& v)
{
    at(2, 0)+=v.x;
    at(2, 1)+=v.y;
}


void Matrix3x3::setRotation(float rad)
{
    m[0]=std::cos(rad); m[3]=-std::sin(rad); m[6]=0;
    m[1]=std::sin(rad); m[4]=std::cos(rad);  m[7]=0;
    m[2]=0; 		    m[5]=0; 			 m[8]=1;
}


void Matrix3x3::rotate(float rad)
{
    float r = std::acos(m[0]) + rad;
    setRotation(r);
}


void Matrix3x3::setScale(const cute::Vector2& v)
{
    m[0]=v.x; m[3]=0;   m[6]=0;
    m[1]=0;   m[4]=v.y; m[7]=0;
    m[2]=0;   m[5]=0;   m[8]=1;
}


void Matrix3x3::scale(const cute::Vector2& v)
{
    at(0, 0)*=v.x;
    at(1, 1)*=v.y;
}


void Matrix3x3::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Matrix3x3> matrix3x3(vm);
    matrix3x3.SquirrelFunc("constructor", &cute::Matrix3x3::SqConstructor);
    matrix3x3.SquirrelFunc("data", &cute::Matrix3x3::SqGetArrayData);
    matrix3x3.SquirrelFunc("_mul", &cute::Matrix3x3::Sq_mul);
    matrix3x3.Func<float (Matrix3x3::*)(unsigned int, unsigned int) const>("at", &cute::Matrix3x3::at);
    matrix3x3.Func("transpose", &Matrix3x3::transpose);
    matrix3x3.Func("cofactor", &Matrix3x3::cofactor);
    matrix3x3.Func("invert", &Matrix3x3::invert);
    matrix3x3.Func("setTranslation", &Matrix3x3::setTranslation);
    matrix3x3.Func("translate", &Matrix3x3::translate);
    matrix3x3.Func("setRotation", &Matrix3x3::setRotation);
    matrix3x3.Func("rotate", &Matrix3x3::rotate);
    matrix3x3.Func("setScale", &Matrix3x3::setScale);
    matrix3x3.Func("scale", &Matrix3x3::scale);

    Sqrat::RootTable(vm).Bind("Matrix3x3", matrix3x3);
}


SQInteger Matrix3x3::SqConstructor(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 1) { // no manually put arguments? default constructor. note that the first argument is always a class or a environment in Squirrel functions
        Matrix3x3* instance = new Matrix3x3();
        sq_setinstanceup(vm, 1, instance);
        sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Matrix3x3>::Delete);
        return 0;
    } else if (sq_gettop(vm) == 2) {
        Sqrat::Var<const Matrix3x3&> copy(vm, 2); // copy constructor if 2nd argument (1st manually defined argument)
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Matrix3x3* instance = new Matrix3x3(copy.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Matrix3x3>::Delete);
            return 0;
        }
        Sqrat::Error::Instance().Clear(vm);
        return sq_throwerror(vm, Sqrat::Error::FormatTypeError(vm, 2, Sqrat::ClassType<b2Vec3>::ClassName(vm) + _SC("|float")).c_str()); // wasnt a float either... user is dumb
    }
    return sq_throwerror(vm, _SC("wrong number of parameters"));
}


SQInteger Matrix3x3::SqGetArrayData(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 1)
    {
        Sqrat::Var<Matrix3x3&> left(vm,1);
        if (!Sqrat::Error::Instance().Occurred(vm))
        {
            const float* _data = left.value.data();
            sq_newarray(vm, 9);
            for (std::size_t i = 0; i < 9; ++i)
            {
                Sqrat::PushVar(vm, i);
                Sqrat::PushVar(vm, _data[i]);
                sq_rawset(vm, -3);
            }
            return 1;
        }
        return sq_throwerror(vm, Sqrat::Error::Instance().Message(vm).c_str());
    }
    return sq_throwerror(vm, _SC("wrong number of parameters"));
}

SQInteger Matrix3x3::Sq_mul(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // matrix * matrix
        Sqrat::Var<const Matrix3x3&> left(vm, 1);
        Sqrat::Var<const Matrix3x3&> rightm(vm, 2); // multiply with another matrix
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Matrix3x3 result = left.value * rightm.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);

        // matrix * vector2
        Sqrat::Var<const Vector2&> rightv2(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Vector2 result =  left.value * rightv2.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);

        // matrix * vector3
        Sqrat::Var<const b2Vec3&> rightv3(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec3 result =  left.value * rightv3.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}


