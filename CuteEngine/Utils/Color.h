#ifndef COLOR_H
#define COLOR_H

#include <CuteEngine/World/SquirrelBindFactory.hpp>
namespace cute
{
    typedef unsigned char byte;
    class Color
    {
    public:
        Color(byte _r = 255, byte _g = 255, byte _b = 255, byte _a = 255);

        byte r, g, b, a;

        static const Color Black;
        static const Color White;
        static const Color Red;
        static const Color Green;
        static const Color Blue;
        static const Color Yellow;
        static const Color Magenta;
        static const Color Cyan;
        static const Color Transparent;

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static SQInteger SqConstructor(HSQUIRRELVM vm);
        static SQInteger Sq_add(HSQUIRRELVM vm);
        static SQInteger Sq_sub(HSQUIRRELVM vm);
        static SQInteger Sq_mul(HSQUIRRELVM vm);
        static SQInteger Sq_cmp(HSQUIRRELVM vm);
    };

    bool operator==(const cute::Color& lhs, const cute::Color& rhs);
    bool operator!=(const cute::Color& lhs, const cute::Color& rhs);
    cute::Color operator+(const cute::Color& lhs, const cute::Color& rhs);
    cute::Color operator-(const cute::Color& lhs, const cute::Color& rhs);
    cute::Color operator*(const cute::Color& lhs, const cute::Color& rhs);
    cute::Color& operator +=(cute::Color& lhs, const cute::Color& rhs);
    cute::Color& operator -=(cute::Color& lhs, const cute::Color& rhs);
    cute::Color& operator *=(cute::Color& lhs, const cute::Color& rhs);
}

#endif // COLOR_H
