#include "Time.hpp"
#include <SDL.h>

double GetTime()
{
    return (double)SDL_GetTicks()/1000.0;
}

unsigned int GetTimeStamp()
{
    return SDL_GetTicks();
}
