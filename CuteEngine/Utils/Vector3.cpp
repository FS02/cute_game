#include <CuteEngine/Utils/Vector3.hpp>

using namespace cute;

void _StaticBindVector3::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<b2Vec3> _b2Vec3;
    _b2Vec3.SquirrelFunc("constructor", &_StaticBindVector3::SqConstructor);
    _b2Vec3.SquirrelFunc("_add", &_StaticBindVector3::Sq_add);
    _b2Vec3.SquirrelFunc("_sub", &_StaticBindVector3::Sq_sub);
    _b2Vec3.SquirrelFunc("_mul", &_StaticBindVector3::Sq_mul);
    _b2Vec3.SquirrelFunc("_div", &_StaticBindVector3::Sq_div);
    _b2Vec3.SquirrelFunc("_cmp", &_StaticBindVector3::Sq_cmp);
    _b2Vec3.Func("set", &b2Vec3::Set);
    _b2Vec3.Func("setZero", &b2Vec3::SetZero);
    _b2Vec3.Var("x", &b2Vec3::x);
    _b2Vec3.Var("y", &b2Vec3::y);
    _b2Vec3.Var("z", &b2Vec3::z);
    Sqrat::RootTable(vm).Bind("Vector3", _b2Vec3);
}

SQInteger _StaticBindVector3::SqConstructor(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 1) { // no manually put arguments? default constructor. note that the first argument is always a class or a environment in Squirrel functions
        b2Vec3* instance = new b2Vec3();
        sq_setinstanceup(vm, 1, instance);
        sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<b2Vec3>::Delete);
        return 0;
    } else if (sq_gettop(vm) == 2) {
        Sqrat::Var<const b2Vec3&> copy(vm, 2); // copy constructor if 2nd argument (1st manually defined argument)
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec3* instance = new b2Vec3(copy.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<b2Vec3>::Delete);
            return 0;
        }
        Sqrat::Error::Instance().Clear(vm);
        return sq_throwerror(vm, Sqrat::Error::FormatTypeError(vm, 2, Sqrat::ClassType<b2Vec3>::ClassName(vm) + _SC("|float")).c_str()); // wasnt a float either... user is dumb
    } else if (sq_gettop(vm) == 4) { // 4 arguments?
        Sqrat::Var<float> _vx(vm, 2);
        Sqrat::Var<float> _vy(vm, 3);
        Sqrat::Var<float> _vz(vm, 4);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec3* instance = new b2Vec3(_vx.value, _vy.value, _vz.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<b2Vec3>::Delete);
            return 0;
        }
        return sq_throwerror(vm, Sqrat::Error::Instance().Message(vm).c_str());
    }
    return sq_throwerror(vm, _SC("wrong number of parameters"));
}

SQInteger _StaticBindVector3::Sq_add(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector + vector
        Sqrat::Var<const b2Vec3&> left(vm, 1);
        Sqrat::Var<const b2Vec3&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec3 result = left.value + right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger _StaticBindVector3::Sq_sub(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector - vector
        Sqrat::Var<const b2Vec3&> left(vm, 1);
        Sqrat::Var<const b2Vec3&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec3 result = left.value - right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger _StaticBindVector3::Sq_mul(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector * float
        Sqrat::Var<const b2Vec3&> left(vm, 1);
        Sqrat::Var<float> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec3 result = right.value * left.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger _StaticBindVector3::Sq_div(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector / float
        Sqrat::Var<const b2Vec3&> left(vm, 1);
        Sqrat::Var<float> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec3 result(left.value.x/right.value, left.value.y/right.value,left.value.z/right.value);
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger _StaticBindVector3::Sq_cmp(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument

        Sqrat::Var<const b2Vec3&> left(vm, 1);
        Sqrat::Var<const b2Vec3&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            int result;
            float lLengthSq = left.value.x * left.value.x + left.value.y * left.value.y + left.value.z * left.value.z;
            float rLengthSq = right.value.x * right.value.x + right.value.y * right.value.y + right.value.z * right.value.z;
            if (left.value == right.value) result = 0;
            else if (lLengthSq > rLengthSq) result = 1;
            else result = -1;
            return result;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}
