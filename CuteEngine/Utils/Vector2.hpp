#ifndef _CUVECTOR2_HPP_
#define _CUVECTOR2_HPP_

#include <ostream>
#include <cmath>
#include <CuteEngine/Utils/Vector3.hpp>
#include <Box2D/Box2D.h>

namespace cute
{
    typedef b2Vec2 Vector2;

    class _StaticBindVector2
    {
    public:
        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static SQInteger SqConstructor(HSQUIRRELVM vm);
        static SQInteger Sq_add(HSQUIRRELVM vm);
        static SQInteger Sq_sub(HSQUIRRELVM vm);
        static SQInteger Sq_mul(HSQUIRRELVM vm);
        static SQInteger Sq_div(HSQUIRRELVM vm);
        static SQInteger Sq_cmp(HSQUIRRELVM vm);
    };
}

inline bool operator!=(const b2Vec2& lhs, const b2Vec2& rhs){return !(lhs == rhs); }

#endif /* _CUVECTOR2_HPP_ */
