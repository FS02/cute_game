#include <CuteEngine/Utils/Rect.hpp>

using namespace cute;

Rect::Rect()
    : x(0), y(0), width(0), height(0)
{

}

Rect::Rect(float _x, float _y, float _width, float _height)
    : x(_x), y(_y), width(_width), height(_height)
{

}

Rect::Rect(const Vector2& position, const Vector2& size)
    : x(position.x), y(position.y), width(size.x), height(size.y)
{

}

Rect::Rect(const Rect& r)
    : x(r.x), y(r.y), width(r.width), height(r.height)
{

}

Rect::~Rect()
{

}

bool Rect::contains(float _x, float _y) const
{
    float maxX = std::max(x, x + width);
    float minX = std::min(x, x + width);
    float maxY = std::max(y, y + height);
    float minY = std::min(y, y + height);
    return ((_x >=  minX && _x < maxX) && (_y >= minY && _y < maxY));
}

bool Rect::contains(const Vector2& point) const
{
    return contains(point.x, point.y);
}

void Rect::bindSquirrel(HSQUIRRELVM vm)
{
    // Binding Rect
    Sqrat::Class<Rect> rect;
    rect.SquirrelFunc("constructor", &Rect::SqConstructor);
    rect.Overload<bool (Rect::*)(float, float) const>("contains", &Rect::contains);
    rect.Overload<bool (Rect::*)(const Vector2&) const>("contains", &Rect::contains);
    rect.Var("x", &Rect::x);
    rect.Var("y", &Rect::y);
    rect.Var("width", &Rect::width);
    rect.Var("height", &Rect::height);
    Sqrat::RootTable(vm).Bind("Rect", rect);
}

SQInteger Rect::SqConstructor(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 1) { // no manually put arguments? default constructor. note that the first argument is always a class or a environment in Squirrel functions
        Rect* instance = new Rect();
        sq_setinstanceup(vm, 1, instance);
        sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Rect>::Delete);
        return 0;
    } else if (sq_gettop(vm) == 2) {
        Sqrat::Var<const Rect&> copy(vm, 2); // copy constructor if 2nd argument (1st manually defined argument)
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Rect* instance = new Rect(copy.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Rect>::Delete);
            return 0;
        }
        Sqrat::Error::Instance().Clear(vm);
        return sq_throwerror(vm, Sqrat::Error::FormatTypeError(vm, 2, Sqrat::ClassType<Rect>::ClassName(vm) + _SC("|float")).c_str()); // wasnt a float either... user is dumb
    } else if (sq_gettop(vm) == 3) { // 3 arguments?
        Sqrat::Var<Vector2> _position(vm, 2);
        Sqrat::Var<Vector2> _size(vm, 3);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Rect* instance = new Rect(_position.value, _size.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Rect>::Delete);
            return 0;
        }
        return sq_throwerror(vm, Sqrat::Error::Instance().Message(vm).c_str());
    } else if (sq_gettop(vm) == 5) { // 5 arguments?
        Sqrat::Var<float> _x(vm, 2);
        Sqrat::Var<float> _y(vm, 3);
        Sqrat::Var<float> _width(vm, 4);
        Sqrat::Var<float> _height(vm, 5);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Rect* instance = new Rect(_x.value, _y.value, _width.value, _height.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Rect>::Delete);
            return 0;
        }
        return sq_throwerror(vm, Sqrat::Error::Instance().Message(vm).c_str());
    }
    return sq_throwerror(vm, _SC("wrong number of parameters"));
}

