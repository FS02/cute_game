#include "Path.hpp"

std::string path::join(const std::string& a, const std::string& b)
{
	if(a.empty())
		return b;
	if(b.empty())
		return a;

	if(a.back() == '/' && b[0] == '/')
		return a+b.substr(1);

	else if(a.back() == '/' || b[0] == '/')
		return a+b;

	else
		return a+'/'+b;
}

std::string path::parent(const std::string& p)
{
	size_t idx=p.find_last_of('/');
	if(idx == std::string::npos)
		return std::string();
	return p.substr(0, idx);
}
