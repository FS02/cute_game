#ifndef _PATH_HPP_
#define _PATH_HPP_

#include <string>

namespace path
{
	std::string join(const std::string& a, const std::string& b);
	std::string parent(const std::string& p);
}

#endif /* _PATH_HPP_ */
