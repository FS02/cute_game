#include <CuteEngine/Utils/Color.h>

using namespace cute;

const Color Color::Black(0, 0, 0);
const Color Color::White(255, 255, 255);
const Color Color::Red(255, 0, 0);
const Color Color::Green(0, 255, 0);
const Color Color::Blue(0, 0, 255);
const Color Color::Yellow(255, 255, 0);
const Color Color::Magenta(255, 0, 255);
const Color Color::Cyan(0, 255, 255);
const Color Color::Transparent(0, 0, 0, 0);

Color::Color(byte _r, byte _g, byte _b, byte _a)
    : r(_r), g(_g), b(_b), a(_a)
{}

void Color::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<Color> _color;
    _color.SquirrelFunc("constructor", &Color::SqConstructor);
    _color.SquirrelFunc("_add", &Color::Sq_add);
    _color.SquirrelFunc("_sub", &Color::Sq_sub);
    _color.SquirrelFunc("_mul", &Color::Sq_mul);
    _color.SquirrelFunc("_cmp", &Color::Sq_cmp);
    _color.Var("r", &Color::r);
    _color.Var("g", &Color::g);
    _color.Var("b", &Color::b);
    _color.Var("a", &Color::a);
//    _color.StaticVar("Black", &Color::Black);
//    _color.StaticVar("Blue", &Color::Blue);;
//    _color.StaticVar("Cyan", &Color::Cyan);
//    _color.StaticVar("Green", &Color::Green);
//    _color.StaticVar("Magenta", &Color::Magenta);
//    _color.StaticVar("Red", &Color::Red);
//    _color.StaticVar("Transparent", &Color::Transparent);
//    _color.StaticVar("White", &Color::White);
//    _color.StaticVar("Yellow", &Color::Yellow);
    Sqrat::RootTable(vm).Bind("Color", _color);
}

SQInteger Color::SqConstructor(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 1) { // no manually put arguments? default constructor. note that the first argument is always a class or a environment in Squirrel functions
        Color* instance = new Color();
        sq_setinstanceup(vm, 1, instance);
        sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Color>::Delete);
        return 0;
    } else if (sq_gettop(vm) == 2) {
        Sqrat::Var<const Color&> copy(vm, 2); // copy constructor
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Color* instance = new Color(copy.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Color>::Delete);
            return 0;
        }
        Sqrat::Error::Instance().Clear(vm);
        return sq_throwerror(vm, Sqrat::Error::FormatTypeError(vm, 2, Sqrat::ClassType<Color>::ClassName(vm) + _SC("|float")).c_str());
    } else if (sq_gettop(vm) == 4) {
        Sqrat::Var<byte> _vr(vm, 2);
        Sqrat::Var<byte> _vg(vm, 3);
        Sqrat::Var<byte> _vb(vm, 4);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Color* instance = new Color(_vr.value, _vg.value, _vb.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Color>::Delete);
            return 0;
        }
        return sq_throwerror(vm, Sqrat::Error::FormatTypeError(vm, 5, Sqrat::ClassType<Color>::ClassName(vm) + _SC("|float")).c_str());

    } else if (sq_gettop(vm) == 5) {
        Sqrat::Var<byte> _vr(vm, 2);
        Sqrat::Var<byte> _vg(vm, 3);
        Sqrat::Var<byte> _vb(vm, 4);
        Sqrat::Var<byte> _va(vm, 5);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Color* instance = new Color(_vr.value, _vg.value, _vb.value, _va.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<Color>::Delete);
            return 0;
        }
        return sq_throwerror(vm, Sqrat::Error::Instance().Message(vm).c_str());
    }
    return sq_throwerror(vm, _SC("wrong number of parameters"));
}

SQInteger Color::Sq_add(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) {
        Sqrat::Var<const Color&> left(vm, 1);
        Sqrat::Var<const Color&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Color result = left.value + right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger Color::Sq_sub(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector - vector
        Sqrat::Var<const Color&> left(vm, 1);
        Sqrat::Var<const Color&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Color result = left.value - right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger Color::Sq_mul(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector * float
        Sqrat::Var<const Color&> left(vm, 1);
        Sqrat::Var<float> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Color result = right.value * left.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);

        // float * vector
        Sqrat::Var<float> leftf(vm, 1);
        Sqrat::Var<const Color&> rightf(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            Color result = leftf.value * rightf.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger Color::Sq_cmp(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector / float
        Sqrat::Var<const Color&> left(vm, 1);
        Sqrat::Var<const Color&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            int result;
            if (left.value == right.value) result = 0;
            else result = -1;
            return result;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

bool cute::operator ==(const Color& lhs, const Color& rhs)
{
    return (lhs.r == rhs.r)
            && (lhs.g == rhs.g)
            && (lhs.b == rhs.b)
            && (lhs.a == rhs.a);
}

bool cute::operator !=(const Color& lhs, const Color& rhs)
{
    return !(lhs == rhs);
}

Color cute::operator +(const Color& lhs, const Color& rhs)
{
    return Color(byte(std::min(int(lhs.r) + rhs.r, 255)),
                 byte(std::min(int(lhs.g) + rhs.g, 255)),
                 byte(std::min(int(lhs.b) + rhs.b, 255)),
                 byte(std::min(int(lhs.a) + rhs.a, 255)));
}

Color cute::operator -(const Color& lhs, const Color& rhs)
{
    return Color(byte(std::max(int(lhs.r) - rhs.r, 0)),
                 byte(std::max(int(lhs.g) - rhs.g, 0)),
                 byte(std::max(int(lhs.b) - rhs.b, 0)),
                 byte(std::max(int(lhs.a) - rhs.a, 0)));
}

Color cute::operator *(const Color& lhs, const Color& rhs)
{
    return Color(byte(int(lhs.r) * rhs.r / 255),
                 byte(int(lhs.g) * rhs.g / 255),
                 byte(int(lhs.b) * rhs.b / 255),
                 byte(int(lhs.a) * rhs.a / 255));
}

Color& cute::operator +=(Color& lhs, const Color& rhs)
{
    return lhs = lhs + rhs;
}

Color& cute::operator -=(Color& lhs, const Color& rhs)
{
    return lhs = lhs - rhs;
}

Color& cute::operator *=(Color& lhs, const Color& rhs)
{
    return lhs = lhs * rhs;
}
