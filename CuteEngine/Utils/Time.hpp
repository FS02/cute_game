#ifndef TIME_HPP
#define TIME_HPP

//! wrapper around SDL_GetTicks. Returns time in seconds.
double GetTime();

//! Time stamp. Time in miliseconds since the game has started.
unsigned int GetTimeStamp();

#endif // TIME_HPP
