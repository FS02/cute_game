#ifndef RECT_HPP
#define RECT_HPP

#include <CuteEngine/Utils/Vector2.hpp>

namespace cute
{
    class Rect
    {
    public:
        Rect();
        Rect(float _x, float _y, float _width, float _height);
        Rect(const Vector2& position, const Vector2& size);
        Rect(const Rect& r);

        ~Rect();

        bool contains(float _x, float _y) const;
        bool contains(const Vector2& point) const;

        float x;
        float y;
        float width;
        float height;

        static void bindSquirrel(HSQUIRRELVM vm);

    private:
        static SQInteger SqConstructor(HSQUIRRELVM vm);
    };

    inline bool operator ==(const Rect& a, const Rect& b)
    {
        return (a.x == b.x && a.y == b.y && a.width == b.width && a.height == b.height);
    }

    inline bool operator !=(const Rect& a, const Rect& b)
    {
        return !(a.x == b.x && a.y == b.y && a.width == b.width && a.height == b.height);
    }

    inline bool operator >(const Rect&a, const Rect& b)
    {
        return ((a.x * a.x + a.y * a.y) > (b.x * b.x + b.y * b.y));
    }

    inline bool operator <(const Rect&a, const Rect& b)
    {
        return !(a>b || a==b);
    }
}

#endif // RECT_HPP
