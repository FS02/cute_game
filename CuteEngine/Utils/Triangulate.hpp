#ifndef TRIANGULATE_HPP
#define TRIANGULATE_HPP

#include <CuteEngine/Utils/Vector2.hpp>

namespace cute
{
    /*
    * generate valid triangle polygon vertex
    * based on : http://www.flipcode.com/archives/Efficient_Polygon_Triangulation.shtml
    */
    class Triangulate
    {
    public:
        // triangulate a contour/polygon, places results in STL vector
        // as series of triangles.
        static bool process(const std::vector<Vector2> &contour, std::vector<Vector2> &result);

        // compute area of a contour/polygon
        static float area(const std::vector<Vector2> &contour);

        // decide if point Px/Py is inside triangle defined by
        // (Ax,Ay) (Bx,By) (Cx,Cy)
        static bool insideTriangle(float Ax, float Ay,
                                   float Bx, float By,
                                   float Cx, float Cy,
                                   float Px, float Py);

    private:
        static bool snip(const std::vector<Vector2> &contour,int u,int v,int w,int n,int *V);

    };
}

#endif // TRIANGULATE_HPP
