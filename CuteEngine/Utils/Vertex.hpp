#ifndef VERTEX_HPP
#define VERTEX_HPP

namespace  cute
{
    struct Vertex
    {
        float x, y, z;
        unsigned char r, g, b, a;
        float u, v;
    };
}
#endif // VERTEX_HPP
