#include <CuteEngine/Utils/Vector2.hpp>
#include <CuteEngine/Squirrel/include/sqrat.h>

using namespace cute;

void _StaticBindVector2::bindSquirrel(HSQUIRRELVM vm)
{
    Sqrat::Class<b2Vec2> _b2Vec2;
    _b2Vec2.SquirrelFunc("constructor", &_StaticBindVector2::SqConstructor);
    _b2Vec2.SquirrelFunc("_add", &_StaticBindVector2::Sq_add);
    _b2Vec2.SquirrelFunc("_sub", &_StaticBindVector2::Sq_sub);
    _b2Vec2.SquirrelFunc("_mul", &_StaticBindVector2::Sq_mul);
    _b2Vec2.SquirrelFunc("_div", &_StaticBindVector2::Sq_div);
    _b2Vec2.SquirrelFunc("_cmp", &_StaticBindVector2::Sq_cmp);
    _b2Vec2.Func("isValid", &b2Vec2::IsValid);
    _b2Vec2.Func("length", &b2Vec2::Length);
    _b2Vec2.Func("lengthSquared", &b2Vec2::LengthSquared);
    _b2Vec2.Func("normalize", &b2Vec2::Normalize);
    _b2Vec2.Func("set", &b2Vec2::Set);
    _b2Vec2.Func("setZero", &b2Vec2::SetZero);
    _b2Vec2.Func("skew", &b2Vec2::Skew);
    _b2Vec2.Var("x", &b2Vec2::x);
    _b2Vec2.Var("y", &b2Vec2::y);
    Sqrat::RootTable(vm).Bind("Vector2", _b2Vec2);
}

SQInteger _StaticBindVector2::SqConstructor(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 1) { // no manually put arguments? default constructor. note that the first argument is always a class or a environment in Squirrel functions
        b2Vec2* instance = new b2Vec2();
        sq_setinstanceup(vm, 1, instance);
        sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<b2Vec2>::Delete);
        return 0;
    } else if (sq_gettop(vm) == 2) {
        Sqrat::Var<const b2Vec2&> copy(vm, 2); // copy constructor
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec2* instance = new b2Vec2(copy.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<b2Vec2>::Delete);
            return 0;
        }
        Sqrat::Error::Instance().Clear(vm);
        return sq_throwerror(vm, Sqrat::Error::FormatTypeError(vm, 2, Sqrat::ClassType<b2Vec2>::ClassName(vm) + _SC("|float")).c_str());
    } else if (sq_gettop(vm) == 3) { // 3 arguments?
        Sqrat::Var<float> _vx(vm, 2);
        Sqrat::Var<float> _vy(vm, 3);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec2* instance = new b2Vec2(_vx.value, _vy.value);
            sq_setinstanceup(vm, 1, instance);
            sq_setreleasehook(vm, 1, &Sqrat::DefaultAllocator<b2Vec2>::Delete);
            return 0;
        }
        return sq_throwerror(vm, Sqrat::Error::Instance().Message(vm).c_str());
    }
    return sq_throwerror(vm, _SC("wrong number of parameters"));
}

SQInteger _StaticBindVector2::Sq_add(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector + vector
        Sqrat::Var<const b2Vec2&> left(vm, 1);
        Sqrat::Var<const b2Vec2&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec2 result = left.value + right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger _StaticBindVector2::Sq_sub(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector - vector
        Sqrat::Var<const b2Vec2&> left(vm, 1);
        Sqrat::Var<const b2Vec2&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec2 result = left.value - right.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger _StaticBindVector2::Sq_mul(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector * float
        Sqrat::Var<const b2Vec2&> left(vm, 1);
        Sqrat::Var<float> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec2 result = right.value * left.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);

        // float * vector
        Sqrat::Var<float> leftf(vm, 1);
        Sqrat::Var<const b2Vec2&> rightf(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec2 result = leftf.value * rightf.value;
            Sqrat::PushVar(vm, result);
            return 1;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger _StaticBindVector2::Sq_div(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector / float
        Sqrat::Var<const b2Vec2&> left(vm, 1);
        Sqrat::Var<float> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            b2Vec2 result(left.value.x/right.value, left.value.y/right.value);
            Sqrat::PushVar(vm, result);
            return 1;
        }

        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}

SQInteger _StaticBindVector2::Sq_cmp(HSQUIRRELVM vm)
{
    if (sq_gettop(vm) == 2) { // one argument
        // vector / float
        Sqrat::Var<const b2Vec2&> left(vm, 1);
        Sqrat::Var<const b2Vec2&> right(vm, 2);
        if (!Sqrat::Error::Instance().Occurred(vm)) {
            int result;
            if (left.value == right.value) result = 0;
            else if (left.value.LengthSquared() > right.value.LengthSquared()) result = 1;
            else result = -1;
            return result;
        }
        Sqrat::Error::Instance().Clear(vm);
        Sqrat::PushVar(vm, "oh_noooes you cannot do that!");
        return 1;
    }
    return 0;
}
